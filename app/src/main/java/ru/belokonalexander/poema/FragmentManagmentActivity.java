package ru.belokonalexander.poema;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by alexander on 13.10.2016.
 */
abstract public class FragmentManagmentActivity extends AppCompatActivity {

//TODO

    abstract public void openNavigation();

    abstract void openInThisContainer(Fragment whichWillOpened);
}
