package ru.belokonalexander.poema.Fragments;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gc.materialdesign.views.Button;

import java.util.ArrayList;
import java.util.List;

import StaticHelper.LogSystem;
import StaticHelper.Settings;
import belokonalexander.Api.ApiTask;
import belokonalexander.Api.CurrentApi;
import belokonalexander.Api.InputQueryDataHolders.MassiveQueryInputData;
import belokonalexander.Api.QueryExecutorExplicit;
import belokonalexander.GlobalShell;
import belokonalexander.RxCommonElements.AnswerApiRx;
import ru.belokonalexander.poema.R;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by alexander on 14.10.2016.
 */
//TODO need refactoring
 public class FragmentContentWaiter {

    Observable<Object> provider;

    Subscriber<Object> receiver;




    ViewGroup rootView;
    Context context;
    ViewGroup popup;
    View toolbar;

    Long startTime;

    SuccessAnswer action;


    Button tryAgain;

    ImageView errorImage;
    TextView  errorText;
    ProgressBar progressBar;

    ViewGroup progressLayout;
    ViewGroup errorLayout;
    Animation hide;
    Animation show;
    final int animationDuration = 300;

    ApiTask[] queries;

    //todo rewrite
    List<Object> queryAnswers;

    public FragmentContentWaiter(Context context, ViewGroup rootView, View toolbar, SuccessAnswer action, ApiTask... queries) {


        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.popup = (ViewGroup) inflater.inflate(R.layout.help_loading_layout, null);


        progressLayout = (ViewGroup) popup.findViewById(R.id.progress_layout);
        errorLayout = (ViewGroup) popup.findViewById(R.id.error_layout);


        tryAgain = (Button) popup.findViewById(R.id.try_again_button);
        tryAgain.setOnClickListener(view -> startLoading());

        errorImage = (ImageView) popup.findViewById(R.id.error_image);
        errorText = (TextView) popup.findViewById(R.id.error_msg);
        progressBar = (ProgressBar) popup.findViewById(R.id.progress);


        this.queries = queries;
        this.queryAnswers = new ArrayList<>();
        //-----

        this.toolbar = toolbar;
        this.action = action;

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        popup.setLayoutParams(lp);

        this.provider = Observable.from(this.queries)
                .observeOn(Schedulers.newThread())
                .flatMap(queryTask -> Observable.create(s -> {
                        s.onNext(queryTask.execute());
                    s.onCompleted();
                }))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnCompleted(() -> {
                    action.onSuccessCompleted(queryAnswers);
                    hidePopup();
                })
        ;;

        this.rootView = rootView;

        prepareReceiver();

    }

    private void prepareReceiver() {

        resetParams();
        queryAnswers.clear();

        receiver = AnswerApiRx.getOnlyOnNext(new AnswerApiRx.CustomizeActionAdditional() {
            @Override
            public void onSuccess(Object res) {
                queryAnswers.add(res);
            }

            @Override
            public void onFailure(String message) {

                errorText.setText(message);
                hideGroup(progressLayout);
                //TODO generate exception
                throw new NullPointerException();
            }

            @Override
            public void onCanceled() {

            }
        });



    }


    void resetParams(){

        //recreate queries (wee need to clear state of each query)
        for (int i=0; i< queries.length ; i++){
           queries[i].refresh();
        }
    }

    public void startLoading(){
        startTime = System.currentTimeMillis();
        showPopup();

        if(receiver.isUnsubscribed()) {
            prepareReceiver();
        }

        provider.subscribe(receiver);
    }

    private void showPopup(){

        //TODO for while it only for RelayiveLayout

        if(rootView.indexOfChild(popup)<0)
            if(rootView instanceof RelativeLayout){
                rootView.addView(popup);
                popup.bringToFront();
                toolbar.bringToFront();

                LogSystem.logThis(" TOOLBAR: " + toolbar);

            } else throw new NullPointerException();

        hideGroup(errorLayout);
    }

    //TODO rewrite crazy animation area
    private void hideGroup(View hide) {

        View show;

        if(hide==progressLayout)
            show = errorLayout;
        else show = progressLayout;


                Animation anim = new AlphaAnimation(1f,0f);
                anim.setDuration(animationDuration);
                anim.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {

                        hide.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });


                hide.startAnimation(anim);


                anim = new AlphaAnimation(0f,1f);
                anim.setDuration(animationDuration);
                anim.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                        show.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                show.startAnimation(anim);

    }

    public void hidePopup(){

        Animation animation = new AlphaAnimation(1f,0f);
        animation.setDuration(300);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                popup.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        popup.startAnimation(animation);

    }

    public interface SuccessAnswer{
        //void onSuccessStep(Object answer);  //runs after each query
        void onSuccessCompleted(List<Object> answers);          // void onSuccessFinish(); //runs after all queries successfully finished and provide results
    }

}
