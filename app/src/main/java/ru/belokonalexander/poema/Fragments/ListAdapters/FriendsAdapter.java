package ru.belokonalexander.poema.Fragments.ListAdapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import StaticHelper.Settings;
import belokonalexander.Api.Models.FriendsRelate;
import belokonalexander.Api.Models.User;
import belokonalexander.GlobalShell;
import belokonalexander.RxCommonElements.UserRelation.UserRelationController;
import ru.belokonalexander.poema.CustomViews.GlideCircleTransformation;
import ru.belokonalexander.poema.Fragments.Profile;
import ru.belokonalexander.poema.MainActivity;
import ru.belokonalexander.poema.R;

/**
 * Created by alexander on 26.09.2016.
 */
public class FriendsAdapter extends CommonAdapter implements RelateControllerListener {


    public FriendsAdapter(List items,  Fragment fragmentHolder) {
        super(items, fragmentHolder);
    }

    @Override
    RecyclerView.ViewHolder onCreateVH(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_friends,parent,false);
        return new MyFriendsViewHolder(v, user -> {

            Profile openFriendProfile = new Profile();
            Bundle bundle = new Bundle();
            bundle.putString("UserID", user.getObjectId());
            bundle.putString("Name", user.getName());
            openFriendProfile.setArguments(bundle);

            ((MainActivity)getFragmentHolder().getActivity()).openInThisContainer(openFriendProfile);

        });
    }

    @Override
    void onBindVH(RecyclerView.ViewHolder holder, int position) {
        MyFriendsViewHolder holderMain = (MyFriendsViewHolder) holder;

        User friend = (User)items.get(position);

        holderMain.friendName.setText(friend.getName());

        Settings.setCommonOptions(Glide.with(context).load(friend.getSmallImage()).error(Settings.getDefaultImage(context, friend)).override(GlobalShell.dpToPixels(40), GlobalShell.dpToPixels(40)).transform(new GlideCircleTransformation(context))).into(holderMain.friendAvatar);

        holderMain.userRelationController.setUser(friend, this, holderMain);

    }


    //callback for all methods which update adapter's items
    @Override
    public void onUpdateUserState(FriendsRelate.RelateStates from , User user) {

        //was friend -> got none friend
        if(from == FriendsRelate.RelateStates.FRIENDS && user.getFriendStatusDesc()== FriendsRelate.RelateStates.NONE){
            int removedPosition = getPosition(user);
            if(items.remove(user))
                notifyItemRemoved(removedPosition);
        }
    }


    private class MyFriendsViewHolder extends ControllerHolder {

        public CardView cv;
        public TextView friendName;
        public ImageView friendAvatar;
        public UserRelationController userRelationController;

        public MyFriendsViewHolder(View itemView, Clicks clicks) {
            super(itemView);
            friendName = (TextView) itemView.findViewById(R.id.user_name);
            cv = (CardView)itemView.findViewById(R.id.item_view);
            friendAvatar = (ImageView) itemView.findViewById(R.id.user_avatar);

            userRelationController = (UserRelationController) itemView.findViewById(R.id.friends_control);
            cv.setOnClickListener(view -> clicks.mainClick((User)getItem(getLayoutPosition())));

        }
    }

    private interface Clicks{
        void mainClick(User user);
    }
}
