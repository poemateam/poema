package ru.belokonalexander.poema.Events;

import java.util.List;

import belokonalexander.Api.Models.PoemaMessage;

/**
 * Created by Alexander on 12.02.2017.
 */

public class MessageEvent {

    List<PoemaMessage> poemaMessage;

    public MessageEvent(   List<PoemaMessage>  poemaMessage) {
        this.poemaMessage = poemaMessage;
    }

    public  List<PoemaMessage>  getPoemaMessage() {
        return poemaMessage;
    }

    public void setPoemaMessage(List<PoemaMessage>  poemaMessage) {
        this.poemaMessage = poemaMessage;
    }
}
