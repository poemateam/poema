package ru.belokonalexander.poema.Interfaces;

/**
 * Created by alexander on 03.08.2016.
 */
public interface RedirectNextFragment {

    public void redirect();

}
