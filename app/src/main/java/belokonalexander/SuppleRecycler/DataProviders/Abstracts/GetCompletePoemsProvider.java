package belokonalexander.SuppleRecycler.DataProviders.Abstracts;

import java.util.ArrayList;
import java.util.List;

import Helper.RecycledElement;
import belokonalexander.Api.InputQueryDataHolders.MassiveQueryInputData;
import belokonalexander.Api.InputQueryDataHolders.SearchInputData;
import belokonalexander.Api.Models.CompletePoema;
import belokonalexander.Api.Models.SugarAdditional.SugarPoemaDB;

/**
 * Created by Alexander on 22.02.2017.
 */

public class GetCompletePoemsProvider extends SolidProvider<RecycledElement> implements ISearchProvider {

    SearchInputData searchInputData = new SearchInputData();

    @Override
    public List<RecycledElement> getData(MassiveQueryInputData state) {

         searchInputData.setOffset(state.getOffset());
         searchInputData.setPageSize(state.getPageSize());

        ArrayList<RecycledElement> data = new ArrayList<>();

        for(CompletePoema cp : SugarPoemaDB.getInstance().getCompletePoems(searchInputData)){
            data.add(new RecycledElement(cp));
        }

        return data;
    }


    @Override
    public List getCacheData(int pageSize) {
        return new ArrayList<>();
    }

    @Override
    public void updateSearchInputData(SearchInputData searchInputData) {
        this.searchInputData = searchInputData;
    }

    @Override
    public boolean onEmptyInputData() {
        searchInputData = new SearchInputData();

        return true;
    }


}
