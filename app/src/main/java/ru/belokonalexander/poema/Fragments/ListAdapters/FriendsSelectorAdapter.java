package ru.belokonalexander.poema.Fragments.ListAdapters;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import StaticHelper.LogSystem;
import StaticHelper.Settings;
import belokonalexander.Api.Models.User;
import belokonalexander.GlobalShell;
import ru.belokonalexander.poema.CustomViews.GlideCircleTransformation;
import ru.belokonalexander.poema.Fragments.BundleKeeper;
import ru.belokonalexander.poema.R;

/**
 * Created by alexander on 26.09.2016.
 */
public class FriendsSelectorAdapter extends CommonAdapter {


    public FriendsSelectorAdapter(List items, Fragment fragmentHolder) {
        super(items, fragmentHolder);
    }

    @Override
    RecyclerView.ViewHolder onCreateVH(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_friends_selector,parent,false);
        return new MyFriendsViewHolder(v, user -> {

            Intent intent = new Intent();
            intent.putExtra("USER_ID",user.getObjectId());

            View avatar = v.findViewById(R.id.user_avatar);

            int[] screen = new int[2];
            int[] sizes = new int[2];


            avatar.getLocationOnScreen(screen);

            LogSystem.logThis(" GET X: " + screen[0] + " / " + screen[1]);

            intent.putExtra("IMAGE_LOCATION_SCREEN",screen);
            sizes[0] = avatar.getLayoutParams().width;
            sizes[1] = avatar.getLayoutParams().height;
            intent.putExtra("IMAGE_SIZE", sizes);

            if(fragmentHolder instanceof BundleKeeper){
                Bundle bn = ((BundleKeeper) fragmentHolder).getBundle();

                for(String key : bn.keySet()){
                    intent.putExtra(key, bn.get(key).toString());
                }

            }



            getFragmentHolder().getActivity().onBackPressed();
            getFragmentHolder().getTargetFragment().onActivityResult(getFragmentHolder().getTargetRequestCode(), Activity.RESULT_OK, intent);


        });
    }

    @Override
    void onBindVH(RecyclerView.ViewHolder holder, int position) {
        MyFriendsViewHolder holderMain = (MyFriendsViewHolder) holder;

        User friend = (User)items.get(position);

        String name = "";
        if(friend.getReal_name().length()>0)
            name = "(" + friend.getReal_name() + ")";

        holderMain.friendName.setText(friend.getName()+" "+name);

        Settings.setCommonOptions(Glide.with(context).load(friend.getSmallImage()).error(Settings.getDefaultImage(context, friend)).override(GlobalShell.dpToPixels(40), GlobalShell.dpToPixels(40)).transform(new GlideCircleTransformation(context))).into(holderMain.friendAvatar);


    }


    private class MyFriendsViewHolder extends RecyclerView.ViewHolder {

        public CardView cv;
        public TextView friendName;
        public ImageView friendAvatar;

        public MyFriendsViewHolder(View itemView, Clicks clicks) {
            super(itemView);
            friendName = (TextView) itemView.findViewById(R.id.user_name);
            cv = (CardView)itemView.findViewById(R.id.item_view);
            friendAvatar = (ImageView) itemView.findViewById(R.id.user_avatar);

            cv.setOnClickListener(view -> clicks.mainClick((User)getItem(getLayoutPosition())));

        }
    }

    private interface Clicks{
        void mainClick(User user);
    }
}
