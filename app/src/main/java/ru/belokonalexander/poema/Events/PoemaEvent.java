package ru.belokonalexander.poema.Events;

import belokonalexander.Api.Models.Poema;

/**
 * Created by Alexander on 12.02.2017.
 */

public class PoemaEvent {

    Poema poema;

    public PoemaEvent(Poema poema) {
        this.poema = poema;
    }

    public Poema getPoema() {
        return poema;
    }

    public void setPoema(Poema poema) {
        this.poema = poema;
    }
}
