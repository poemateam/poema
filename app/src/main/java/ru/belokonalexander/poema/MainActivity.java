package ru.belokonalexander.poema;

import android.content.ComponentCallbacks;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethod;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;


import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.local.UserTokenStorageFactory;
import com.bumptech.glide.Glide;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.ArrayList;
import java.util.Map;


import StaticHelper.AppNavigation;
import StaticHelper.Command;
import StaticHelper.ContentToPath;

import StaticHelper.LogSystem;
import StaticHelper.Settings;
import belokonalexander.Api.CurrentApi;

import belokonalexander.Api.Models.Poema;
import belokonalexander.Api.Models.PoemaMessage;
import belokonalexander.Api.Models.SugarAdditional.SugarPoemaDB;
import belokonalexander.Api.Models.CompletePoema;
import belokonalexander.Api.Models.User;
import belokonalexander.Api.QueryExecutorExplicit;
import belokonalexander.Backendless.AutoCanceledWaitingTask;
import belokonalexander.Backendless.Services.PoemaUserService;
import belokonalexander.GlobalShell;
import belokonalexander.SharedAppPrefs;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import ru.belokonalexander.poema.Events.MessageStatusEvent;
import ru.belokonalexander.poema.Fragments.Feedback;
import ru.belokonalexander.poema.Fragments.Friends;
import ru.belokonalexander.poema.Fragments.PoemaLibrary;
import ru.belokonalexander.poema.Fragments.PoemsMenu;
import ru.belokonalexander.poema.Fragments.Profile;
import ru.belokonalexander.poema.Fragments.SettingsFragment;
import ru.belokonalexander.poema.PageManagers.BackendRunner;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    //UltimateRecyclerView messagesRecycleView;

    Profile.PhotoLogic profilePhotoLogic;
    NavigationView navigationView;
    DrawerLayout drawer;


    CircleImageView userImageView;

    TextView userName;

    TextView userEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        Bundle scope = getIntent().getBundleExtra("EXTEND");
        String path = "";


        if(scope!=null && (path = scope.getString("PATH"))!=null && path.equals(Friends.class.getCanonicalName())){
            LogSystem.logThis(" TARGET: " + path);
            displayView(R.id.nav_friends);
        }
        else
            displayView(R.id.nav_poems);


        updateHeaderProfile();

        CurrentApi.getInstanse().getApplicationApi().startMessagingService();

    }


    public void updateHeaderProfile() {

            ViewGroup header = (ViewGroup) navigationView.getHeaderView(0);



            userImageView = (CircleImageView) header.findViewById(R.id.user_avatar_header);
            userName = (TextView) header.findViewById(R.id.user_name_header);
            userEmail = (TextView) header.findViewById(R.id.user_email_header);

            userImageView.setBorderWidth(GlobalShell.dpToPixels(2));
            userImageView.setBorderColor(ContextCompat.getColor(getBaseContext(),R.color.normal_text_color_white));


            //TODO NullPointerException in getObjectId();
            Settings.setCommonOptions(Glide.with(getBaseContext()).load(CurrentApi.getInstanse().getCurrentUser().getImageUrl()).error(Settings.getDefaultImage(getBaseContext(), CurrentApi.getInstanse().getCurrentUser())).override(GlobalShell.dpToPixels(115), GlobalShell.dpToPixels(115))
                ).into(userImageView);

            userImageView.setOnClickListener(v -> displayView(R.id.nav_profile));

            userName.setText(CurrentApi.getInstanse().getCurrentUser().getName());
            userEmail.setText(CurrentApi.getInstanse().getCurrentUser().getEmail());

        //GlobalShell.openActivityWithCloseThis(getBaseContext(), SplashScreen.class);

    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        displayView(item.getItemId());
        return true;

    }

    public void displayView(int viewId)
    {
        Fragment fragment = null;
        String title = "";

        System.gc();

        switch (viewId)
        {
            case R.id.nav_settings_exit:
                unregisterDevice(() -> CurrentApi.getInstanse().getApplicationApi().logout(getBaseContext()));
                return;

            case R.id.nav_profile:

                fragment = new Profile();
                title = getString(R.string.profile_menu_item);
                profilePhotoLogic = ((Profile)fragment).getPhotoLogic();
                break;

            case R.id.nav_friends:
                fragment = new Friends();
                title = getString(R.string.friends_menu_item);
                break;

            case R.id.nav_poems:
                fragment = new PoemsMenu();
                title = getString(R.string.poems);
                break;
            case R.id.nav_my_library:
                fragment = new PoemaLibrary();
                title = getString(R.string.poema_library);
                break;
            case R.id.nav_feedback:
                fragment = new Feedback();
                title = getString(R.string.poema_feedback);
                break;

            case R.id.nav_settings:
                fragment = new SettingsFragment();
                title = getString(R.string.settings);
                break;
        }

        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment, addNewFragment(true));
            ft.commit();
        }

        navigationView.setCheckedItem(viewId);


        //TODO определить для каждого ActionBar
        // set the toolbar title
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);

        }

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

    }

    interface AfterDeviceStat{
        void onNext();
    }

    private void unregisterDevice(AfterDeviceStat command) {
        QueryExecutorExplicit registerDevice = new QueryExecutorExplicit(GlobalShell.getParentHash(this.getClass()), resultWaiter -> CurrentApi.getInstanse().getApplicationApi().unregisterDevice(resultWaiter));

        BackendRunner backendRunner = new BackendRunner(answers -> {
            command.onNext();
        }, messsage -> {
            command.onNext();
        }, registerDevice);

        backendRunner.run();
    }

    int fragmentsCount = 0;

    String addNewFragment(boolean starter){
        if(starter)
            fragmentsCount = 0;
        else fragmentsCount++;
        return String.valueOf(fragmentsCount);
    }

    void popNewFragment(){
        fragmentsCount--;
    }

    public int getTopFragment(){
        return fragmentsCount;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode== GlobalShell.PROFILE_IMAGE_CHANGED)
            if(resultCode==RESULT_OK)
            {
               profilePhotoLogic.uploadNewPhoto(new File(ContentToPath.getPath(getBaseContext(),data.getData())));
            }

    }


    public void openNavigation(){
        drawer.openDrawer(Gravity.LEFT);
    }

    public void openInThisContainer(Fragment whichWillOpened){

        try { // hide keyboard
            InputMethodManager inputManager = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Bundle additional = whichWillOpened.getArguments();

        if(additional==null)
            additional = new Bundle();

        additional.putBoolean(AppNavigation.BACKPRESS_ENABLE, true);

        whichWillOpened.setArguments(additional);


        getSupportFragmentManager().beginTransaction()
                .add(R.id.content_frame, whichWillOpened, addNewFragment(false))
                .setCustomAnimations(R.anim.slide_in_left,0,0,R.anim.slide_out_right)
                .show(whichWillOpened)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onBackPressed() {
        if(getSupportFragmentManager().getBackStackEntryCount()>0){
            LogSystem.logThis("НАЗАД ВО ФРАГМЕНТЕ");
            popNewFragment();
            getSupportFragmentManager().popBackStack();
        }
        else {
            super.onBackPressed();
        }
    }

    public int getFragmentCount() {
        return getSupportFragmentManager().getBackStackEntryCount();
    }

    public Fragment getFragmentAt(int index) {
        return getFragmentCount() > 0 ? getSupportFragmentManager().findFragmentByTag(Integer.toString(index)) : null;
    }

    public void savePoemaInternal(Poema currentPoema, String s) {
        CompletePoema completePoema = new CompletePoema(currentPoema, s);
        SugarPoemaDB.getInstance().save(completePoema);
    }
}
