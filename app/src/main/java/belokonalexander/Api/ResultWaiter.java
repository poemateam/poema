package belokonalexander.Api;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import belokonalexander.Api.InputQueryDataHolders.MassiveQueryInputData;
import belokonalexander.Api.Models.FriendsRelate;
import belokonalexander.Api.Models.Poema;
import belokonalexander.Api.Models.PoemaMessage;
import belokonalexander.Api.Models.QueryLog;
import belokonalexander.GlobalShell;

/**
 * Created by admin on 31.07.2016.
 */
public class ResultWaiter {

    /*
    using this conception can help cancel waiting loop
    Thread -> { While (ResultWaiter) -> doApiTask } -> return ResultWaiter::result
    You can interrupt While block and return null
    */

    private String requestDescription;
    private List<String> deletedCommand; // for cache
    private MassiveQueryInputData massiveQueryInputData;
    private boolean isEmptyCommand = false;

    public List<String> getDeletedCommand(){
        if(massiveQueryInputData!=null && massiveQueryInputData.getOffset()==0){
            return deletedCommand;
        }

        return null;
    }

    public Class getClassItem(){
        if(requestDescription.contains("Poema"))
            return Poema.class;
        else if(requestDescription.contains("Friendship")){
            return FriendsRelate.class;
        } else if(requestDescription.contains("Messages")){
            return PoemaMessage.class;
        }

        return null;
    }

    public void setEmptyCommand(boolean emptyCommand) {
        isEmptyCommand = emptyCommand;
    }

    public boolean isEmptyCommand(){
        return isEmptyCommand;
    }

    //this type of command whil deleted while insert new data
    public void addDeletedCommand(String action){
        if (deletedCommand==null)
            deletedCommand = new ArrayList<>();

        deletedCommand.add(GlobalShell.formAntonimCommand(requestDescription,action));
    }

    public Boolean isDeleteCommand(){
        if(GlobalShell.firstCamelWord(requestDescription).equals("delete")){
            return true;
        }
        else return false;
    }

    public Boolean isEditCommand(){
        if(GlobalShell.firstCamelWord(requestDescription).equals("edit")){
            return true;
        }
        else return false;
    }

    private String inputData;
    private Long startTime;
    private String parentHash;
    private String service;
    private Object result;

    public ResultWaiter(String parentHash) {
        this.parentHash = parentHash;
    }

    public ResultWaiter()
    {

    }

    public String getService() {
        return service;
    }

    public String getRequestDescription() {
        return requestDescription;
    }

    public MassiveQueryInputData getMassiveQueryInputData() {
        return massiveQueryInputData;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    private boolean interrupted = false;

    protected void setInterrupted()
    {
        interrupted = true;
    }

    public boolean isInterrupted() {
        return interrupted;
    }

    public void setMetaData(String requestDescription, MassiveQueryInputData massiveQueryInputData){
        setMetaData(requestDescription);
        this.massiveQueryInputData = massiveQueryInputData;
    }

    public void setMetaData(String requestDescription){
        this.requestDescription = requestDescription;
    }

    public String getInputData() {
        return inputData;
    }


    public void start(Object inputData, String serviceName)
    {
        startTime = System.currentTimeMillis();

        if(inputData == null)
        {
            this.inputData = "{empty}";
        }
          else if(inputData instanceof Map)
        {
            this.inputData = "Map: " + ((Map)inputData).keySet().toString();
        } else if(inputData instanceof File)
        {
            File file = ((File)inputData);
            this.inputData = "File: " + file.getAbsolutePath() + " [" + file.length() + " bytes]";
        }
          else this.inputData = "Object: " + inputData.toString();

        this.service = serviceName;
        parentHash+= "[" + serviceName + "]";

    }

    public void finish()
    {

        Long waitingTime = System.currentTimeMillis() - startTime;

        String outputData;
        if(getResult()==null)
            outputData = "canceled";
        else outputData = getResult().toString();

        QueryAnalyzer.showQueryResult(inputData, outputData,parentHash, waitingTime);


    }

}
