package ru.belokonalexander.poema.Receivers;

import com.backendless.push.BackendlessBroadcastReceiver;
import com.backendless.push.BackendlessPushService;

import ru.belokonalexander.poema.Services.CustomBackendlessPushService;

/**
 * Created by Alexander on 18.02.2017.
 */

public class BackendlessPushReceiver extends BackendlessBroadcastReceiver
{
    @Override
    public Class<? extends BackendlessPushService> getServiceClass()
    {
        return CustomBackendlessPushService.class;
    }
}