package belokonalexander.Api.Models.Annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by admin on 15.08.2016.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Table {
    String name();
}
