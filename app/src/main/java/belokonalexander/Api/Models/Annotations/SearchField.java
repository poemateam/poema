package belokonalexander.Api.Models.Annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by alexander on 13.08.2016.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface SearchField {
    boolean lazySearch();
    boolean fullContains() default false;
    String alias();
    int order() default 100;
}
