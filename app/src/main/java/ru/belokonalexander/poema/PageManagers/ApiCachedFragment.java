package ru.belokonalexander.poema.PageManagers;

import android.util.Log;
import android.view.ViewGroup;

import com.orm.SugarDb;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import StaticHelper.LogSystem;
import StaticHelper.Settings;
import belokonalexander.Api.ApiTask;
import belokonalexander.Api.LocalQueryExecutorExplicit;
import belokonalexander.Api.Models.SugarAdditional.SugarEvent;
import belokonalexander.Api.Models.SugarAdditional.SugarPoemaDB;
import belokonalexander.GlobalShell;
import ru.belokonalexander.poema.Fragments.FragmentContentWaiter;
import ru.belokonalexander.poema.Fragments.Profile;

/**
 * Created by alexander on 16.11.2016.
 */
public abstract class ApiCachedFragment extends ApiFragment{
    //TODO compare cache and received data (if they're different -> reshow view, else do nothing)

    ApiTask[] localQueries;

    boolean settingsApplied = false;

    boolean updateOnResume = true;

    public void setLocalSettings(ApiTask...localQueries) {
        this.localQueries = localQueries;
        settingsApplied = true;
    }

    public void setLocalSettings(boolean updateOnResume, ApiTask...localQueries) {
        this.localQueries = localQueries;
        settingsApplied = true;
        this.updateOnResume = updateOnResume;
    }

    //Class[] dependencies; // add strings-flags for events - if generated event with this flag - update cache data!

    List<Object> localAnswers;

    @Override
    public void buildFragment() {

        lastTaskWasCanceled = false;

        for(ApiTask localQuery : localQueries)
            localQuery.refresh();

        new BackendRunner(localAnswers -> {

            showView(localAnswers, true);

            this.localAnswers = localAnswers;

            refreshPage();

        }, message -> {
            new FragmentContentWaiter(getContext(), (ViewGroup) getRootView(), getToolbar(), answers -> showView(answers, false), backendTasks).startLoading();
        }, localQueries).run();

    }

    //add to list new predicted answer
    public void addToLocalData(Object object, int answerNum){
        ((List)localAnswers.get(answerNum)).add(object);
    }

    @Override
    public void refreshPage() {

        for(ApiTask backendTask : backendTasks)
            backendTask.refresh();

        startRefresh();
        new BackendRunner(queryAnswers -> {

                LogSystem.logThis("Сравниваю ответы : \n" + localAnswers + "\n" + queryAnswers);


                if (!GlobalShell.listsAreEquals(localAnswers, queryAnswers)) {




                    LogSystem.logThis("Loading...");
                    showView(queryAnswers, false);
                    localAnswers = queryAnswers;


                } else {
                    LogSystem.logThis("Equal results");
                    //need to show actions list
                }



            if(firstTask)
                firstTask = false;

            finishRefresh();


        }, messsage -> {
            finishRefresh();
        }, backendTasks).run();
    }

    boolean firstTask = true;

    @Override
    public void onResume() {
        super.onResume();




        if(firstTask) { //если не прогрузилось успешно
            if(lastTaskWasCanceled){
                buildFragment();
            }
            return;
        }


        if(!lastTaskWasCanceled) {
            if(updateOnResume)
                refreshPage();

        } else {
             buildFragment();
        }


    }

    boolean lastTaskWasCanceled = false;

    public void cancelTasks(){
        if(isTaskRunning()) {
            for (int i = 0; i < backendTasks.length; i++) {
                LogSystem.logThis("СБРОШЕН ЗАПРОС");
                backendTasks[i].cancel();
            }
            lastTaskWasCanceled = true;
        }
    }

    //boolean

    @Override
    public void onPause() {
        super.onPause();

        cancelTasks();
    }


    @Override
    public void onStop() {
        super.onStop();
        LogSystem.logThis(" ON STOP ");
    }


}
