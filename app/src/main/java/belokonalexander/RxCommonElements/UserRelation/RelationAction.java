package belokonalexander.RxCommonElements.UserRelation;

import belokonalexander.Api.Models.User;

/**
 * Created by alexander on 21.09.2016.
 */
public interface RelationAction {

    void execute(RelationApiMethod action);

}
