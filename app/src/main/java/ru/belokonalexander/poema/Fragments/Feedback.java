package ru.belokonalexander.poema.Fragments;

import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.OvershootInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.EditText;

import com.sevenheaven.segmentcontrol.SegmentControl;

import java.util.ArrayList;
import java.util.List;

import StaticHelper.LogSystem;
import StaticHelper.Settings;
import belokonalexander.Api.ApiItem;
import belokonalexander.Api.CurrentApi;
import belokonalexander.Api.QueryExecutorExplicit;
import belokonalexander.GlobalShell;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.belokonalexander.poema.CustomViews.Buttons.ButtonFlatPoema;
import ru.belokonalexander.poema.PageManagers.BackendRunner;
import ru.belokonalexander.poema.R;

import static StaticHelper.Settings.SHOW_ITEMS;
import static StaticHelper.Settings.SHOW_ITEMS_ANIMATE;

/**
 * Created by alexander on 13.08.2016.
 */
public class Feedback extends NavigationFragment{

    @BindView(R.id.app_toolbar)
    Toolbar toolbar;

    @BindView(R.id.segment_control)
    SegmentControl segmentControl;

    @BindView(R.id.feedback_message)
    EditText feedbackMessage;

    @BindView(R.id.send_feedback)
    ButtonFlatPoema sendButton;

    @BindViews({R.id.segment_control_wrapper, R.id.edit_text_wrapper, R.id.send_feedback})
    List<View> containers;

    @OnClick(R.id.send_feedback)
    void sendFeedback(){

        String message = feedbackMessage.getText().toString().trim();



        if(message.length()==0){
            LogSystem.makeToast(getActivity(),getContext().getResources().getString(R.string.please_write_the_message), LogSystem.ToastType.Info, LogSystem.TOAST_POSITION_CENTER);
            return;
        }

        sendButton.setEnabled(false);

        QueryExecutorExplicit sendMessageQuery = new QueryExecutorExplicit(GlobalShell.getParentHash(this.getClass()), resultWaiter -> CurrentApi.getInstanse().getApplicationApi().sendFeedback(CurrentApi.getInstanse().getCurrentUser().getObjectId(), category, message, resultWaiter));

        BackendRunner backendRunner = new BackendRunner(answers -> {

            LogSystem.makeToast(getActivity(),getContext().getResources().getString(R.string.message_was_sent), LogSystem.ToastType.Success, LogSystem.TOAST_POSITION_CENTER);
            feedbackMessage.setText("");
            sendButton.setEnabled(true);

        }, messsage -> {


            sendButton.setEnabled(true);

        }, sendMessageQuery);

        backendRunner.run();

    }

    String category = ApiItem.FeedbackCategory.OTHER.name();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if(rootView!=null)
            return rootView;

        View view = inflater.inflate(R.layout.fragment_feedback,container,false);
        ButterKnife.bind(this, view);
        fillToolbar(getString(R.string.poema_feedback));

        List<String> itemsValues = new ArrayList<>();
        List<String> itemsKeys = new ArrayList<>();
        int indexSelected = 0;
        int i = 0;
        for(ApiItem.FeedbackCategory category : ApiItem.FeedbackCategory.values()){
            itemsValues.add(category.name());
            itemsKeys.add(category.getKey(getContext()));
            if(category== ApiItem.FeedbackCategory.OTHER){
                indexSelected = i;
            }

            i++;
        }
        String [] keys = new String[i];
        itemsKeys.toArray(keys);

        segmentControl.setText(keys);
        segmentControl.setDirection(SegmentControl.Direction.HORIZONTAL);
        segmentControl.setSelectedIndex(indexSelected);
        segmentControl.setOnSegmentControlClickListener(index -> category = itemsValues.get(index));
        segmentControl.setColors(new ColorStateList(new int[][]{{}}, new int[]{ContextCompat.getColor(getContext(),R.color.colorAccentAddition)}));


        ButterKnife.apply(containers, SHOW_ITEMS_ANIMATE);

        rootView = view;
        return view;
    }



    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }


}
