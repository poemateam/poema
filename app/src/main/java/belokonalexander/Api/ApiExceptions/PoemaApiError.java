package belokonalexander.Api.ApiExceptions;

import com.backendless.exceptions.BackendlessException;
import com.backendless.exceptions.BackendlessFault;

import StaticHelper.LogSystem;

/**
 * Created by admin on 26.08.2016.
 */
public class PoemaApiError extends Exception {

    String message;
    int code = -1;
    String desc ="";
    Class itemType;
    Boolean resolvedStatus = false;

    public Class getItemType() {
        return itemType;
    }

    public PoemaApiError(BackendlessFault exception)
    {
        LogSystem.logThis("Ошибка: " + exception.getMessage());

        this.message = exception.getMessage();
        try {
            this.code = Integer.valueOf(exception.getCode());
        } catch (NumberFormatException e) {

        }
    }

    public PoemaApiError(BackendlessFault exception, Class type, String desc)
    {
        itemType = type;
        this.desc = desc;

        this.message = exception.getMessage();
        try {
            this.code = Integer.valueOf(exception.getCode());
        } catch (NumberFormatException e) {

        }
    }

    public PoemaApiError(BackendlessException exception) {

        this.message = exception.getMessage();
        try {
            this.code = Integer.valueOf(exception.getCode());
        } catch (NumberFormatException e) {

        }

    }

    public Boolean getResolvedStatus() {
        return resolvedStatus;
    }

    public void setResolvedStatus(Boolean resolvedStatus) {
        this.resolvedStatus = resolvedStatus;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public int getCode() {
        return code;
    }

    @Override
    public String toString() {
        return "[code:" + code +"]: " + message;
    }
}
