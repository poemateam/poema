package belokonalexander.Api.Models.SugarAdditional;

import com.orm.dsl.Table;

import belokonalexander.Api.CurrentApi;
import belokonalexander.Api.Models.FriendsRelate;
import belokonalexander.Api.Models.User;
import belokonalexander.GlobalShell;

/**
 * Created by alexander on 02.11.2016.
// */
//
public class MyFriendsRelate {

     public String relateObjectId;
     public Integer status;
     public Boolean myInitiation;



    public MyFriendsRelate(String relateObjectId, Integer status, Boolean myInitiation) {
        if(relateObjectId!=null)
            this.relateObjectId = relateObjectId;
        if(status!=null)
            this.status = status;
        if(myInitiation!=null)
            this.myInitiation = myInitiation;
    }

    public String getRelateObjectId() {
        return relateObjectId;
    }

    public Boolean getMyInitiation() {
        return myInitiation;
    }

    public String getObjectId() {
        return relateObjectId;
    }

    public Integer getStatus() {
        return status;
    }

    public boolean isMyInitiation() {
        return myInitiation;
    }

    @Override
    public String toString() {
        return "status:" + status;
    }

    @Override
    public boolean equals(Object obj) {

        if(obj instanceof MyFriendsRelate) {
            MyFriendsRelate friendsRelate = (MyFriendsRelate) obj;

            return GlobalShell.compareTwoObjects(relateObjectId,friendsRelate.relateObjectId) && GlobalShell.compareTwoObjects(status,friendsRelate.status)
                    && GlobalShell.compareTwoObjects(myInitiation,friendsRelate.myInitiation) ;

        }
        return false;

    }
}
