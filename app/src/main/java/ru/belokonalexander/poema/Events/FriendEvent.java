package ru.belokonalexander.poema.Events;

import belokonalexander.Api.Models.Poema;
import belokonalexander.Api.Models.User;

/**
 * Created by Alexander on 12.02.2017.
 */

public class FriendEvent {


    public static final String UPDATE = "update";
    public static final String DELETE = "delete";

    User user;

    String action;

    public FriendEvent(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User poema) {
        this.user = poema;
    }
}
