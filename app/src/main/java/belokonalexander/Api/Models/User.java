package belokonalexander.Api.Models;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.backendless.BackendlessUser;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import StaticHelper.LogSystem;
import StaticHelper.Settings;
import belokonalexander.Api.CurrentApi;
import belokonalexander.Api.Models.Annotations.Field;
import belokonalexander.Api.Models.Annotations.SearchField;
import belokonalexander.Api.Models.Annotations.Table;
import belokonalexander.Api.Models.SugarAdditional.MyFriendsRelate;
import belokonalexander.Api.Models.SugarAdditional.SugarPersistence;
import belokonalexander.Api.Models.SugarAdditional.SugarPoemaDB;
import belokonalexander.SharedAppPrefs;
import ru.belokonalexander.poema.Fragments.ListAdapters.SearchAdapter;

/**
 * Created by admin on 10.08.2016.
 */

@com.orm.dsl.Table(name = "Users")
@Table(name="Users")
public class User extends ApiEntity implements SearchEntityHolder, SugarPersistence{


    Long id;

    public Long getId() {
        return id;
    }



    //RELATION OBJECTS
    public String relateObjectId;
    public Integer relateStatus;
    public Boolean relateMyInitiation;





    @Field
    String objectId;

    @Field
    @SearchField(lazySearch = true, alias = "Pen name", order = 1)
    String name = "";

    @Field
    String status = "";

    @Field
    @SearchField(lazySearch = false, fullContains = true, alias = "Email")
    String email = "";

    @Field
    String image ="";

    @Field
    String small_image ="";

    @Field
    String address = "";

    @Field
    @SearchField(lazySearch = true, alias = "Name", order = 2)
    String real_name = "";

    @Field
    Date birthday = null;

    @Field
    String phone = "";

    @Field
    Boolean sex = null;

    @Field
    Date created;

    @Field
    Date updated;




    public User() {

    }

    public User(BackendlessUser backendlessUser){

        this.initFromMap(backendlessUser.getProperties());

    }

    public User(String s) {
        this.name = s;
    }



    public User(Map<String, Object> properties, String objectId) {
        initFromMap(properties);
        this.objectId = objectId;
    }


    //region getters
    public String getSmallImage() {
        return small_image; //  == null || small_image.length()==0 ? Settings.DEFAULT_IMAGE : small_image
    }

    public String getName() {
        return name;
    }

    public String getStatus() {
        return status;
    }

    public MyFriendsRelate getFriendsRelate() {

        return new MyFriendsRelate(relateObjectId,relateStatus,relateMyInitiation);
    }

    public String getEmail() {
        return email;
    }

    public String getImageUrl() {
        return image;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public String getReal_name() {
        return real_name;
    }

    public Date getBirthday() {
        return birthday;
    }




    public Calendar getBirthdayCalendar() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(birthday);
        return cal;
    }

    public String getStringBirthday() {
       return (birthday==null) ? "" : Settings.dateFormat.format(birthday);
        //return Settings.dateFormat.format(birthday);
    }

    public Boolean getSex() {
        return sex;
    }
    //endregion


    public String getObjectId()
    {
        return this.objectId;
    }

    @Override
    public void afterInit() {

    }

    //region setters

    public void setFriendsRelate(MyFriendsRelate friendsRelate) {

        this.relateStatus = friendsRelate.getStatus();
        this.relateObjectId = friendsRelate.getObjectId();
        this.relateMyInitiation = friendsRelate.isMyInitiation();


    }

    public void clearFriendsRelate(){
        this.relateStatus = null;
        this.relateObjectId = null;
        this.relateMyInitiation = null;
    }

    public void setFriendsRelate(FriendsRelate friendsRelate) {

        if(friendsRelate.getStatus()<0 ) {
            clearFriendsRelate();
            return;
        }

        this.relateStatus = friendsRelate.getStatus();
        this.relateObjectId = friendsRelate.getObjectId();
        this.relateMyInitiation = friendsRelate.isMyInitiation();

         if (!this.relateMyInitiation && this.relateStatus==0) {
             this.relateStatus = 1;
         }

    }

    public String setName(String name) {
        this.name = name;
        updateInSharedApp();
        return "name";
    }

    public String setStatus(String status) {
        this.status = status;
        updateInSharedApp();
        return "status";
    }

    public String setEmail(String email) {
        this.email = email;
        updateInSharedApp();
        return "email";
    }

    public String setImage(String image) {
        this.image = image;
        updateInSharedApp();
        return "image";
    }

    public String setSmall_image(String small_image) {
        this.small_image = small_image;
        updateInSharedApp();
        return "small_image";
    }

    public String setAddress(String address) {
        this.address = address;
        updateInSharedApp();
        return "address";
    }

    public String setReal_name(String real_name) {
        this.real_name = real_name;
        updateInSharedApp();
        return "real_name";
    }

    public void setObjectId(String objectId){
        this.objectId = objectId;
    }

    public String setBirthday(Date birthday) {
        this.birthday = birthday;
        updateInSharedApp();
        return "birthday";
    }

    public String setBirthday(String birthday) throws ParseException {
        this.birthday = Settings.dateFormat.parse(birthday);
        updateInSharedApp();
        return "birthday";
    }

    public String setSex(Boolean sex) {
        this.sex = sex;
        updateInSharedApp();
        return "sex";
    }

    public String setPhone(String phone) {
        this.phone = phone;
        updateInSharedApp();
        return "phone";
    }

    private void updateInSharedApp() {
        if(this.getObjectId()!=null && this.getObjectId().equals(CurrentApi.getInstanse().getCurrentUser().getObjectId()))
            SharedAppPrefs.getInstance().storeCurrentUser(this);
    }
    //endregion

    @Override
    public String toString() {

        return " USER ID: " + objectId;

    }

    @Override
    public RecyclerView.ViewHolder getSearchViewForItem(ViewGroup parent, SearchAdapter parentAdapter) {
        return UserSearchViewHolder.Builder(parent, parentAdapter);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, SearchedEntity entity, int pos, Context context) {
        UserSearchViewHolder.onBindViewHolder(holder,entity, pos, context);
    }

    public FriendsRelate.RelateStates getFriendStatusDesc(){
        FriendsRelate.RelateStates relateStates;

        if(relateStatus==null)
            relateStates = FriendsRelate.RelateStates.NONE;
        else
            switch (relateStatus){
                case 0:
                    relateStates = FriendsRelate.RelateStates.I_SEND_FRIENDSHIP;
                    break;
                case 1:
                    relateStates = FriendsRelate.RelateStates.I_GOT_FRIENDSHIP;
                    break;
                case 2:
                    relateStates = FriendsRelate.RelateStates.FRIENDS;
                    break;
                default:
                    relateStates = FriendsRelate.RelateStates.NONE;
                    break;
            }

        return relateStates;
    }

    public boolean isLocalUser(){
        return CurrentApi.getInstanse().getCurrentUser().getObjectId()==getObjectId();
    }


    @Override
    public boolean equals(Object obj) {

        // SUGAR'S CURSOR CALLING THIS METHOD AND @this can be null (idk why)
        if(this.getObjectId()==null)
            return super.equals(obj);

        if(obj instanceof User){
            return compareWithMainFields((User)obj);
        }
        else
            return super.equals(obj);
    }

    public Date getCreated() {
        return created;
    }

    public Date getUpdated() {
        return updated;
    }

    boolean compareWithMainFields(User obj) {

        return objectId.equals(obj.getObjectId()) && name.equals(obj.name) && status.equals(obj.status)
                && email.equals(obj.email) && image.equals(obj.image) && small_image.equals(obj.small_image)
                && address.equals(obj.address) && real_name.equals(obj.real_name) && ( (birthday==null && obj.birthday==null ) || birthday.getTime() == obj.birthday.getTime())
                && phone.equals(obj.phone) && sex==obj.sex && getFriendsRelate().equals(obj.getFriendsRelate());

        //relateObjectId.equals(obj.relateObjectId) && relateStatus.equals(obj.relateStatus) && relateMyInitiation.equals(obj.relateMyInitiation
    }

    @Override
    public void setUpdateDate(Date date) {
        lastUpdate = date;
    }

    @Override
    public Long getUpdateDate() {
        return lastUpdate.getTime();
    }

    @Override
    public void generateId() {
        /*String s = objectId;
        if(relateObjectId!=null){
            s+=relateObjectId;
        }*/
        this.id = CurrentApi.getInstanse().getLongHashOfItem(objectId);
    }

    @Override
    public void saveRelateObjects() {

    }

    public void updateInCache(FriendsRelate.RelateStates states) {
        LogSystem.logThis("STATE : " + states);
        //TODO
        if (states == FriendsRelate.RelateStates.NONE)
            SugarPoemaDB.getInstance().update(this, "deleteFriends?" + CurrentApi.getInstanse().getCurrentUser().getObjectId());
        if (states == FriendsRelate.RelateStates.FRIENDS || states == FriendsRelate.RelateStates.I_GOT_FRIENDSHIP) {
            SugarPoemaDB.getInstance().update(this, "createFriends?" + CurrentApi.getInstanse().getCurrentUser().getObjectId());
        }
    }

}
