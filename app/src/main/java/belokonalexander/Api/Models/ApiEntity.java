package belokonalexander.Api.Models;

import com.backendless.BackendlessUser;
import com.orm.dsl.Ignore;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import StaticHelper.LogSystem;
import belokonalexander.Api.CurrentApi;
import belokonalexander.Api.Models.Annotations.Field;
import belokonalexander.Api.Models.Annotations.Table;

/**
 * Created by alexander on 24.08.2016.
 */
public abstract class ApiEntity implements Serializable {

    @Ignore
    QueryLog queryLog;


    public QueryLog getQueryLog() {
        return queryLog;
    }

    public void setQueryLog(QueryLog queryLog) {
        this.queryLog = queryLog;
    }



    //last update in local db (updates with save)
    Date lastUpdate;

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    //TODO need is there objectId -> id convercet?
    public Map<String,Object> fillToMap(Map<String,Object> properties)
    {
        for(java.lang.reflect.Field field : this.getClass().getDeclaredFields())
        {
            if (field.isAnnotationPresent(Field.class))
            {
                try {

                    if((ApiEntity.class.isAssignableFrom((Class)field.getType()))){

                        HashMap<String,Object> sub = new HashMap<>();
                        ApiEntity entity = (ApiEntity) field.get(this);

                        //entity can be undefined
                        if(entity!=null) {
                            entity.fillToMap(sub);
                            Table table = entity.getClass().getAnnotation(Table.class);
                            sub.put("___class", table.name());
                            properties.put(field.getName(), sub);
                        }

                    } else if(List.class.isAssignableFrom((Class)field.getType())) { //p.s for list<ApiEntity>:

                        List<Object> outer = new ArrayList<>();
                        List list = (List) field.get(this);

                        if(list == null || list.isEmpty()){
                            // TODO: 10.01.2017
                        } else {
                            for(int i =0; i < list.size(); i++){

                                HashMap<String,Object> sub = new HashMap<>();
                                ApiEntity entity = (ApiEntity) list.get(i);
                                if(entity!=null) {
                                    entity.fillToMap(sub);
                                    Table table = entity.getClass().getAnnotation(Table.class);
                                    sub.put("___class", table.name());
                                    outer.add(sub);
                                }
                            }


                        }

                        properties.put(field.getName(), outer);

                    }
                    else


                        properties.put(field.getName(), field.get(this));

                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }




            }
        }

        return properties;
    }

    public void initFromMap(Map<String,Object> properties)
    {
        for(java.lang.reflect.Field field : this.getClass().getDeclaredFields())
        {
            if (field.isAnnotationPresent(Field.class))
            {
                    if(properties.get(field.getName())!=null) {
                        //если в Map нашли одноименное поле
                    try {
                        //это поле - объект?

                         if(properties.get(field.getName()).getClass().isArray()) { // related list object

                             List<ApiEntity> result = new ArrayList<>();

                             Object[] objects = (Object[]) properties.get(field.getName());

                             for(Object object : objects){

                                 ApiEntity entity = null;

                                 if(object instanceof BackendlessUser){
                                     entity = new User();
                                     entity.initFromMap(((BackendlessUser)object).getProperties());
                                     result.add(entity);
                                 }
                             }

                             field.set(this,result);

                         }
                            //sandalone Api object
                            else if(!isPrimitive(properties.get(field.getName()))) {

                            ApiEntity entity = null;
                            Object item = properties.get(field.getName());

                            if (item instanceof BackendlessUser){
                                entity = new User();
                                entity.initFromMap(((BackendlessUser)properties.get(field.getName())).getProperties());
                            } else if(item instanceof Map)  {

                                if(((Map)item).get("___class")!=null){

                                    String className = (String) ((Map)item).get("___class");
                                    if(className.equals("MessageLog"))
                                        className = "PoemaMessage";
                                    className = "belokonalexander.Api.Models." + className;
                                    Class c = Class.forName(className);
                                    entity = (ApiEntity) c.newInstance();
                                    entity.initFromMap((Map<String, Object>) item);

                                } else LogSystem.logThis("I don't know this object...");

                            }



                            field.set(this,entity);

                        }
                        else {

                            //LogSystem.logThis(" NAME: " + field.getName() + " / " + field.getName() + " = " + properties.get(field.getName()) + " -> " + properties.get(field.getName()).getClass());
                            field.set(this,properties.get(field.getName()));
                            // LogSystem.logThis("SAVED");

                            if(field.getName().equals("objectId")){
                                this.getClass().getDeclaredField("id").set(this, CurrentApi.getInstanse().getLongHashOfItem((String) properties.get(field.getName())));
                            }
                        }

                    } catch (IllegalAccessException e) {
                        LogSystem.logThis(" Ошибка при заполнении объекта: " + e.getMessage());
                        e.printStackTrace();
                    } catch (NoSuchFieldException e) {
                        //just skip not existing field
                    } catch (ClassNotFoundException e) {
                        LogSystem.logThis(" НЕТ КЛАССА: " + e.getMessage());
                        e.printStackTrace();
                    } catch (InstantiationException e) {
                        e.printStackTrace();
                    }

                    }
            }
        }

        afterInit();
    }

    private boolean isPrimitive(Object object) {

        return object instanceof Number || object instanceof Boolean || object instanceof String || object instanceof Date || object instanceof Array;

    }

    public Object getReflectPropertyValue(String property) {

        for(java.lang.reflect.Field field : this.getClass().getDeclaredFields())
        {
            if (field.isAnnotationPresent(Field.class) && field.getName().equals(property))
            {
                try {
                    return field.get(this);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }

        return null;
    }

    abstract public String getObjectId();

    abstract public void afterInit();
}
