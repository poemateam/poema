package belokonalexander.Api.Models;

import com.orm.dsl.Ignore;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import StaticHelper.LogSystem;
import belokonalexander.Api.CurrentApi;
import belokonalexander.Api.Models.Annotations.Field;
import belokonalexander.Api.Models.Annotations.Table;
import belokonalexander.Api.Models.SugarAdditional.SugarPersistence;
import belokonalexander.Api.Models.SugarAdditional.SugarPoemaDB;
import belokonalexander.GlobalShell;

/**
 * Created by alexander on 10.01.2017.
 */
@com.orm.dsl.Table(name = "Poems")
@Table(name="Poems")
public class Poema extends  ApiEntity implements SugarPersistence {


    Long id;


    public Poema(String title, User... users) {

        this.members = new ArrayList<>();
        for(int i =0; i < users.length; i++){
            members.add(users[i]);
        }

        updateMembersIds();


        this.title = title;
        getMemberTitleDesc();
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    @Ignore
    @Field
    PoemaMessage lastMessage;

    public void setLastMessage(PoemaMessage lastMessage) {
        this.lastMessage = lastMessage;
        lastMessageId = lastMessage.getObjectId();
        updated = lastMessage.getCreated();
    }

    public PoemaMessage getLastMessage() {
        return lastMessage;
    }

    @Field
    String objectId;

    @Ignore
    @Field
    List<User> members;

    String membersIds="";

    @Field
    String memberTitleDesc = "";

    private String lastMessageId;

    public void setLastMessageId(String lastMessageId) {
        this.lastMessageId = lastMessageId;
    }

    public String getLastMessageId() {
        return lastMessageId;
    }

    public List<User> getMembers() {
        return members;
    }

    public User getMember(String objectId) {
        for(User user : members) {
            if (user.getObjectId().equals(objectId))
                return user;
        }
        return null;
    }

    public void addMember(User user){
        if(members==null){
            members = new ArrayList<>();
        }

        members.add(user);
    }

    public String getMemberTitleDesc() {

        Long hash = 0L;

        if(memberTitleDesc==null || memberTitleDesc.length()==0){
            for(User member : members){
                hash = (hash + CurrentApi.getInstanse().getLongHashOfItem(member.getObjectId()))%Long.MAX_VALUE;
            }
            hash = (hash +  CurrentApi.getInstanse().getLongHashOfItem(title)) % Long.MAX_VALUE;

            memberTitleDesc = String.valueOf(hash);
        }



        return memberTitleDesc;
    }

    @Field
    String title;

    @Override
    public void setUpdateDate(Date date) {
        lastUpdate = date;
    }

    @Override
    public Long getUpdateDate() {
        return lastUpdate.getTime();
    }

    @Override
    public void generateId() {
        this.id = CurrentApi.getInstanse().getLongHashOfItem(objectId);
    }

    public String getMembersIds() {
        return membersIds;
    }

    @Override
    public void saveRelateObjects() {

        updateMembersIds();

        for(User user : members){
            user.setQueryLog(new QueryLog(user.getObjectId(),User.class,"fromPoema?"+getObjectId()));
        }

        SugarPoemaDB.getInstance().save(members);

        if(getLastMessage()!=null){
            PoemaMessage poemaMessage = getLastMessage();
            poemaMessage.setQueryLog(new QueryLog(poemaMessage.getObjectId(), PoemaMessage.class, "fromPoemaMessage?" + getObjectId()));
            SugarPoemaDB.getInstance().save(poemaMessage);
        }

    }

    public void updateInCache(){
        //LogSystem.logThis(" UPDATE IN CACHE POEMA");
        SugarPoemaDB.getInstance().update(this, "updatePoema?"+CurrentApi.getInstanse().getCurrentUser().getObjectId());
    }

    public Poema() {

    }

    @Override
    public String toString() {
        return "Poema{" +
                "id=" + id +
                ", objectId='" + objectId + '\'' +
                ", members=" + members +
                ", membersIds='" + membersIds + '\'' +
                ", memberTitleDesc='" + memberTitleDesc + '\'' +
                ", title='" + title + '\'' +
                ", created=" + created +
                ", updated=" + updated +
                ", lastMessage=" + lastMessageId +
                '}';
    }

    @Field
    Date created;

    @Field
    Date updated;

    public Date getUpdated() {
        return updated;
    }

    public Date getCreated() {
        return created;
    }

    public String getObjectId() {
        return objectId;
    }

    @Override
    public void afterInit() {
        if(getLastMessage()!=null)
            lastMessageId = getLastMessage().getObjectId();
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;

    }

    public void setMembers(List<User> members) {
        this.members = members;
        updateMembersIds();

    }

    @Override
    public boolean equals(Object obj) {

        if(this.getObjectId()==null)
            return super.equals(obj);

        if(obj instanceof Poema){
            return compareWithPoema((Poema)obj);
        }
        else
            return super.equals(obj);
    }

    private boolean compareWithPoema(Poema obj) {
        return this.getMemberTitleDesc().equals(obj.getMemberTitleDesc()) && GlobalShell.compareTwoObjects(getLastMessage(),obj.getLastMessage());
    }

    private void updateMembersIds() {
        membersIds = "";
        for(int i =0; i < members.size(); i++){
            if(i==0){
                membersIds+= members.get(i).getObjectId();
            } else  {
                membersIds+= "," + members.get(i).getObjectId();
            }
        }
    }

    public List<String> getUsersIds(){
        if(membersIds.length()==0){
            if(members!=null && !members.isEmpty())
                updateMembersIds();
            else return null;
        }

        return new ArrayList<>(Arrays.asList(membersIds.split(",")));

    }

    //TODO while for 2 users
    public User getAnotherUser(){

        for(User user : getMembers()){
            if(!user.getObjectId().equals(CurrentApi.getInstanse().getCurrentUser().getObjectId()))
                return user;
        }

        return null;
    };

}
