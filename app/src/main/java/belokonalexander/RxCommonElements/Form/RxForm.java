package belokonalexander.RxCommonElements.Form;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.Toast;

import com.dd.processbutton.iml.ActionProcessButton;
import com.jakewharton.rxbinding.view.RxView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import StaticHelper.LogSystem;
import StaticHelper.Settings;
import belokonalexander.Api.ApiExceptions.PoemaApiError;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by admin on 31.07.2016.
 */
public class RxForm {

    private ActionProcessButton mainButton;

    private FormAction formAction;

    private FormValidator formValidator;

    private Context context;



    public static RxForm create(Context context, ActionProcessButton mainButton, List<? extends View> formViews, FormAction formAction)
    {
        RxForm form = new RxForm(context, mainButton, formViews, formAction);

        return form;
    }

    private RxForm(Context context, ActionProcessButton mainButton, List<? extends View> formViews, FormAction formAction)
    {
        this.mainButton = mainButton;
        this.mainButton.setMode(ActionProcessButton.Mode.ENDLESS);
        this.formAction = formAction;
        this.context = context;


        List<ValidationField> validationFields = new ArrayList<>();

        //TODO textfield flow
        //create validationFields array and logic
        int i =0;
        for(View view : formViews)
        {
            if(view instanceof ValidationField)
            {
                validationFields.add((ValidationField)view);
            }

            if(view instanceof EditText){
                ((EditText)view).setOnEditorActionListener((v, actionId, event) -> {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        mainButton.callOnClick();
                        return true;
                    }
                    return false;
                });
            }

            i++;
        }

        this.formValidator = new FormValidator(context,validationFields);

        RxView.clicks(mainButton).throttleFirst(Settings.THROTTLE_CLICK_VALUE, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(func -> Observable.create(obs -> {
                    if (!formValidator.checkFormFields())
                        obs.onNext(new Exception("Validation Exception"));
                    else obs.onNext(true);
                    obs.onCompleted();
                }))
                .filter(s -> {
                    if (s instanceof Throwable) return false;
                    else {
                        mainButton.setProgress(50);
                        return true;
                    }
                })
                .observeOn(Schedulers.newThread())

                .flatMap(func -> Observable.create(s -> {
                    s.onNext(formAction.actionStart());
                    s.onCompleted();
                }))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    if (result instanceof Exception) {

                        mainButton.setProgress(-1);
                        formAction.failedAction(((Exception) result));

                    } else {

                        if (result == null) {

                        } else {
                            mainButton.setProgress(0);
                            formAction.succeedAction(result);
                        }
                    }
                });

    }



}
