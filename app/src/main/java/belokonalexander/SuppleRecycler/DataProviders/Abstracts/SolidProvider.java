package belokonalexander.SuppleRecycler.DataProviders.Abstracts;

import java.util.List;

import StaticHelper.*;
import belokonalexander.Api.InputQueryDataHolders.MassiveQueryInputData;
import belokonalexander.Api.QueryExecutor;
import belokonalexander.SuppleRecycler.ActionRecycler;

/**
 * Created by alexander on 06.10.2016.
 */
//provide data with non controlling size
abstract public class SolidProvider<T>  {

      abstract public List<T> getData(MassiveQueryInputData state);

      //SHOULDN'T RETURN NULL DATA. IT'S ALWAYS RETURN SOME OBJECT
      abstract public List<T> getCacheData(int pageSize);

}
