package belokonalexander.Api;

import java.util.ArrayList;
import java.util.List;

import StaticHelper.LogSystem;

/**
 * Created by alexander on 23.08.2016.
 */
public class QueryExecutorExplicit extends QueryExecutor implements ApiTask{
    //WARNING!!! THIS CODE IS SYNC


    public QueryExecutorExplicit(String parentHash, Actions queryAction) {
        super(parentHash);
        this.queryAction = queryAction;
    }

    public QueryExecutorExplicit(String parentHash, boolean unique, Actions queryAction) {
        super(parentHash, unique);
        this.queryAction = queryAction;
    }

    public QueryExecutorExplicit(String parentHash,  QueryExecutor.Actions queryAction, boolean selfInitialization) {
        super(parentHash);
        this.queryAction = queryAction;

        if(selfInitialization){
            refresh();
        }
    }

    Actions queryAction;

    public Object execute() {
        return super.execute(queryAction);
    }

    @Override
    public void refresh() {
        String hash = getParentHash();
        resultWaiter = new ResultWaiter(hash);
        isInterrupt = false;
    }

    @Override
    public void cancel() {
        resultWaiter.setInterrupted();
        interruptThis();
    }

    public Actions getQueryAction() {
        return queryAction;
    }
}
