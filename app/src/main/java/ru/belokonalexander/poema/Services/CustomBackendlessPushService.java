package ru.belokonalexander.poema.Services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.backendless.push.BackendlessPushService;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import StaticHelper.LogSystem;
import StaticHelper.Settings;
import belokonalexander.Api.Models.PoemaMessage;
import belokonalexander.Api.Models.QueryLog;
import belokonalexander.Api.Models.SugarAdditional.MyFriendsRelate;
import belokonalexander.Api.Models.SugarAdditional.SugarPoemaDB;
import belokonalexander.Api.Models.User;
import belokonalexander.GlobalShell;
import belokonalexander.SharedAppPrefs;
import ru.belokonalexander.poema.Events.FriendEvent;
import ru.belokonalexander.poema.Events.MessageEvent;
import ru.belokonalexander.poema.Fragments.Friends;
import ru.belokonalexander.poema.Fragments.PoemsMenu;
import ru.belokonalexander.poema.MainActivity;
import ru.belokonalexander.poema.PoemaApplication;
import ru.belokonalexander.poema.R;
import ru.belokonalexander.poema.SplashScreen;

import static ru.belokonalexander.poema.Services.BackendlessMessagingService.OPERATION_DESC;

public class CustomBackendlessPushService extends BackendlessPushService
{

    public static final int MESSAGE_ID = 10002;
    public static final int FRIENDS_ID = 10003;

    @Override
    public void onRegistered(Context context, String registrationId )
    {
        //Toast.makeText( context, "device registered" + registrationId, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUnregistered( Context context, Boolean unregistered )
    {
        //Toast.makeText( context, "device unregistered", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onMessage( Context context, Intent intent )
    {
        for(HandlersChain handler : HandlersChain.values()) {
            if(handler.handle(context,intent))
                break;
        }

        return false;
    }



    @Override
    public void onError( Context context, String message )
    {
        Toast.makeText( context, message, Toast.LENGTH_SHORT).show();
    }


    enum HandlersChain{

        MESSAGE {
            @Override
            boolean handle(Context context, Intent intent) {

                if(intent.getStringExtra("type")!=null)
                    return false;

                String message = intent.getStringExtra("message");
                Long time = intent.getLongExtra("google.sent_time", 0);
                String poemaId = intent.getStringExtra("poemaId");
                String publisherId = intent.getStringExtra("BL_PUBLISHER_ID");
                String messageId = intent.getStringExtra("messageId");

                //TODO message
                PoemaMessage msg = new PoemaMessage(messageId, message, poemaId, publisherId, time);
                msg.setQueryLog(new QueryLog(msg.getObjectId(), PoemaMessage.class, OPERATION_DESC + "?" + msg.getPoema()));
                SugarPoemaDB.getInstance().save(msg);
                List<PoemaMessage> poemaMessages = new ArrayList<>();
                poemaMessages.add(msg);
                SugarPoemaDB.getInstance().save(poemaMessages);
                for (PoemaMessage m : poemaMessages) {
                    SugarPoemaDB.getInstance().updatePoemaLastMessage(m.getPoema(), m.getObjectId());
                }

                EventBus.getDefault().post(new MessageEvent(poemaMessages));

                if (((PoemaApplication) context.getApplicationContext()).isAppActive()) {
                    liveNotify(context);
                } else {
                    NotificationCompat.Builder builder = getNotificationBuilder(context,context.getResources().getString(R.string.you_have_some_answers), msg.getMessage(), null, PoemsMenu.class);
                    if(builder!=null){
                        NotificationManager mNotificationManager =
                                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                        mNotificationManager.notify(MESSAGE_ID, builder.build());
                    }
                }

                return true;
            }
        },
        FRIENDSHIP {
            @Override
            boolean handle(Context context, Intent intent) {

                if(intent.getStringExtra("type")==null || !intent.getStringExtra("type").equals("friendship"))
                    return false;

                LogSystem.logThis(" FRIENDSHIP EVENT !!! : " + intent.getExtras());

                String title = intent.getStringExtra("message"); // name
                String avatar = intent.getStringExtra("imageAvatar");
                String objectId = intent.getStringExtra("userObjectId");
                String relateObjectId = intent.getStringExtra("relateObjectId");
                String text = context.getResources().getString(R.string.offers_friendship);

                Bitmap avatarBitmap = GlobalShell.getBitmapFromURL(avatar);



                    User user = new User();
                    user.setName(title);
                    user.setObjectId(objectId);

                    user.setSmall_image(avatar);
                    user.setFriendsRelate(new MyFriendsRelate(relateObjectId,1,false));
                    //user
                    EventBus.getDefault().post(new FriendEvent(user));




                if (((PoemaApplication) context.getApplicationContext()).isAppActive()) {
                    //liveNotify(context);
                } else {
                    NotificationCompat.Builder builder = getNotificationBuilder(context,title, text, avatarBitmap, Friends.class);
                    if(builder!=null){
                        NotificationManager mNotificationManager =
                                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                        mNotificationManager.notify(FRIENDS_ID, builder.build());
                    }
                }

                return true;
            }
        };

        abstract boolean handle(Context context, Intent intent);

        public void liveNotify(Context context){
            if (SharedAppPrefs.getInstance().getUsingNotification()) {

                if (SharedAppPrefs.getInstance().getUsingNotificationsVibro()) {
                    try {
                        long[] pattern = {100, 100, 100, 100};
                        Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
                        vibrator.vibrate(pattern, -1);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if (SharedAppPrefs.getInstance().getUsingNotificationsSound()) {
                    try {
                        Uri notification = Uri.parse("android.resource://" + context.getPackageName() + "/raw/notification");
                        Ringtone r = RingtoneManager.getRingtone(context, notification);
                        r.play();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        public NotificationCompat.Builder getNotificationBuilder(Context context, String title, String text, Bitmap largeIcon, Class target){
            if (SharedAppPrefs.getInstance().getUsingNotification()) {
                NotificationCompat.Builder mBuilder =
                        new NotificationCompat.Builder(context)
                                .setSmallIcon(R.mipmap.ic_launcher)
                                .setLargeIcon(largeIcon==null ? BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher) : largeIcon)
                                .setContentTitle(title)
                                .setContentText(text)
                                .setAutoCancel(true);

                if (SharedAppPrefs.getInstance().getUsingNotificationsVibro()) {
                    long[] pattern = {100, 100, 100, 100};
                    mBuilder.setVibrate(pattern);
                }

                if (SharedAppPrefs.getInstance().getUsingNotificationsSound()) {
                    Uri alarmSound = Uri.parse("android.resource://" + context.getPackageName() + "/raw/notification");
                    mBuilder.setSound(alarmSound);
                }


                // mId allows you to update the notification later on.
                Intent resultIntent = new Intent(context, SplashScreen.class);
                resultIntent.putExtra("PATH",target.getCanonicalName());
                mBuilder.setContentIntent(PendingIntent.getActivity(context, 0, resultIntent, PendingIntent.FLAG_CANCEL_CURRENT));
                return mBuilder;
            }

            return null;
        }
    }








}
