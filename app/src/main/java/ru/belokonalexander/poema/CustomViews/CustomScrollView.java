package ru.belokonalexander.poema.CustomViews;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;

import ru.belokonalexander.poema.Interfaces.OnScrollViewListener;

/**
 * Created by admin on 16.08.2016.
 */
public class CustomScrollView extends ScrollView {

    public CustomScrollView(Context context) {
        super(context);
    }

    public CustomScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    private OnScrollViewListener onScrollViewListener;

    public void setOnScrollViewListener(OnScrollViewListener onScrollViewListener) {
        this.onScrollViewListener = onScrollViewListener;
    }


    @Override
    protected void onScrollChanged(int x, int y, int oldl, int oldt) {
        currentScrollY = y;
        onScrollViewListener.onScrollChanged( this, x, y, oldl, oldt );
        super.onScrollChanged( x, y, oldl, oldt );

    }

    private int currentScrollY = 0;

    public int getCurrentScrollY() {
        return currentScrollY;
    }

}
