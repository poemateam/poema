package ru.belokonalexander.poema.Fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.view.menu.ActionMenuItemView;
import android.support.v7.widget.ActionMenuView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.EditText;
import android.widget.ImageButton;


import com.backendless.Backendless;
import com.backendless.messaging.PublishOptions;


import com.liucanwen.app.headerfooterrecyclerview.HeaderAndFooterRecyclerViewAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import StaticHelper.LogSystem;
import StaticHelper.Settings;
import belokonalexander.Api.CurrentApi;
import belokonalexander.Api.Models.LastMessageCache;
import belokonalexander.Api.Models.Poema;
import belokonalexander.Api.Models.PoemaMessage;
import belokonalexander.Api.QueryExecutorExplicit;
import belokonalexander.GlobalShell;
import belokonalexander.SuppleRecycler.ActionRecycler;
import belokonalexander.SuppleRecycler.AutoRefreshActionRecycler;
import belokonalexander.SuppleRecycler.DataProviders.Abstracts.GetPoemMessagesProvider;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.belokonalexander.poema.CustomViews.Buttons.ButtonFlatPoema;
import ru.belokonalexander.poema.Events.MessageEvent;
import ru.belokonalexander.poema.Events.MessageStatusEvent;
import ru.belokonalexander.poema.Events.PoemaEvent;
import ru.belokonalexander.poema.Fragments.ListAdapters.PoemsMessagesAdapter;
import ru.belokonalexander.poema.MainActivity;
import ru.belokonalexander.poema.PageManagers.BackendRunner;
import ru.belokonalexander.poema.PageManagers.Types.ListData;
import ru.belokonalexander.poema.R;

/**
 * Created by Alexander on 25.01.2017.
 */

public class PoemsMessages extends NavigationFragment implements ListData{

    @BindView(R.id.app_toolbar)
    Toolbar toolbar;

    @BindView((R.id.message_text_view))
    EditText messageTextView;

    PublishOptions publishOptions;

    @BindView(R.id.send_message_button)
    ButtonFlatPoema sendMessage;

    @BindView(R.id.messages_recycler)
    AutoRefreshActionRecycler<PoemaMessage> recyclerMyMessage;

    LastMessageCache lastMessageCache;

    @OnClick(R.id.send_message_button)
    void sendMessage(){


        if(!recyclerMyMessage.getDataAdapter().getData().isEmpty() && recyclerMyMessage.getDataAdapter().getData().get(0)!=null && recyclerMyMessage.getDataAdapter().getData().get(0).isMyMessage()){

            LogSystem.makeToast(getActivity(),getContext().getResources().getString(R.string.poema_order), LogSystem.ToastType.Info, LogSystem.TOAST_POSITION_CENTER);
            return;
        }

        GlobalShell.tryCloseKeyboard(getActivity());
        messageTextView.clearFocus();

        String poemaMessage =  messageTextView.getText().toString().trim();

        if(currentPoema==null || poemaMessage.length()==0)
            return;

        PoemaMessage message = new PoemaMessage("",poemaMessage,currentPoema.getObjectId(),CurrentApi.getInstanse().getCurrentUser().getObjectId());
        addNewMessage(message);

        messageTextView.setText("");

        QueryExecutorExplicit sendMessageQuery = new QueryExecutorExplicit(GlobalShell.getParentHash(this.getClass()), resultWaiter -> CurrentApi.getInstanse().getApplicationApi().sendMessage(currentPoema, poemaMessage, resultWaiter));

        BackendRunner backendRunner = new BackendRunner(answers -> {

            lastMessageCache.delete();
            recyclerMyMessage.replaceWith(message, (PoemaMessage) answers.get(0), true);
            currentPoema.setLastMessage((PoemaMessage) answers.get(0));

            EventBus.getDefault().post(new PoemaEvent(currentPoema));

        }, messsage -> {
            recyclerMyMessage.removeItem(message);
            messageTextView.setText(poemaMessage);
        }, sendMessageQuery);

        backendRunner.run();

    }

    Poema currentPoema;
    View savePoemaView;

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(getRootView()!=null)
            return getRootView();
        View view = inflater.inflate(R.layout.fragments_poems_messages,container,false);
        ButterKnife.bind(this, view);

        //todo async
        //currentPoema = SugarPoemaDB.getInstance().getPoema(getArguments().getString("POEMA_ID"));

        currentPoema = EventBus.getDefault().getStickyEvent(Poema.class);
        lastMessageCache = new LastMessageCache(currentPoema.getObjectId());
        messageTextView.setText(lastMessageCache.getText(true));
        messageTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                lastMessageCache.setText(s.toString());
                lastMessageCache.update();
            }
        });

        if(currentPoema != null) {
            EventBus.getDefault().removeStickyEvent(currentPoema);
        }

        fillToolbar(currentPoema.getTitle()+" (" + currentPoema.getAnotherUser().getName() + ")");
        setRootView(view);



        MenuItem savePoemaLocal = getToolbar().getMenu().add(0,10,10,getContext().getResources().getString(R.string.save));
        savePoemaLocal.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        savePoemaLocal.setIcon(R.drawable.ic_cloud_download_white_36dp);
        savePoemaLocal.setOnMenuItemClickListener(item -> {
            getAndSaveAllPoema(currentPoema.getObjectId());
            return false;
        });


        List<PoemaMessage> poemaMessages = new ArrayList<>();
        //adapter =
        LinearLayoutManager llm = new LinearLayoutManager(getContext());

        llm.setReverseLayout(true);



        recyclerMyMessage.setLayoutManager(llm);


        HeaderAndFooterRecyclerViewAdapter adapterWithHeader = new HeaderAndFooterRecyclerViewAdapter(new PoemsMessagesAdapter(poemaMessages,this, currentPoema));

        recyclerMyMessage.setAdapter(adapterWithHeader);
        recyclerMyMessage.init(toolbar,true);
        recyclerMyMessage.setProvider(new GetPoemMessagesProvider(currentPoema));
        recyclerMyMessage.setAfterSuccessInit(this::readMessages);

        recyclerMyMessage.getData(ActionRecycler.UpdateMode.INITIAL);


        ActionMenuView amv = (ActionMenuView) getToolbar().getChildAt(getToolbar().getChildCount()-1);
        savePoemaView = amv.getChildAt(0);


        return view;
    }

    private void getAndSaveAllPoema(String objectId) {
        savePoemaView.setEnabled(false);


        AnimationSet animationSet = new AnimationSet(true);

        Animation animationRotate = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, //fromXType
                0.0f,                       //fromXValue
                Animation.RELATIVE_TO_SELF, //toXType
                0.0f,                      //toXValue
                Animation.RELATIVE_TO_SELF, //fromYType
                0.0f,                       //fromYValue
                Animation.RELATIVE_TO_SELF, //toYType
                0.05f);

        //Animation animationRotate = new RotateAnimation(0,360,Animation.RELATIVE_TO_SELF, .5f, Animation.RELATIVE_TO_SELF, .5f);
        animationRotate.setDuration(Settings.REFRESH_TRANSLATE_ANIMATION_DURATION);
        animationRotate.setRepeatMode(Animation.REVERSE);
        animationRotate.setRepeatCount(Animation.INFINITE);
        animationSet.addAnimation(animationRotate);
        savePoemaView.startAnimation(animationSet);


        QueryExecutorExplicit savePoemaLocal = new QueryExecutorExplicit(GlobalShell.getParentHash(this.getClass()), resultWaiter -> CurrentApi.getInstanse().getApplicationApi().getAllPoema(objectId, resultWaiter), true);
        BackendRunner backendRunner = new BackendRunner(answers -> {
            ((MainActivity) getActivity()).savePoemaInternal(currentPoema, answers.get(0).toString());
            savePoemaView.setEnabled(true);
            savePoemaView.clearAnimation();
            LogSystem.makeToast(getActivity(),getContext().getResources().getString(R.string.success_save_poema), LogSystem.ToastType.Success, LogSystem.TOAST_POSITION_BOTTOM);
        }, messsage -> {
            savePoemaView.setEnabled(true);
            savePoemaView.clearAnimation();
            LogSystem.makeToast(getActivity(),getContext().getResources().getString(R.string.poema_is_empty), LogSystem.ToastType.Error, LogSystem.TOAST_POSITION_BOTTOM);

        }, savePoemaLocal);

        backendRunner.run();
    }

    private void readMessages() {

        QueryExecutorExplicit readMessages = new QueryExecutorExplicit(GlobalShell.getParentHash(this.getClass()), resultWaiter -> CurrentApi.getInstanse().getApplicationApi().readMessage(currentPoema, resultWaiter), true);
        BackendRunner backendRunner = new BackendRunner(answers -> {
            //TODO READ MESSAGE STATUS
            if(answers.get(0) instanceof PoemaMessage) {
                EventBus.getDefault().post(new MessageStatusEvent((PoemaMessage) answers.get(0)));
            }
        }, messsage -> {
        }, readMessages);

        if(currentPoema.getLastMessage()!=null && currentPoema.getLastMessage().getActual())
            backendRunner.run();
    }

    public void addNewMessage(PoemaMessage message){
        recyclerMyMessage.addFirstItem(message);
    }


   @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNewMessagesEvent(MessageEvent messages){

        List<PoemaMessage> forThisPoema = new ArrayList<>();

        for(PoemaMessage poemaMessage : messages.getPoemaMessage()){
            if(poemaMessage.getPoema().equals(currentPoema.getObjectId())){
                forThisPoema.add(poemaMessage);
            }
        }

        if(!forThisPoema.isEmpty()){

            //сортирую сообщения в обратном порядке 3 2 1
            Collections.sort(forThisPoema, (o1, o2) -> {
                long first = o1.getCreated().getTime();
                long second =  o2.getCreated().getTime();
                return (first>second) ? 1 : (first<second) ? -1 : 0;
            });


            recyclerMyMessage.insertInStart(forThisPoema);

            readMessages();


        }


    }

    @Override
    public ActionRecycler getActionRecycler() {
        return recyclerMyMessage;
    }
}
