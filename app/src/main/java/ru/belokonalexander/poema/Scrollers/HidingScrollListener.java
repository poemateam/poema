package ru.belokonalexander.poema.Scrollers;

import android.support.v7.widget.RecyclerView;

import StaticHelper.LogSystem;
import belokonalexander.GlobalShell;

/**
 * Created by alexander on 30.08.2016.
 */
public abstract class HidingScrollListener extends RecyclerView.OnScrollListener {
    private static final int HIDE_THRESHOLD = 30;
    private int scrolledDistance = 0;
    private boolean controlsVisible = true;


    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        if (scrolledDistance > HIDE_THRESHOLD && controlsVisible) {
                if(onHide()) {
                    controlsVisible = false;
                    scrolledDistance = 0;
                }

        } else if (scrolledDistance < -HIDE_THRESHOLD && !controlsVisible) {

                onShow();
                controlsVisible = true;
                scrolledDistance = 0;

        }

        if((controlsVisible && dy>0) || (!controlsVisible && dy<0)) {
            scrolledDistance += dy;
        }
    }

    public abstract boolean onHide(); // because of we set min full scroll value from another class
    public abstract void onShow();

    public void reset(){
        onShow();
        controlsVisible = true;
        scrolledDistance = 0;

    }


}
