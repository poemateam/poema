package BorrowedSolutions;

import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageView;

/**
 * Created by alexander on 14.10.2016.
 */

public class HideAnimationLIstener implements Animation.AnimationListener {
    ImageView view;
    public void setImage(ImageView view) {
        this.view = view;
    }
    public void onAnimationEnd(Animation animation) {
        // Do whatever you want
        view.setVisibility(View.GONE);
    }
    public void onAnimationRepeat(Animation animation) {
    }
    public void onAnimationStart(Animation animation) {
    }
}