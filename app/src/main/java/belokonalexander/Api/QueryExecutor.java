package belokonalexander.Api;

import java.util.ArrayList;
import java.util.List;

import StaticHelper.LogSystem;
import StaticHelper.Settings;

/**
 * Created by alexander on 23.08.2016.
 */
public class QueryExecutor {
    // SYNC METHOD

    public static List<QueryExecutor> currentTasksStack = new ArrayList<>();


    public static synchronized void addTask(QueryExecutor waiterShell) {

        currentTasksStack.add(waiterShell);

    }

    public static synchronized void removeTask(QueryExecutor waiterShell) {
        currentTasksStack.remove(waiterShell);
    }


    boolean isRunning = false;
    boolean isInterrupt = false;

    ResultWaiter resultWaiter;

    private String parentHash;

    public String getParentHash() {
        return parentHash;
    }

    public void setParentHash(String parentHash) {
        this.parentHash = parentHash;
    }

    public QueryExecutor(String parentHash) {

        setParentHash(parentHash);
        //interruptPreviouslyThis();
        this.resultWaiter = new ResultWaiter(parentHash);
    }

    public QueryExecutor(String parentHash, boolean unique) {
        parentHash = parentHash + "["+(currentTasksStack.size()+1)+"]";
        setParentHash(parentHash);
        //interruptPreviouslyThis();
        this.resultWaiter = new ResultWaiter(parentHash);
    }


    private void interruptPreviouslyThis() {
        for(QueryExecutor item : currentTasksStack)
        {
            if(item.getParentHash().equals(getParentHash()))
            {
                LogSystem.logThis("INTERRUPTED FROM " + Thread.currentThread().getName());
                item.interruptThis();
            }
        }
    }

    public void interruptThis()
    {
        isInterrupt = true;
    }

    public boolean isRunning() {
        return isRunning;
    }

    private Actions actions;


    public Object execute(Actions actions)
    {
        interruptPreviouslyThis();

        isRunning = true;
        addTask(this);

        //! main task should update resultWaiter.result field AND should be ASYNC
        actions.mainTask(this.resultWaiter);

        while (resultWaiter.getResult()==null)
        {



            if(isInterrupt)
            {
                this.resultWaiter.setInterrupted();
                LogSystem.logThis("INTERRUPT THIS " + Thread.currentThread().getName() + " ? " + this.getParentHash());


                break;
            }

            try {
                Thread.sleep(Settings.SLEEP_EXECUTOR_TIME);
               // LogSystem.logThis(" Running... " + this.getParentHash() + "/ " + currentTasksStack.size());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        isRunning = false;
        removeTask(this);
        resultWaiter.finish();

        return resultWaiter.getResult();
    }



    public interface Actions
    {
        void mainTask(ResultWaiter resultWaiter);
    }


}
