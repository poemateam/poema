package ru.belokonalexander.poema.Fragments;

import android.os.Bundle;

/**
 * Created by Alexander on 24.01.2017.
 */

public interface BundleKeeper {
    Bundle getBundle();
}
