package Helper;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Alexander on 02.03.2017.
 */

public class ClickableExpandableTextView extends ExpandableTextDecorator{
    public ClickableExpandableTextView(Context context) {
        super(context);
    }

    public ClickableExpandableTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ClickableExpandableTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public interface AdditionListener{
        void onClick(View view);
    }

    AdditionListener additionListener;

    @Override
    public void onClick(View view) {
        super.onClick(view);
        if(additionListener!=null)
            additionListener.onClick(view);

    }

    public void setAdditionListener(AdditionListener additionListener) {
        this.additionListener = additionListener;
    }

    public TextView getTextView(){
        return mTv;
    }
}
