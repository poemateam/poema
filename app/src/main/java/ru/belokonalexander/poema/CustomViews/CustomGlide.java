package ru.belokonalexander.poema.CustomViews;

import android.content.Context;

import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.integration.okhttp3.OkHttpUrlLoader;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.module.GlideModule;

import java.io.File;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

/**
 * Created by alexander on 14.10.2016.
 */
public class CustomGlide implements GlideModule {
    @Override
    public void applyOptions(Context context, GlideBuilder builder) {


    }




    @Override public void registerComponents(Context context, Glide glide) {
        // register ModelLoaders here.
        OkHttpClient.Builder client = new OkHttpClient.Builder()
                .connectTimeout(45, TimeUnit.SECONDS)
                .readTimeout(45, TimeUnit.SECONDS)
                .writeTimeout(45, TimeUnit.SECONDS);

        OkHttpUrlLoader.Factory factory=new OkHttpUrlLoader.Factory(client.build());

        glide.register(GlideUrl.class, InputStream.class, factory);
    }
}
