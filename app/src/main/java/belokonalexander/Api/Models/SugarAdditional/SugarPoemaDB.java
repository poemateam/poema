package belokonalexander.Api.Models.SugarAdditional;

import android.content.Context;
import android.util.Log;

import com.orm.SchemaGenerator;
import com.orm.SugarContext;
import com.orm.SugarDb;
import com.orm.SugarRecord;
import com.orm.util.Collection;
import com.orm.util.SugarConfig;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import StaticHelper.LogSystem;
import belokonalexander.Api.CurrentApi;
import belokonalexander.Api.InputQueryDataHolders.MassiveQueryInputData;
import belokonalexander.Api.InputQueryDataHolders.SearchInputData;
import belokonalexander.Api.Models.ApiEntity;
import belokonalexander.Api.Models.CompletePoema;
import belokonalexander.Api.Models.FriendsRelate;
import belokonalexander.Api.Models.LastMessageCache;
import belokonalexander.Api.Models.Poema;

import belokonalexander.Api.Models.PoemaMessage;
import belokonalexander.Api.Models.QueryLog;
import belokonalexander.Api.Models.ShortFriendListRow;
import belokonalexander.Api.Models.User;
import belokonalexander.GlobalShell;


/**
 * Created by alexander on 02.11.2016.
 */
public class SugarPoemaDB {

    public  final int CACHE_ROW_LIFE_TIME = 600000; // 10mins

    private static SugarPoemaDB instanse;

    private Context context;


    private SugarPoemaDB(Context context) {
        this.context = context;
    }

    public static SugarPoemaDB init(Context context){
        if(instanse==null){
            SugarContext.init(context);
            instanse = new SugarPoemaDB(context);
        }

        return instanse;
    }

    public static SugarPoemaDB getInstance(){
        return instanse;
    }




    public void deleteFromFriends(String userId){

        String myId = CurrentApi.getInstanse().getCurrentUser().getObjectId();

        SugarRecord.deleteAll(ShortFriendListRow.class," (user_one = ? and user_two = ?) or (user_two = ? and user_one = ? )", userId, myId, userId, myId);
    }

    public void deleteFromLogs(String whereClause){
        SugarRecord.deleteAll(QueryLog.class,whereClause);
    }

    public void changeStatus(PoemaMessage poemaMessage){
        //SugarRecord.save(poemaMessage);
    }




    public<T extends SugarPersistence> void update(T item, String queryItemDesc){

        if(item instanceof ApiEntity){
             ApiEntity apiEntity = (ApiEntity) item;
             apiEntity.setQueryLog(new QueryLog(apiEntity.getObjectId(),apiEntity.getClass(),queryItemDesc));
             LogSystem.logThis("SAVE UPDATE: " + queryItemDesc);
             save(apiEntity);
        }



    }


    public void updatePoemaLastMessage(String poemaId, String messageId){
        List<Poema> poemas =  SugarRecord.find(Poema.class,"object_id = ?", poemaId);

        for(Poema poema : poemas){
            poema.setLastMessageId(messageId);
            SugarRecord.save(poema);
        }

    }

    public  void  save(Object object) throws IllegalStateException{

        if(object==null)
            return;

        Date updateDate = new Date();

        if(object instanceof List && ((List) object).size()>0 && ((List) object).get(0) instanceof SugarPersistence){

                //clear logs for this query
                //but can they have a different queryLog??? - (17.01.2017) - I don't see that condition
                //очищаем старые логи -
                SugarPersistence firstItem = (SugarPersistence) ((List) object).get(0);
                if(firstItem instanceof ApiEntity && ((ApiEntity) firstItem).getQueryLog()!=null){
                    deleteFromLogs(((ApiEntity) firstItem).getQueryLog().getQuery(),((ApiEntity) firstItem).getQueryLog().getOffset());
                }



                for(Object i : (List)object) {

                    SugarPersistence item = (SugarPersistence) i;

                    //генерируем ID и обновляем дату вставки
                    item.setUpdateDate(updateDate);
                    if(item.getId()==null || item.getId()==0){
                        item.generateId();
                    }

                    //сохраняем лог-запись
                    if(item instanceof ApiEntity){
                         ApiEntity apiEntity = (ApiEntity) item;

                        QueryLog queryLog = apiEntity.getQueryLog();
                        if(queryLog!=null) {
                            save(apiEntity.getQueryLog());
                        }
                    }
                         item.saveRelateObjects(); //сохраняем дополнительные объекты
                }

                // TODO: 18.01.2017 copy relations to inserting data from exists values
                Object first = ((List)object).get(0);
                if(first instanceof User){


                    User firstUser = (User) first;
                    if(firstUser.getFriendsRelate().getObjectId()==null){
                        //well, we've got data without relate
                        List<String> ids = new ArrayList<>();
                        List<User> inserting = ((List)object);
                        for(User ins : inserting){
                            ids.add(ins.getObjectId());
                        }

                        List<User> existsUsersWithRelate = SugarRecord.find(User.class, "relate_object_id IS NOT NULL AND object_id IN " + GlobalShell.listToINSQLClause(ids));
                        for(User us : existsUsersWithRelate){
                            for(User ins : inserting){
                                if(us.getObjectId().equals(ins.getObjectId())){
                                    ins.setFriendsRelate(us.getFriendsRelate());
                                }
                            }
                        }

                    }

                }

                //LogSystem.logThis("THREAD DB: " + Thread.currentThread().getName());
                SugarRecord.saveInTx((List)object);

        } else if(object instanceof SugarPersistence) {

            SugarPersistence item = (SugarPersistence) object;



            item.setUpdateDate(updateDate);
            if(item.getId()==null || item.getId()==0){
                 item.generateId();
            }

            if(item instanceof ApiEntity){
                ApiEntity apiEntity = (ApiEntity) item;

                QueryLog queryLog = apiEntity.getQueryLog();
                if(queryLog!=null){
                    deleteFromLogs(queryLog.getQuery(),queryLog.getOffset());
                    save(apiEntity.getQueryLog());
                }
            }


            if(item instanceof User && ((User)item).getFriendsRelate().relateObjectId==null){
                List<User> us = SugarRecord.find(User.class," object_id = ? AND relate_object_id IS NOT NULL");
                if(!us.isEmpty()){
                    ((User)item).setFriendsRelate(us.get(0).getFriendsRelate());
                }

            }

            item.saveRelateObjects();


            SugarRecord.save(item);
        }
         else return;

        //showTable(getClassOf(object));

    }

    private  boolean isTablePersistence(Object item){
        return item instanceof SugarPersistence;
    };


    public Class getClassOf(Object object){
        if(object instanceof List && ((List) object).size()>0){
            if(((List) object).get(0) instanceof SugarPersistence)
                return ((List) object).get(0).getClass();
        }

        return object.getClass();
    }


    public  void showTable(Class type){

        Iterator it = SugarRecord.findAll(type);

        List array = GlobalShell.iteratorToList(it);

        Collections.sort(array, new Comparator<SugarPersistence>() {
            @Override
            public int compare(SugarPersistence t1, SugarPersistence t2) {

                if(t1 == null || t2 == null)
                    return 0;

                return (int) (t2.getUpdateDate()-t1.getUpdateDate());
            }
        });

        LogSystem.logThis("---> Sugar collection: " + type.getName() + " it: " + it );

        int size = 0;

        for(Object item: array) {
            LogSystem.logThis(item);
            size++;
        }

        LogSystem.logThis("---> Sugar collection SIZE: " + size);

    };



    public List<User> getFriendsWithSomeStatus() {
        return SugarRecord.find(User.class, "relate_status >= 0");
    }


    public User getUserById(String objectId){
        return SugarRecord.findById(User.class,CurrentApi.getInstanse().getLongHashOfItem(objectId));
    }




    //get object by log row
    private<T> List<T> getEntityFromLog(List<QueryLog> listData){

        if(listData==null || listData.isEmpty())
            return new ArrayList<>();


        List<T> result = new ArrayList<>();

        String standartClause = "object_id = ?";
        String messageClause = "message_id = ?";

        for(QueryLog log : listData){
            Class type = null;
            try {
                type = Class.forName(listData.get(0).getClassName());
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            List<T> queryResult = SugarRecord.find(type, (type.equals(PoemaMessage.class)) ? messageClause : standartClause, log.getObjectId());
                for(T item : queryResult){
                    result.add(item);
                }
        }

        return result;
    }



    public void deleteFromLogs(String query, Integer offset) throws IllegalStateException{

        //удаляем все кроме create и update
        if(query.contains("create") || query.contains("update"))
            return;

        StringBuilder clause = new StringBuilder(" query = '").append(query).append("'");
        if(offset!=null)
            clause.append(" AND offset >= ").append(offset);

        SugarRecord.deleteAll(QueryLog.class,clause.toString());
    }


    private List<QueryLog> getCachedRows(int limit, String get, String create, String update){
        StringBuilder clause = new StringBuilder("");



        clause.append("query = ?");
        clause.append(" ORDER BY offset, order_in_offset, last_update DESC").append(" LIMIT ").append(limit);
        List<QueryLog> logs = SugarRecord.find(QueryLog.class,clause.toString(), get);


        Date topLastUpdate= new Date(0);
        Set<String> messagesIds = new HashSet<>();
        for(QueryLog log : logs){
            if(topLastUpdate==null || log.getLastUpdate().getTime()>topLastUpdate.getTime()){
                topLastUpdate = log.getLastUpdate();
            }
            messagesIds.add(log.getObjectId());
        }

        //берем все updated и created
        List<QueryLog> createdLogs = SugarRecord.find(QueryLog.class,"((query = ? AND object_id NOT IN " + GlobalShell.listToINSQLClause(new ArrayList<>(messagesIds)) + ") OR (query = ?)) AND last_update>? ORDER BY last_update DESC", create, update, String.valueOf(topLastUpdate.getTime()));
        if(!createdLogs.isEmpty()){

            //TODO NEED REFACTORING
            //удаляем возможные совпадения в update
            Set<String> uniqueIds = new LinkedHashSet<>();
            for(QueryLog log : createdLogs){
                uniqueIds.add(log.getObjectId());
            }

            List<QueryLog> createdLogsUnique = new ArrayList<>();
            for(String uniqueId : uniqueIds){
                for(QueryLog queryLog : createdLogs){
                    if(queryLog.getObjectId().equals(uniqueId)){
                        createdLogsUnique.add(queryLog);
                        break;
                    }
                }
            }


            //удаляем повтореные object id
            List<QueryLog> tmp = new ArrayList<>(logs);
            int deleted = 0;
            LogSystem.logThis(" SIZES: " + tmp.size() + " / " + logs.size());
            for(int i =0; i < tmp.size(); i++ ){
                for(int j = 0; j < createdLogsUnique.size(); j++){
                    if(tmp.get(i).getObjectId().equals(createdLogsUnique.get(j).getObjectId())){
                        LogSystem.logThis(" SIZES: " + i);
                        logs.remove(i-deleted);
                        deleted++;
                    }
                }
            }

            logs.addAll(0,createdLogsUnique);



            if(logs.size()>limit)
                logs = logs.subList(0,limit);
        }

        return logs;
    }

    public List<PoemaMessage> getTopPoemsMessages(int limit, String... params){

        List<QueryLog> logs = getCachedRows(limit, params[0], params[1], params[2]);

        if(logs.isEmpty())
            return null;

        List<PoemaMessage> res = getEntityFromLog(logs);

        return (res.isEmpty()) ? null : res;
    }


    public List<Poema> getTopPoems(int limit, String... params){ // need 2 params
        //
        long starttime = System.currentTimeMillis();

        List<Poema> res = new ArrayList<>();

        List<QueryLog> logs = getCachedRows(limit, params[0], params[1], params[2]);

        if(logs.isEmpty())
            return null;

        Set<String> poemasUsersIds = new HashSet<>();

        List<Poema> poemsList = getEntityFromLog(logs);

        for(Poema poema : poemsList){
            poemasUsersIds.addAll(poema.getUsersIds());
            res.add(poema);
        }

        List<User> allUserList = getAllUsersForPoems(poemasUsersIds);
        for(Poema p : res){
            for(String id : p.getUsersIds()){
                for(User u : allUserList){
                    if(u.getObjectId().equals(id)){
                        p.addMember(u);
                    }
                }
            }
        }


        Set<String> messagesIds = new HashSet<>();
        for(Poema poema : res){
            if(poema.getLastMessageId()!=null && poema.getLastMessageId().length()>0)
                messagesIds.add(poema.getLastMessageId());
        }
        List<PoemaMessage> allTopMessages = getPoemaMessagesByIds(messagesIds);
        for(PoemaMessage pm : allTopMessages){
            for(Poema p : res){
                if(p.getLastMessageId()!=null && p.getLastMessageId().length()>0 ) {
                    if (p.getLastMessageId().equals(pm.getObjectId())){
                        p.setLastMessage(pm);
                    }
                }
            }
        }


        return (res.isEmpty()) ? null : res;
    }

    private List<PoemaMessage> getPoemaMessagesByIds(Set<String> messagesIds){

        List ids = new ArrayList<>(messagesIds);

        return SugarRecord.find(PoemaMessage.class,"message_id IN " + GlobalShell.listToINSQLClause(ids));

    }

    private List<User> getAllUsersForPoems(Set<String> poemasIds) {

        List ids = new ArrayList<>(poemasIds);

        String inClause = GlobalShell.listToINSQLClause(ids);

        LogSystem.logThis("IN: " + inClause);

        return SugarRecord.find(User.class, " object_id IN " + inClause);
    }


    public void deleteAllUsers(String whereClause){
        SugarRecord.deleteAll(User.class, whereClause);
    }

    public  User getUser(String objectId) {
        List<User> res = SugarRecord.find(User.class, "object_id = ? ", objectId);

        return (res.isEmpty()) ? null : res.get(0);
    }

    synchronized public void clearDatabase(){
        List<CompletePoema> all = SugarRecord.listAll(CompletePoema.class);
        SugarContext.terminate();
        SchemaGenerator schemaGenerator = new SchemaGenerator(context);
        schemaGenerator.deleteTables(new SugarDb(context).getDB());
        SugarContext.init(context);
        schemaGenerator.createDatabase(new SugarDb(context).getDB());
        SugarRecord.saveInTx(all);
    }


    public List<User> getAllFriends(String userId, int limit){

     return SugarRecord.find(User.class, "relate_status = ? ORDER BY created, updated DESC LIMIT ?", "2", String.valueOf(limit));

    }

    public List<CompletePoema> getCompletePoems(SearchInputData data){

        String limit = String.valueOf(data.getPageSize());
        String offset = String.valueOf(data.getOffset());
        if(data.getEmpty())
            return SugarRecord.find(CompletePoema.class," owner_id = ? ORDER BY is_favorite DESC, last_update DESC LIMIT ? OFFSET ?", CurrentApi.getInstanse().getCurrentUser().getObjectId(), limit, offset);
        else return SugarRecord.find(CompletePoema.class," owner_id = ? AND " + data.getKey() +" LIKE '"+ data.getValue()+"%' ORDER BY is_favorite DESC, last_update DESC LIMIT ? OFFSET ?", CurrentApi.getInstanse().getCurrentUser().getObjectId(), limit, offset);
    }


    public Object getShortFriends(String userId, int limit) {
        List<String> usersIds = new ArrayList<>();

        List<ShortFriendListRow> friendList = SugarRecord.find(ShortFriendListRow.class, "user_one = ?", userId);

        if(friendList.size()==0) {
            return new ArrayList<>(); //return empty list
        }

        for(ShortFriendListRow item : friendList){
            String anotherUserId = (userId.equals(item.getUserOne())) ? item.getUserTwo() : item.getUserOne() ;

            usersIds.add(anotherUserId);

        }

        String inClause = GlobalShell.listToINSQLClause(usersIds);

        List<User> res = SugarRecord.find(User.class,"object_id IN " + inClause + " limit ?", String.valueOf(limit));

        return res;
    }

    public static void deleteAllPoems(String whereClause) {

        List<Poema> poems = SugarRecord.find(Poema.class, whereClause);

        SugarRecord.deleteAll(Poema.class,whereClause);
    }

    //return pageSize TOP users
    public List<User> getTopUsers(int pageSize, String... params) {

        List<QueryLog> logs = getCachedRows(pageSize, params[0], params[1], params[2]);

        if(logs.isEmpty())
            return null;

         List<User> res = getEntityFromLog(logs);

         return (res.isEmpty()) ? null : res;
    }

    public Poema getPoema(String poema_id) {

        List<Poema> poems = SugarRecord.find(Poema.class," object_id = ?", poema_id);

        if(!poems.isEmpty()){
            Poema p = poems.get(0);
            Set<String> usersIds = new HashSet<>(p.getUsersIds());
            List<User> allUserList = getAllUsersForPoems(usersIds);

            for(User u : allUserList){
                    p.addMember(u);
            }



            return p;
        }




        return null;
    }


    public String getLastCachedMessage(String poemaId){

        List<LastMessageCache> data = SugarRecord.find(LastMessageCache.class, "poema_id = ?", poemaId);
        if(!data.isEmpty()){
            return data.get(0).getText(false);
        }

        return "";
    }

    public void saveLastCachedMessage(LastMessageCache data){
        data.generateId();
        SugarRecord.save(data);
    }

    public void deleteLastCachedMessage(LastMessageCache data){
        data.generateId();
        SugarRecord.delete(data);
    }

    public void updateFavorite(CompletePoema completePoema) {
        SugarRecord.save(completePoema);
    }

    public void deleteComplete(CompletePoema poema) {
        LogSystem.logThis("DELETE: " + poema.getTitle());
        SugarRecord.deleteAll(CompletePoema.class,"id = ?", String.valueOf(poema.getId()));
    }

    public long getCompletePoemsSize() {
        return SugarRecord.count(CompletePoema.class, "owner_id= ? ", new String[]{CurrentApi.getInstanse().getCurrentUser().getObjectId()});
    }

    public String getSizeOfDB() {
        return String.valueOf(SugarRecord.count(User.class) + SugarRecord.count(ShortFriendListRow.class) + SugarRecord.count(QueryLog.class) +
                SugarRecord.count(PoemaMessage.class) + SugarRecord.count(LastMessageCache.class) + SugarRecord.count(Poema.class));
    }
}
