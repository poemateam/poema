package belokonalexander.Backendless;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.BackendlessUser;
import com.backendless.Subscription;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.async.callback.BackendlessCallback;
import com.backendless.exceptions.BackendlessException;
import com.backendless.exceptions.BackendlessFault;

import com.backendless.messaging.PublishOptions;

import com.backendless.messaging.SubscriptionOptions;
import com.backendless.persistence.BackendlessDataQuery;
import com.backendless.persistence.QueryOptions;
import com.backendless.persistence.local.UserIdStorageFactory;
import com.backendless.persistence.local.UserTokenStorageFactory;
import com.backendless.services.messaging.MessageStatus;
import com.backendless.services.messaging.PublishStatusEnum;
import com.orm.SugarRecord;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import StaticHelper.AppNavigation;
import StaticHelper.LogSystem;
import StaticHelper.PoemaErrors;
import StaticHelper.Settings;
import belokonalexander.Api.ApiExceptions.PoemaApiError;
import belokonalexander.Api.ApiItem;
import belokonalexander.Api.ApiStarter;
import belokonalexander.Api.CurrentApi;
import belokonalexander.Api.InputQueryDataHolders.MassiveQueryInputData;
import belokonalexander.Api.Models.FriendsRelate;

import belokonalexander.Api.Models.Poema;
import belokonalexander.Api.Models.PoemaMessage;
import belokonalexander.Api.Models.QueryLog;
import belokonalexander.Api.Models.ShortFriendListRow;
import belokonalexander.Api.Models.SugarAdditional.SugarPoemaDB;
import belokonalexander.Api.Models.User;
import belokonalexander.Api.ResultWaiter;

import belokonalexander.Backendless.Services.PoemaUserService;
import belokonalexander.GlobalShell;
import belokonalexander.Api.InputQueryDataHolders.SearchInputData;
import belokonalexander.SharedAppPrefs;
import id.zelory.compressor.Compressor;
import ru.belokonalexander.poema.PoemaApplication;
import ru.belokonalexander.poema.R;
import ru.belokonalexander.poema.Services.BackendlessMessagingService;


/**
 * Created by admin on 30.07.2016.
 */
public class BackendlessShell extends ApiStarter {

    private Context context;

    public static Boolean isInit = false;

    public static final String FILE_AVATARS_PATH = "/avatars";

    public BackendlessShell(Context context) {

        this.context = context;
        if(!isInit) {
            Backendless.initApp(context, context.getString(R.string.backendless_app_id),context.getString(R.string.backendless_key),"v1");

            isInit = true;
        }

    }


    @Override
    public void login(HashMap<String, String> data, ResultWaiter waiter) {
        super.login(data, waiter);

        Backendless.UserService.login(data.get(ApiItem.EMAIL_KEY), data.get(ApiItem.PASSWORD_KEY), new AutoCanceledWaitingTask<BackendlessUser>(waiter,context,true) {
            @Override
            Object onCorrectResponse(BackendlessUser o) {
                LogSystem.logThis(" RESPONSE 2");
                User user = new User(o);
                return user;
            }
        },true);


    }


    @Override
    public void registration(HashMap<String,String> data, @NonNull ResultWaiter waiter) {
        super.registration(data, waiter);

        BackendlessUser user = new BackendlessUser();
        user.setEmail(data.get(ApiItem.EMAIL_KEY));
        user.setPassword(data.get(ApiItem.PASSWORD_KEY));
        user.setProperty("name",data.get(ApiItem.NAME_KEY));

        Backendless.UserService.register(user, new AutoCanceledWaitingTask<BackendlessUser>(waiter,context,true) {
            @Override
            Object onCorrectResponse(BackendlessUser o) {
                User user = new User(o);
                return user;
            }
        });
    }

    @Override
    public String getSavedToken() {
        return  UserTokenStorageFactory.instance().
                getStorage().get();
    }


    @Override
    public String getDescription() {
        return "This is Backandless api";
    }

    @Override
    public void logout(Context redirectContext) {
        super.logout(redirectContext);

        Backendless.UserService.logout(new AsyncCallback<Void>() {
            @Override
            public void handleResponse(Void aVoid) {
                SugarPoemaDB.getInstance().clearDatabase();
                SharedAppPrefs.getInstance().clearUserData();
                stopMessageingService();
                AppNavigation.restartAppForLogin(redirectContext);

            }

            @Override
            public void handleFault(BackendlessFault backendlessFault) {
                LogSystem.makeToast(((PoemaApplication)context.getApplicationContext()).getCurrentActivity(), context.getString(R.string.connection_error), LogSystem.ToastType.Error,LogSystem.TOAST_POSITION_BOTTOM);
            }
        });
    }

    @Override
    public void searchEntity(SearchInputData data, ResultWaiter waiter) {
        super.searchEntity(data, waiter);

        String whereClause;

        if(!data.getFullContains()){
            whereClause = data.getKey() + " LIKE '" + data.getValue() + "%'";
            } else {
            whereClause = data.getKey() + " = '" + data.getValue() + "'";
            }

        BackendlessDataQuery dataQuery = new BackendlessDataQuery(whereClause);

        //TODO paging
        QueryOptions queryOptions = new QueryOptions();
        queryOptions.setOffset(data.getOffset());
        queryOptions.setPageSize(data.getPageSize());

        if(data.getPlace().equals(("Users").toUpperCase())) {
            dataQuery.setQueryOptions(queryOptions);
            Backendless.Data.of(BackendlessUser.class).find(dataQuery, new AutoCanceledWaitingTask<BackendlessCollection<BackendlessUser>>(waiter,context) {

                @Override
                Object onCorrectResponse(BackendlessCollection<BackendlessUser> backendlessUserBackendlessCollection) {
                    //теперь получим статусы друг или нет
                    List<User> userEntities = new ArrayList<>();
                    List<String> userIds = new ArrayList<>();
                    for (int i = 0; i < backendlessUserBackendlessCollection.getCurrentPage().size(); i++) {
                        BackendlessUser item = backendlessUserBackendlessCollection.getCurrentPage().get(i);
                        User user = new User(item.getProperties(), item.getUserId());
                        userEntities.add(user);
                        userIds.add(user.getObjectId());
                    }

                    return userEntities;
                }
            });
        }

    }

    @Override
    public void updateCurrentUser(ResultWaiter waiter) {

        super.updateCurrentUser(waiter);

        syncUserData();

        Backendless.UserService.update(Backendless.UserService.CurrentUser(), new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser backendlessUser) {

                waiter.setResult(true);
            }

            @Override
            public void handleFault(BackendlessFault backendlessFault) {
                waiter.setResult(new PoemaApiError(backendlessFault));
            }
        });
    }

    @Override
    public boolean isSessionValid() {

        try {
            return Backendless.UserService.isValidLogin();
        } catch (BackendlessException e) {

            //BackendlessFault backendlessFault = new BackendlessFault("-1001", "Illegal token");
            //CurrentApi.getErrorResolver().tryResolve(new PoemaApiError(backendlessFault));
            return false;
        }
        }

        @Override
        public void uploadUserImage (File file, ResultWaiter waiter){
            super.uploadUserImage(file, waiter);




            long fileSize = file.length();

            LogSystem.logThis("START: " + fileSize + " / " + Settings.MAX_IMAGE_SIZE);

            if(fileSize>Settings.MAX_IMAGE_SIZE){
                waiter.setResult(new Exception("Too large file"));
                return;
            }

            int quality = (int) (100 * Math.min(((float) Settings.MAX_IMAGE_SIZE / (float) fileSize), 1f));


            //Bitmap uploadingFileBitmap = BitmapFactory.decodeStream(new ByteArrayInputStream(GlobalShell.bitmapToByteArray(BitmapFactory.decodeFile(file.getAbsolutePath()), Bitmap.CompressFormat.JPEG, quality)));

            Bitmap uploadingFileBitmap = Compressor.getDefault(context).compressToBitmap(file);

            String original = GlobalShell.getBase64(uploadingFileBitmap);
            String mini = GlobalShell.getBase64(GlobalShell.getMiniBitmap(uploadingFileBitmap));


            PoemaUserService poemaUserService = PoemaUserService.getInstance();

            long start = System.currentTimeMillis();
            poemaUserService.uploadUserAvatarAsync(original, mini, new AsyncCallback<Map<String, String>>() {
            @Override
            public void handleResponse(Map<String, String> stringStringMap) {

                CurrentApi.getInstanse().getCurrentUser().setImage(stringStringMap.get("image"));
                CurrentApi.getInstanse().getCurrentUser().setSmall_image(stringStringMap.get("small_image"));
                LogSystem.logThis("FINISH: " + (System.currentTimeMillis()-start));
                waiter.setResult(true);
            }

            @Override
            public void handleFault(BackendlessFault backendlessFault) {

                waiter.setResult(new PoemaApiError(backendlessFault));
            }
        });

    }

    @Override
    public void getFriends(String userId, MassiveQueryInputData data, int status,  ResultWaiter waiter, boolean includeInviteForMe) {
        super.getFriends(userId, data, status, waiter, includeInviteForMe);

        PoemaUserService poemaUserService = PoemaUserService.getInstance();



        poemaUserService.getFriendsAsync(userId, data.getOffset(), data.getPageSize(), status, new AutoCanceledWaitingTask<List<Map<String, Object>>>(waiter,context) {
            @Override
            Object onCorrectResponse(List<Map<String, Object>> o) {

                List<User> users = new ArrayList<>();

                for(int i = 0; i < o.size(); i++) {

                    FriendsRelate relate = new FriendsRelate(o.get(i));
                    User user = relate.getFriendFromRelate(userId);
                        if (user == null) throw new NullPointerException();
                    user.setFriendsRelate(relate);
                    user.setQueryLog(new QueryLog(user.getObjectId(),User.class,requestDescription,data.getOffset(),i));
                    users.add(user);

                }


                return users;
            }
        });


    }

    @Override
    public void getShortFriendsList(String userId, ResultWaiter waiter) {
        super.getShortFriendsList(userId, waiter);

        LogSystem.logThis(" SHORT ID: " + userId);

        PoemaUserService poemaUserService = PoemaUserService.getInstance();

        poemaUserService.getFriendsAsync(userId, 0, Settings.SHORT_USER_LIST_COUNT, 2, new AutoCanceledWaitingTask<List<Map<String, Object>>>(waiter,context) {
            @Override
            Object onCorrectResponse(List<Map<String, Object>> o) {

                List<User> users = new ArrayList<>();

                List<ShortFriendListRow> friends = new ArrayList<>();

                for(int i = 0; i < o.size(); i++) {

                    FriendsRelate relate = new FriendsRelate(o.get(i));
                    LogSystem.logThis(" FRIENDS RELATES: " + relate.getObjectId() + " STATUS: " + relate.getStatus());
                    User user = relate.getFriendFromRelate(userId);
                    if (user == null) throw new NullPointerException();

                    ShortFriendListRow shortFriendListRow= new ShortFriendListRow(userId,user.getObjectId());

                    friends.add(shortFriendListRow);

                    user.setFriendsRelate(relate);
                    user.setQueryLog(new QueryLog(user.getObjectId(),User.class,requestDescription));

                    users.add(user);



                }

                if(SharedAppPrefs.getInstance().getUsingCache() && this.dbCaching) {
                    SugarRecord.deleteAll(ShortFriendListRow.class, "user_one = ?", userId);
                    SugarPoemaDB.getInstance().save(friends);
                }

                return users;
            }
        });

    }

    @Override
    public User getCurrentUser() {

       /* if(true)
            return null;*/

        if(Backendless.UserService.CurrentUser()!=null && Backendless.UserService.CurrentUser().getUserId()!=null) {

            return new User(Backendless.UserService.CurrentUser());
        }

        if(CurrentApi.getInstanse().getUserHash()!=null)
            {

                String userId = UserIdStorageFactory.instance().getStorage().get();
                LogSystem.logThis("USER ID: " + userId);
                //TODO user can change password
                if(GlobalShell.internetIsOn(context)) {
                    BackendlessUser user = Backendless.UserService.findById(userId);
                    LogSystem.logThis(" CURRENT USER MAP: " + user.getProperties());
                    Backendless.UserService.setCurrentUser(user);
                    return new User(user);
                } else {
                    //пробуем взять кешированные данные из SharedPrefs
                    return SugarPoemaDB.getInstance().getUserById(userId);
                }

            }
        else return null;
    }

    @Override
    public void offerFriendship(User user, ResultWaiter waiter) {
        super.offerFriendship(user, waiter);

        FriendsRelate friendsRelate = new FriendsRelate();
        friendsRelate.setStatus(0);
        friendsRelate.setUserTwo(user);
        friendsRelate.setUserOne(CurrentApi.getInstanse().getCurrentUser());
        friendsRelate.updateCompositeKey();


        Map<String,Object> data = new HashMap<>();
        data = friendsRelate.fillToMap(data);


        Backendless.Persistence.of("FriendsRelate").save(data, new AutoCanceledWaitingTask<Map>(waiter,context) {
            @Override
            Object onCorrectResponse(Map o) {
                friendsRelate.initFromMap(o);
                user.setFriendsRelate(friendsRelate);
                return user;
            }
        });

    }


    @Override
    public void cancelFriendship(User user, ResultWaiter waiter) {
        super.cancelFriendship(user, waiter);

        HashMap<String,Object> data = new HashMap<>();
        new FriendsRelate(user.getFriendsRelate()).fillToMap(data);

        Backendless.Persistence.of("FriendsRelate").remove(data, new AutoCanceledWaitingTask<Long>(waiter,context) {
            @Override
            Object onCorrectResponse(Long o) {
                user.clearFriendsRelate();

                if(SharedAppPrefs.getInstance().getUsingCache() && this.dbCaching){
                    SugarPoemaDB.getInstance().deleteFromFriends(user.getObjectId());
                    SugarPoemaDB.getInstance().deleteFromLogs("object_id = '" + user.getObjectId() + "'");
                }

                return user;
            }
        });
    }


    @Override
    public int convertInAppError(int errorCodeApi) {



        int[] tokenExpiredErrors = {-1001,3064};

        for(int i =0; i < tokenExpiredErrors.length; i++)
        {
            if(errorCodeApi==tokenExpiredErrors[i])
            {
                return PoemaErrors.API_TOKEN_EXPIRED_ERROR;
            } else if (errorCodeApi==1155 || errorCodeApi==1000)
                return PoemaErrors.DUBLICATE_ERROR;
        }

        return errorCodeApi;
    }

    @Override
    public int convertInAppError(String message) {



        if(message.contains("No address associated with hostname")){
            return PoemaErrors.INTERNET_CONNECTION_ERROR;
        }

        return -15;
    }


    @Override
    public void confirmFriendship(User user, ResultWaiter waiter) {
        super.confirmFriendship(user, waiter);

        FriendsRelate friendsRelate = new FriendsRelate(user.getFriendsRelate());
        friendsRelate.setStatus(2);
        friendsRelate.updateCompositeKey(user.getObjectId(),CurrentApi.getInstanse().getCurrentUser().getObjectId());

        HashMap<String,Object> data = new HashMap();
        friendsRelate.fillToMap(data);


        //TODO can change "...." on user.getPersistanse()
        Backendless.Persistence.of("FriendsRelate").save(data, new AutoCanceledWaitingTask<Map>(waiter,context) {
            @Override
            Object onCorrectResponse(Map o) {
                friendsRelate.initFromMap(o);
                user.setFriendsRelate(friendsRelate);
                return  user;
            }
        });

    }

    //CurrentUser ->(fill) BackendlessUser
    public void syncUserData()
    {
        Backendless.UserService.CurrentUser().setProperties(CurrentApi.getInstanse().getCurrentUser().fillToMap(Backendless.UserService.CurrentUser().getProperties()));
    }


    @Override
    public void updateCurrentUserProperty(String property, ResultWaiter waiter) {
        super.updateCurrentUserProperty(property + " ::: " + CurrentApi.getInstanse().getCurrentUser().getReflectPropertyValue(property), waiter);
        Backendless.UserService.CurrentUser().setProperty(property, CurrentApi.getInstanse().getCurrentUser().getReflectPropertyValue(property));
        //Backendless.UserService.
    }

    @Override
    public void getUser(String userId, ResultWaiter waiter) {
        super.getUser(userId, waiter);

        PoemaUserService poemaUserService = PoemaUserService.getInstance();

        poemaUserService.getUserWithRelateAsync(userId, new AutoCanceledWaitingTask<Map<String, Object>>(waiter,context) {
            @Override
            Object onCorrectResponse(Map<String, Object> o) {

                Map userData = ((BackendlessUser) o.get("User")).getProperties();
                BackendlessCollection<Map> backendlessCollection = (BackendlessCollection) o.get("Relation");
                User user = new User();
                user.initFromMap(userData);

                if(backendlessCollection.getCurrentPage().size()>0){
                    FriendsRelate friendsRelate = new FriendsRelate();
                    friendsRelate.initFromMap(backendlessCollection.getCurrentPage().get(0));
                    user.setFriendsRelate(friendsRelate);
                }

                return user;
            }
        });

    }

    @Override
    public void createPoema(Poema poema, ResultWaiter waiter) {
        super.createPoema(poema, waiter);

        HashMap<String,Object> data = new HashMap();
        poema.fillToMap(data);

        LogSystem.logThis("Отправляю: " + data);
        Backendless.Persistence.of("Poema").save(data, new AutoCanceledWaitingTask<Map>(waiter,context) {
            @Override
            Object onCorrectResponse(Map o) {

                poema.setObjectId((String) o.get("objectId"));



                return poema;
            }
        });

    }


    @Override
    public void getPoems(String userId, MassiveQueryInputData data, ResultWaiter waiter) {
        super.getPoems(userId, data, waiter);


        //BackendlessDataQuery dataQuery = new BackendlessDataQuery(whereClause);
        String queryParams = userId;
        //TODO paging
        QueryOptions queryOptions = new QueryOptions();
        queryOptions.setOffset(data.getOffset());
        queryOptions.setPageSize(data.getPageSize());
        List<String> sortBy = new ArrayList<String>();

        sortBy.add( "updated DESC" );


        queryOptions.setSortBy(sortBy);
        BackendlessDataQuery dataQuery = new BackendlessDataQuery();
        dataQuery.setWhereClause("members.objectId='" + userId+"'");

        dataQuery.setQueryOptions(queryOptions);

        Backendless.Persistence.of("Poema").find(dataQuery, new AutoCanceledWaitingTask<BackendlessCollection<Map>>(waiter,context) {
            @Override
            Object onCorrectResponse(BackendlessCollection<Map> o) {

                List<Poema> poems = new ArrayList<>();

                for (int i = 0; i < o.getCurrentPage().size(); i++){
                    Map item =  o.getCurrentPage().get(i);

                    Poema poema = new Poema();
                    poema.initFromMap(item);

                    poems.add(poema);
                }

                LogSystem.logThis("POEMS: " + poems);

                return poems;
            }
        });

    }

    @Override
    public void deletePoems(Poema poema, ResultWaiter waiter) {
        super.deletePoems(poema, waiter);

        Map map = new HashMap();
        poema.fillToMap(map);

        Backendless.Persistence.of("Poema").remove(map, new AutoCanceledWaitingTask<Long>(waiter,context) {
            @Override
            Object onCorrectResponse(Long o) {

                if(SharedAppPrefs.getInstance().getUsingCache() && this.dbCaching){
                    SugarPoemaDB.getInstance().deleteFromLogs("object_id = '" + poema.getObjectId() + "'");
                }

                return poema;
            }
        });

    }


    @Override
    public void startMessagingService() {
        //context.startService(new Intent(context, BackendlessMessagingService.class));
    }

    @Override
    public void stopMessageingService() {

        //context.stopService(new Intent(context,BackendlessMessagingService.class));
    }


    @Override
    public void getPoemsMessage(Poema poema, MassiveQueryInputData data, ResultWaiter waiter) {
        super.getPoemsMessage(poema, data, waiter);

        QueryOptions queryOptions = new QueryOptions();
        queryOptions.setOffset(data.getOffset());
        queryOptions.setPageSize(data.getPageSize());
        List<String> sortBy = new ArrayList<String>();



        sortBy.add( "created DESC" );

        queryOptions.setSortBy(sortBy);
        BackendlessDataQuery dataQuery = new BackendlessDataQuery();
        dataQuery.setWhereClause("poemaId='" + poema.getObjectId()+"'");
        dataQuery.setQueryOptions(queryOptions);

        Backendless.Persistence.of("MessageLog").find(dataQuery, new AutoCanceledWaitingTask<BackendlessCollection<Map>>(waiter,context) {
            @Override
            Object onCorrectResponse(BackendlessCollection<Map> o) {

                List<PoemaMessage> poemaMessages = new ArrayList<>();

                for (int i = 0; i < o.getCurrentPage().size(); i++){
                    Map item =  o.getCurrentPage().get(i);
                    PoemaMessage poemaMessage = new PoemaMessage();
                    poemaMessage.initFromMap(item);
                    poemaMessages.add(poemaMessage);


                }



                return poemaMessages;
            }
        });

    }


    @Override
    public void sendMessage(Poema poema, String poemaMessage, ResultWaiter waiter) {
        super.sendMessage(poema, poemaMessage, waiter);

        PublishOptions publishOptions = new PublishOptions();
        publishOptions.putHeader( "poemaId", poema.getObjectId());

        publishOptions.setPublisherId(CurrentApi.getInstanse().getCurrentUser().getObjectId());
        publishOptions.setSubtopic(poema.getAnotherUser().getObjectId());

        publishOptions.putHeader( "android-ticker-text", "Poema! You have some answers" );
        publishOptions.putHeader( "android-content-title", poema.getTitle() );
        publishOptions.putHeader( "android-content-text", poemaMessage);
        String messageId = poema.getObjectId()+"-"+System.currentTimeMillis();
        publishOptions.putHeader("messageId",messageId);

        //todo another
        Backendless.Messaging.publish(poema.getAnotherUser().getObjectId(), (Object) poemaMessage, publishOptions, new AutoCanceledWaitingTask<MessageStatus>(waiter,context) {
            @Override
            Object onCorrectResponse(MessageStatus messageStatus) {

                LogSystem.logThis(" СТАТУС - " + messageStatus.getMessageId());

                if(messageStatus.getStatus()== PublishStatusEnum.SCHEDULED || messageStatus.getStatus()== PublishStatusEnum.PUBLISHED) {
                    return new PoemaMessage(messageId, poemaMessage, poema.getObjectId(),CurrentApi.getInstanse().getCurrentUser().getObjectId());
                }

                return new BackendlessFault("Message fault");
            }
        });
    }

    @Override
    public void readMessage(Poema poema, ResultWaiter waiter) {
        super.readMessage(poema, waiter);

        PoemaUserService poemaUserService = PoemaUserService.getInstance();

        poemaUserService.readMessagesAsync(poema.getObjectId(), new AutoCanceledWaitingTask<Map<String, Object>>(waiter,context) {
            @Override
            Object onCorrectResponse(Map<String, Object> o) {

                if(o!=null){
                    PoemaMessage poemaMessage = new PoemaMessage();
                    poemaMessage.initFromMap(o);
                    poema.setLastMessage(poemaMessage);
                    return poemaMessage;
                }

                return new ArrayList<>();
            }
        });

    }

    @Override
    public void editMessage(PoemaMessage poemaMessage, String newText, ResultWaiter waiter) {
        super.editMessage(poemaMessage, newText, waiter);

        PoemaUserService poemaUserService = PoemaUserService.getInstance();

        LogSystem.logThis(" MESSAGE ID: " + poemaMessage.getObjectId());

        poemaUserService.editMessagesAsync(poemaMessage.getObjectId(), newText, new AutoCanceledWaitingTask<Map<String, Object>>(waiter,context) {
            @Override
            Object onCorrectResponse(Map<String, Object> o) {

                PoemaMessage message = new PoemaMessage();
                message.initFromMap(o);

                if (SharedAppPrefs.getInstance().getUsingCache() && this.dbCaching) {
                    SugarPoemaDB.getInstance().save(message);
                }

                return message;
            }

        });
    }

    @Override
    public void getPoema(String poemaId, ResultWaiter waiter) {
        super.getPoema(poemaId, waiter);

        BackendlessDataQuery dataQuery = new BackendlessDataQuery();
        dataQuery.setWhereClause("objectId='" + poemaId+"'");

        Backendless.Persistence.of("Poema").find(dataQuery, new AutoCanceledWaitingTask<BackendlessCollection<Map>>(waiter,context) {
            @Override
            Object onCorrectResponse(BackendlessCollection<Map> o) {

                if(o.getCurrentPage().size()>0) {
                    Poema poema = new Poema();
                    poema.initFromMap(o.getCurrentPage().get(0));
                    return poema;
                }
                return new ArrayList<>();
            }
        });

    }


    @Override
    public void restorePassword(String userId, ResultWaiter waiter) {
        super.restorePassword(userId, waiter);

        Backendless.UserService.restorePassword(userId, new AutoCanceledWaitingTask<Void>(waiter,context) {
            @Override
            Object onCorrectResponse(Void o) {
                LogSystem.logThis(" ОТВЕТ " + o);
                return new ArrayList<>();
            }
        });



    }

    @Override
    public void registerDevice(String userId, ResultWaiter waiter) {
        super.registerDevice(userId, waiter);

        Backendless.Messaging.registerDevice(context.getResources().getString(R.string.google_sender_id), userId, new AutoCanceledWaitingTask<Void>(waiter,context,true) {
            @Override
            Object onCorrectResponse(Void o) {
                return new ArrayList<>();
            }
        });
    }


    @Override
    public void getAllPoema(String poemaId, ResultWaiter waiter) {
        super.getAllPoema(poemaId, waiter);

        BackendlessDataQuery dataQuery = new BackendlessDataQuery();
        dataQuery.setWhereClause("poemaId='" + poemaId+"'");

        QueryOptions queryOptions = new QueryOptions();

        List<String> sortBy = new ArrayList<String>();
        sortBy.add( "created" );

        queryOptions.setSortBy(sortBy);
        queryOptions.setPageSize(100);
        dataQuery.setQueryOptions(queryOptions);

        int totalLoad = 0;

        getPartOfPoema(dataQuery,queryOptions,totalLoad,new StringBuilder(), waiter);

    }

    private void getPartOfPoema(BackendlessDataQuery dataQuery, QueryOptions queryOptions, int offset, StringBuilder data, ResultWaiter waiter){
        queryOptions.setOffset(offset);
        dataQuery.setQueryOptions(queryOptions);
        Backendless.Persistence.of("MessageLog").find(dataQuery, new AsyncCallback<BackendlessCollection<Map>>() {
            @Override
            public void handleResponse(BackendlessCollection<Map> mapBackendlessCollection) {

                if(mapBackendlessCollection.getTotalObjects()==0){
                    waiter.setResult(new PoemaApiError(new BackendlessFault("empty")));
                    return;
                }

                for(int i = 0; i < mapBackendlessCollection.getCurrentPage().size(); i++){
                    data.append(mapBackendlessCollection.getCurrentPage().get(i).get("message")).append("\n");
                }

                LogSystem.logThis("PART: \n" + offset +  " -> " + mapBackendlessCollection.getCurrentPage().size() + " .... total -> " + mapBackendlessCollection.getTotalObjects());

                if(mapBackendlessCollection.getCurrentPage().size()+offset>=mapBackendlessCollection.getTotalObjects()){
                  waiter.setResult(data.toString());
                } else getPartOfPoema(dataQuery,queryOptions,mapBackendlessCollection.getCurrentPage().size()+offset,data, waiter);
            }

            @Override
            public void handleFault(BackendlessFault backendlessFault) {
                waiter.setResult(new PoemaApiError(backendlessFault));
                CurrentApi.getInstanse().getErrorResolver().tryResolve(new PoemaApiError(backendlessFault,waiter.getClassItem(), waiter.getService()));
            }
        });

    };

    @Override
    public void sendFeedback(String userId, String category, String message, ResultWaiter waiter) {
        super.sendFeedback(userId, category, message, waiter);

        HashMap data = new HashMap();
        HashMap user = new HashMap();
        user.put( "objectId", userId );
        user.put( "___class", "Users" );

        data.put( "user", user);
        data.put( "category", category);
        data.put( "message", message );

        Backendless.Persistence.of("Feedback").save(data, new AutoCanceledWaitingTask<Map>(waiter,context) {
            @Override
            Object onCorrectResponse(Map o) {



                return new ArrayList<>();
            }
        });
    }

    @Override
    public void unregisterDevice(ResultWaiter waiter) {
        super.unregisterDevice(waiter);

        Backendless.Messaging.unregisterDeviceOnServer(new AutoCanceledWaitingTask<Boolean>(waiter,context,true) {
            @Override
            Object onCorrectResponse(Boolean o) {
                return true;
            }
        });

    }
}
