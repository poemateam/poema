package belokonalexander.RxCommonElements.Form;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import StaticHelper.LogSystem;
import ru.belokonalexander.poema.R;

/**
 * Created by alexander on 02.08.2016.
 */
public class FormTextElement extends TextInputEditText implements ValidationField {

    //view for form element item

    private enum ValidateType //enum with regular mask of each field type
    {
        text(".*"),
        email("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE),
        password("^(?=.*[a-zA-Z0-9])(?=\\S+$).{6,}$");

        public final Pattern pattern;

        ValidateType(String text, int patternType) {
            this.pattern = Pattern.compile(text,patternType);
        }
        ValidateType(String text) {
            this.pattern = Pattern.compile(text);
        }
    }

    Context context;

    private ValidateType fieldType;
    private boolean isNotNull;
    public FormTextElement(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;

        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.FormTextElement,
                0, 0);
        try {
            fieldType = ValidateType.values()[a.getInteger(R.styleable.FormTextElement_type, 0)];
            isNotNull = a.getBoolean(R.styleable.FormTextElement_isNotNull, false);
            //getBackground().setColorFilter(ContextCompat.getColor(getContext(), R.color.colorAccent), PorterDuff.Mode.SRC_IN);


        } finally {
            a.recycle();
        }

    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        super.setText(text, type);
        this.setSelection(text.length());
    }

    @Override
    public ValidationAnswer validate() {

        List<String> errors = new ArrayList<>();
        boolean result = true;

        if(isNotNull && getText().toString().trim().isEmpty())
        {
            result = false;
            errors.add(context.getString(R.string.not_null_field_error));
        }

        Matcher m = fieldType.pattern.matcher(getText().toString());

        if(!m.matches())
        {
            result = false;
            errors.add(context.getString(R.string.field_is_incorrect));
        }


        return new ValidationAnswer(result,errors);
    }
}
