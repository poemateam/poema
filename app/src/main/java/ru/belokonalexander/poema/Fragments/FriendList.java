package ru.belokonalexander.poema.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.liucanwen.app.headerfooterrecyclerview.HeaderAndFooterRecyclerViewAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import StaticHelper.LogSystem;
import belokonalexander.Api.CurrentApi;
import belokonalexander.Api.Models.Poema;
import belokonalexander.Api.Models.User;
import belokonalexander.SuppleRecycler.ActionRecycler;
import belokonalexander.SuppleRecycler.AutoRefreshActionRecycler;
import belokonalexander.SuppleRecycler.DataProviders.Abstracts.GetFriendsProvider;
import butterknife.BindView;
import butterknife.ButterKnife;
import ru.belokonalexander.poema.Events.FriendEvent;
import ru.belokonalexander.poema.Events.PoemaEvent;
import ru.belokonalexander.poema.Fragments.ListAdapters.CommonAdapter;
import ru.belokonalexander.poema.Fragments.ListAdapters.FriendsAdapter;
import ru.belokonalexander.poema.PageManagers.Types.ListData;
import ru.belokonalexander.poema.R;

import static StaticHelper.Settings.SHOW_ITEMS;

/**
 * Created by alexander on 13.08.2016.
 */
public class FriendList extends NavigationFragment implements ListData{


    @BindView(R.id.recycler_my_freinds)
    AutoRefreshActionRecycler<User> recyclerMyFriends;



    @BindView(R.id.app_toolbar)
    Toolbar toolbar;


    User owner = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if(rootView!=null)
            return rootView;

        View view = inflater.inflate(R.layout.fragment_my_friends,container,false);
        ButterKnife.bind(this, view);
        fillToolbar(getString(R.string.friends));

        List<User> userList = new ArrayList<>();

        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        recyclerMyFriends.setLayoutManager(llm);
        recyclerMyFriends.setVisibility(View.INVISIBLE);
        HeaderAndFooterRecyclerViewAdapter adapterWithHeader = new HeaderAndFooterRecyclerViewAdapter(new FriendsAdapter(userList,this));
        recyclerMyFriends.setAdapter(adapterWithHeader);
        recyclerMyFriends.init(toolbar);


        recyclerMyFriends.setProvider(new GetFriendsProvider(owner ==null ? CurrentApi.getInstanse().getCurrentUser() : owner));
        recyclerMyFriends.setAfterCacheData(() -> getActivity().runOnUiThread(() -> { recyclerMyFriends.setVisibility(View.VISIBLE);
            ButterKnife.apply(recyclerMyFriends, SHOW_ITEMS);}
        ));

        recyclerMyFriends.getData(ActionRecycler.UpdateMode.INITIAL);


        rootView = view;

        return view;
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        LogSystem.logThis(" SAVE ");
        outState.putInt("qwerty", 125);
    }



    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(savedInstanceState != null)
            LogSystem.logThis("RESTORE: " + savedInstanceState);

    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUserEvent(FriendEvent userEvent){
        LogSystem.logThis(" CATCH USER EVENT: " + userEvent.getUser().getFriendStatusDesc());

        List<User> data = recyclerMyFriends.getDataAdapter().getData();
        User eventItem = userEvent.getUser();

        boolean existInList = false;
        int index = 0;
        User inListObject = null;
        for(User user : data){
            if(user.getObjectId().equals(eventItem.getObjectId())){
                existInList = true;
                inListObject = user;
                break;
            }
            index++;
        }

        switch (eventItem.getFriendStatusDesc()){
            case FRIENDS: {

                if(existInList){
                    recyclerMyFriends.replaceWith(inListObject,eventItem, true);
                } else
                    recyclerMyFriends.addFirstItem(eventItem);

                break;
            }

            case NONE: {

                if(existInList)
                    recyclerMyFriends.removeItem(index);
                break;
            }

            case I_GOT_FRIENDSHIP: {
                if(existInList){
                    recyclerMyFriends.replaceWith(inListObject,eventItem,true);
                }
                else
                    recyclerMyFriends.addFirstItem(eventItem);
                break;
            }
        }

        eventItem.updateInCache(eventItem.getFriendStatusDesc());

    }

    @Override
    public ActionRecycler getActionRecycler() {
        return recyclerMyFriends;
    }
}
