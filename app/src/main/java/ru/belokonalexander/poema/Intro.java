package ru.belokonalexander.poema;


import android.os.Bundle;

import Helper.MaterialTutorial.tutorial.MaterialTutorialActivity;

import belokonalexander.SharedAppPrefs;

public class Intro extends MaterialTutorialActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void showEndTutorial() {
        super.showEndTutorial();
        SharedAppPrefs.getInstance().closeIntro();
    }
}
