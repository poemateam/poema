package belokonalexander.Api.Models;

import java.util.Date;

import belokonalexander.Api.CurrentApi;
import belokonalexander.Api.Models.Annotations.SearchField;
import belokonalexander.Api.Models.SugarAdditional.SugarPersistence;
import belokonalexander.Api.Models.SugarAdditional.SugarPoemaDB;

/**
 * Created by Alexander on 21.02.2017.
 */
@com.orm.dsl.Table(name = "LastMessageCache")
public class LastMessageCache implements SugarPersistence {


    Long id;
    String poemaId;
    String text;

    public LastMessageCache() {
    }

    public LastMessageCache(String poemaId){
        this.poemaId = poemaId;
    }



    Date lastUpdate;

    @Override
    public Long getUpdateDate() {
        return lastUpdate.getTime();
    }

    @Override
    public void setUpdateDate(Date date) {
        lastUpdate = date;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void generateId() {
        id = CurrentApi.getInstanse().getLongHashOfItem(poemaId);
    }

    @Override
    public void saveRelateObjects() {

    }

    public void update(){
        SugarPoemaDB.getInstance().saveLastCachedMessage(this);
    }




    public String getPoemaId() {
        return poemaId;
    }

    public String getText(boolean db) {
        if(db){
            text = SugarPoemaDB.getInstance().getLastCachedMessage(poemaId);
        }
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    @Override
    public String toString() {
        return "LastMessageCache{" +
                "id=" + id +
                ", poemaId='" + poemaId + '\'' +
                ", text='" + text + '\'' +
                ", lastUpdate=" + lastUpdate +
                '}';
    }

    public void delete(){
        SugarPoemaDB.getInstance().deleteLastCachedMessage(this);
    }
}
