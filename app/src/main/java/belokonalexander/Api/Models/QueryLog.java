package belokonalexander.Api.Models;

import com.orm.dsl.Table;

import java.io.Serializable;
import java.util.Date;

import belokonalexander.Api.Models.SugarAdditional.SugarPersistence;
import belokonalexander.Api.Models.SugarAdditional.SugarPoemaDB;

/**
 * Created by alexander on 16.01.2017.
 */

@Table(name = "QueryLog")
public class QueryLog implements SugarPersistence, Serializable{


    String objectId;
    String className;
    String query;
    Integer offset;
    Integer orderInOffset;


    public QueryLog(String objectId, Class cl, String query, Integer offset, Integer orderInOffset) {
        this(objectId, cl, query);
        this.offset = offset;
        this.orderInOffset = orderInOffset;
    }

    public QueryLog(String objectId, Class cl, String query) {
        this.objectId = objectId;
        this.className = cl.getName();
        this.query = query;
    }

    public QueryLog() {

    }

    @Override
    public void setUpdateDate(Date date) {
        lastUpdate = date;
    }

    @Override
    public Long getUpdateDate() {
        return lastUpdate.getTime();
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void generateId() {

    }

    @Override
    public void saveRelateObjects() {

    }




    Long id;

    Date lastUpdate;

    @Override
    public String toString() {
        return "QueryLog{" +
                "objectId='" + objectId + '\'' +
                ", className='" + className + '\'' +
                ", query='" + query + '\'' +
                ", offset=" + offset +
                ", orderInOffset=" + orderInOffset +
                ", id=" + id +
                ", lastUpdate=" + lastUpdate +
                '}';
    }

    public String getObjectId() {
        return objectId;
    }

    public String getClassName() {
        return className;
    }

    public String getQuery() {
        return query;
    }

    public Integer getOffset() {
        return offset;
    }

    public Integer getOrderInOffset() {
        return orderInOffset;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }
}
