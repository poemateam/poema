package belokonalexander.SuppleRecycler.DataProviders.Abstracts;

import java.util.ArrayList;
import java.util.List;

import StaticHelper.LogSystem;
import belokonalexander.Api.CurrentApi;
import belokonalexander.Api.InputQueryDataHolders.MassiveQueryInputData;
import belokonalexander.Api.LocalQueryExecutorExplicit;
import belokonalexander.Api.Models.Poema;
import belokonalexander.Api.Models.SugarAdditional.SugarPoemaDB;
import belokonalexander.Api.Models.User;
import belokonalexander.Api.QueryExecutor;
import belokonalexander.GlobalShell;
import ru.belokonalexander.poema.PageManagers.BackendRunner;

/**
 * Created by alexander on 10.10.2016.
 */
public class GetPoemsProvider extends SolidProvider<Poema> {

    User user;

    public GetPoemsProvider(User user) {
        this.user = user;
    }

    @Override
    public List<Poema> getData(MassiveQueryInputData state) {


        QueryExecutor task = new QueryExecutor(GlobalShell.getParentHash(getClass()));

        Object data = task.execute(resultWaiter -> CurrentApi.getInstanse().getApplicationApi().getPoems(user.getObjectId(), state, resultWaiter));;
        if(data instanceof List)
            return (List<Poema>) data;

        return null;


    }


    @Override
    public List getCacheData(int pageSize) {

        String[] show = {"getPoema?" + user.getObjectId(),"createPoema?"+user.getObjectId(), "updatePoema?"+user.getObjectId() };


        LocalQueryExecutorExplicit getPoemsLocal = new LocalQueryExecutorExplicit(GlobalShell.getParentHash(this.getClass()),
                resultWaiter -> resultWaiter.setResult(SugarPoemaDB.getInstance().getTopPoems(pageSize, show)), true);

        List result = null;
        try {
            result = (List) getPoemsLocal.execute();
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList();
        }

        return result==null ? new ArrayList<>() : result;

    }
}
