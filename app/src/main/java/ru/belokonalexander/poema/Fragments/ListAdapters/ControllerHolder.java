package ru.belokonalexander.poema.Fragments.ListAdapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.daimajia.swipe.SwipeLayout;

import StaticHelper.LogSystem;
import ru.belokonalexander.poema.R;

/**
 * Created by alexander on 18.10.2016.
 */
abstract public class ControllerHolder extends RecyclerView.ViewHolder {

    View controllerLink;
    SwipeLayout swipeLayout;
    View emptyPlace;

    public SwipeLayout getSwipeLayout() {
        return swipeLayout;
    }

    public View getControllerLink() {
        return controllerLink;
    }

    public ControllerHolder(View itemView) {
        super(itemView);

        try {

        swipeLayout = (SwipeLayout)itemView.findViewById(R.id.swipeable_content);
        controllerLink = itemView.findViewById(R.id.swipeable_link);

        //this is always second
        emptyPlace = swipeLayout.getChildAt(1);
        emptyPlace.setOnClickListener(v->swipeLayout.close());

        swipeLayout.setShowMode(SwipeLayout.ShowMode.LayDown);

        controllerLink.setOnClickListener(v->{
            swipeLayout.setVisibility(View.VISIBLE);
            swipeLayout.open();

        });

        swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
        swipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
            @Override
            public void onStartOpen(SwipeLayout layout) {
                //LogSystem.logThis(" START OPEN ");
            }

            @Override
            public void onOpen(SwipeLayout layout) {
                //LogSystem.logThis(" OPEN ");
            }

            @Override
            public void onStartClose(SwipeLayout layout) {
               // LogSystem.logThis(" START CLOSE ");
            }

            @Override
            public void onClose(SwipeLayout layout) {
                //LogSystem.logThis(" CLOSE ");
                swipeLayout.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {
                //LogSystem.logThis(" UPDATING ");
            }

            @Override
            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {
               // LogSystem.logThis(" ON HAND RELEASE ");
            }
        });

        } catch (Exception e)
        {
            Log.e("alert", " Controller holder is not found");
        }

    }



}
