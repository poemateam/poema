package belokonalexander.Api;

import android.content.Context;

import StaticHelper.LogSystem;
import belokonalexander.Api.ApiExceptions.PoemaErrorResolver;
import belokonalexander.Api.ApiExceptions.PoemaResolvers;
import belokonalexander.Api.Models.User;
import belokonalexander.Backendless.BackendlessShell;
import belokonalexander.SharedAppPrefs;
import ru.belokonalexander.poema.PoemaApplication;

/**
 * Created by admin on 01.08.2016.
 */
public class CurrentApi {

    private  ApiItem applicationApi;
    private  PoemaErrorResolver errorResolver;
    private  User currentUser;
    private  String userHash;


    private static CurrentApi currentApi;

    public CurrentApi(ApiEnum type) {
        switch (type){
            case BackendlessApi: applicationApi = new BackendlessShell(PoemaApplication.getContext());
                break;
        }
    }

    public  String getUserHash() {

        if(userHash==null){
            userHash = SharedAppPrefs.getInstance().getUserHash();
        }

        return userHash;
    }

    public  void setUserHash(String uh) {
        userHash = uh;
        SharedAppPrefs.getInstance().setUserHash(userHash);
    }

    public  User getCurrentUser() {
        if(currentUser==null){
            currentUser = SharedAppPrefs.getInstance().restoreCurrentUser();

        }
        return currentUser;
    }

    public  void setCurrentUser(User cu) {
        currentUser = cu;
        SharedAppPrefs.getInstance().storeCurrentUser(cu);
    }

    public  PoemaErrorResolver getErrorResolver() {
        if(errorResolver==null){
            errorResolver = new PoemaResolvers().getInstance(PoemaApplication.getContext());
        }
        return errorResolver;
    }



    public ApiItem getApplicationApi() {

        if(applicationApi==null) throw new NullPointerException();

        return applicationApi;
    }


    public Long getLongHashOfItem(String objectId){

        long h = 1125899906842597L; // prime
        int len = objectId.length();

        for (int i = 0; i < len; i++) {
            h = 31*h + objectId.charAt(i);
        }

        return h;
    };

    public static CurrentApi getInstanse(){
        if(currentApi==null){
            currentApi = new CurrentApi(CurrentApi.ApiEnum.BackendlessApi);
        }

        return  currentApi;
    }




    public enum ApiEnum
    {
        BackendlessApi;
    }


}
