package ru.belokonalexander.poema;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;


import com.google.android.gms.analytics.ExceptionReporter;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.orm.SugarContext;

import java.util.HashMap;

import StaticHelper.LogSystem;
import belokonalexander.Api.ApiExceptions.PoemaResolvers;
import belokonalexander.Api.CurrentApi;
import belokonalexander.Api.Models.SugarAdditional.SugarPoemaDB;
import belokonalexander.DevicePrefs;
import belokonalexander.SharedAppPrefs;
import ru.belokonalexander.poema.Services.BackendlessMessagingService;

import static weborb.util.ThreadContext.context;


/**
 * Created by admin on 26.07.2016.
 */
public class PoemaApplication extends Application {

    private int lifeTime;
    private Activity mCurrentActivity;
    HashMap<TrackerName, Tracker> mTrackers = new HashMap<>();

    @SuppressLint("StaticFieldLeak")
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();

        PoemaApplication.context = getApplicationContext();


        SharedAppPrefs.getInstance().incCounter();
        DevicePrefs.InitDevicePrefs(getResources().getDisplayMetrics());



        SugarPoemaDB.init(getApplicationContext());

        stopService(new Intent(this, BackendlessMessagingService.class));

        CurrentApi.getInstanse().getApplicationApi().stopMessageingService();

        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                setCurrentActivity(activity);
            }

            @Override
            public void onActivityStarted(Activity activity) {

            }

            @Override
            public void onActivityResumed(Activity activity) {
                lifeTime++;

                //CARE
                NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.cancelAll();
            }

            @Override
            public void onActivityPaused(Activity activity) {
                lifeTime--;
            }

            @Override
            public void onActivityStopped(Activity activity) {

            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

            }

            @Override
            public void onActivityDestroyed(Activity activity) {

            }
        });



    }

    public Activity getCurrentActivity(){
        return mCurrentActivity;
    }
    public void setCurrentActivity(Activity mCurrentActivity){
        this.mCurrentActivity = mCurrentActivity;
    }

    public boolean isAppActive(){
        return lifeTime>0;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        SugarContext.terminate();
    }


    private static final String PROPERTY_ID = "UA-93028527-1";
    public enum TrackerName {
        APP_TRACKER, // Счетчик, используемый только в этом приложении.
        GLOBAL_TRACKER, // Счетчик, применяемый ко всем приложениям компании (например, для отслеживания сводных списков).
        ECOMMERCE_TRACKER, // Счетчик, применяемый ко всем транзакциям электронной торговли компании.
    }

    synchronized public Tracker getTracker(TrackerName trackerId) {
        if (!mTrackers.containsKey(trackerId)) {

            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            Tracker t = (trackerId == TrackerName.APP_TRACKER) ? analytics.newTracker(PROPERTY_ID)
                    : (trackerId == TrackerName.GLOBAL_TRACKER) ? analytics.newTracker(R.xml.global_tracker)
                    : analytics.newTracker(R.xml.global_tracker); //ecommerce

            t.enableExceptionReporting(true);

            mTrackers.put(trackerId, t);

        }
        return mTrackers.get(trackerId);
    }

    public static Context getContext(){
        return context;
    }

}
