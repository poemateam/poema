package belokonalexander.Api;

/**
 * Created by alexander on 16.11.2016.
 */
public interface ApiTask {
    Object execute();
    void refresh();
    void cancel();
}
