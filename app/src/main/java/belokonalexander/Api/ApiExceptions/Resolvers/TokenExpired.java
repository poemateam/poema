package belokonalexander.Api.ApiExceptions.Resolvers;

import android.content.Context;

import StaticHelper.LogSystem;
import StaticHelper.PoemaErrors;
import belokonalexander.Api.ApiExceptions.PoemaApiError;
import belokonalexander.Api.ApiExceptions.PoemaErrorResolver;
import belokonalexander.Api.CurrentApi;
import belokonalexander.Api.Models.SugarAdditional.SugarPoemaDB;
import belokonalexander.Api.QueryExecutorExplicit;
import belokonalexander.GlobalShell;
import belokonalexander.SharedAppPrefs;
import ru.belokonalexander.poema.PageManagers.BackendRunner;
import ru.belokonalexander.poema.SplashScreen;

/**
 * Created by admin on 26.08.2016.
 */
public class TokenExpired extends PoemaErrorResolver{

    public TokenExpired(Context context) {
        super(context);
    }

    @Override
    protected Boolean resolve(PoemaApiError error) {

        int errorCode = PoemaErrorResolver.convertInAppErrorCode(error);

          if(errorCode== PoemaErrors.API_TOKEN_EXPIRED_ERROR)
        {
            resolveAction();

            return true;
        }

        return false;
    }

    @Override
    protected void resolveAction() {

        SugarPoemaDB.getInstance().clearDatabase();
        CurrentApi.getInstanse().getApplicationApi().stopMessageingService();
        SharedAppPrefs.getInstance().clearUserData();
        //TODO token don't exists and we can't delete device
        /*QueryExecutorExplicit registerDevice = new QueryExecutorExplicit(GlobalShell.getParentHash(this.getClass()), resultWaiter -> CurrentApi.getApplicationApi().unregisterDevice(resultWaiter));
        BackendRunner backendRunner = new BackendRunner(answers -> {
            LogSystem.logThis("Девайс удален: " + CurrentApi.getCurrentUser().getObjectId());
        }, messsage -> {
        }, registerDevice);
        backendRunner.run();*/

        GlobalShell.openActivityWithCloseThis(context, SplashScreen.class);

    }

}
