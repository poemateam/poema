package belokonalexander.Api.Models;


import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import StaticHelper.Settings;
import belokonalexander.GlobalShell;
import ru.belokonalexander.poema.CustomViews.GlideCircleTransformation;
import ru.belokonalexander.poema.Fragments.ListAdapters.SearchAdapter;
import ru.belokonalexander.poema.Fragments.Profile;
import ru.belokonalexander.poema.MainActivity;
import ru.belokonalexander.poema.R;

/**
 * Created by alexander on 13.09.2016.
 */
public class UserSearchViewHolder extends RecyclerView.ViewHolder {

    private TextView userName;
    private TextView userStatus;
    private ImageView userAvatar;
    private CardView itemView;
    private ImageButton friendsAction;

    boolean isControllerActive;
    SearchAdapter parentAdapter;

    public interface ITSearchUser {
        void mainClick(User user);
    }

    protected void setParentAdapter(SearchAdapter searchAdapter){
        this.parentAdapter = searchAdapter;
    }

    UserSearchViewHolder(View itemView, ITSearchUser itSearchUser) {
        super(itemView);
        userName = (TextView) itemView.findViewById(R.id.user_name);
        userStatus = (TextView) itemView.findViewById(R.id.user_status);
        userAvatar = (ImageView) itemView.findViewById(R.id.user_avatar);

        itemView = (CardView) itemView.findViewById(R.id.item_view);
        itemView.setOnClickListener(v -> itSearchUser.mainClick((User) parentAdapter.getItem(getLayoutPosition())));

    }



    protected static RecyclerView.ViewHolder Builder(ViewGroup parent, SearchAdapter parentAdapter){

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_serach_friends,parent, false);
        UserSearchViewHolder viewHolder = new UserSearchViewHolder(v, user -> {
            //TODO Intent for profile
            Profile openFriendProfile = new Profile();
            Bundle bundle = new Bundle();
            bundle.putString("UserID", user.getObjectId());
            bundle.putString("Name", user.getName());
            openFriendProfile.setArguments(bundle);

            ((MainActivity)parentAdapter.getFragmentHolder().getActivity()).openInThisContainer(openFriendProfile);
        });

        viewHolder.setParentAdapter(parentAdapter);
        return viewHolder;
    }

    protected static void onBindViewHolder(RecyclerView.ViewHolder holder, SearchedEntity entity, int position, Context context){
        UserSearchViewHolder castHolder = (UserSearchViewHolder) holder;
        User user = (User) entity;
        castHolder.userName.setText(user.getName());
        if(user.getReal_name().length()>0) {
            castHolder.userStatus.setText(user.getReal_name());
            castHolder.userStatus.setVisibility(View.VISIBLE);
        } else {
            castHolder.userStatus.setVisibility(View.GONE);
        }
        castHolder.userName.setText(user.getName());
        Settings.setCommonOptions(Glide.with(context).load(user.getSmallImage()).error(Settings.getDefaultImage(context, user)).override(GlobalShell.dpToPixels(40), GlobalShell.dpToPixels(40)).transform(new GlideCircleTransformation(context))).into(castHolder.userAvatar);

    }







}
