package ru.belokonalexander.poema.Interfaces;

import ru.belokonalexander.poema.CustomViews.CustomScrollView;

/**
 * Created by admin on 16.08.2016.
 */
public interface OnScrollViewListener {

    void onScrollChanged(CustomScrollView v, int x, int y, int oldl, int oldt );

}
