package ru.belokonalexander.poema.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;


import StaticHelper.LogSystem;
import belokonalexander.Api.Models.SugarAdditional.SugarPoemaDB;
import belokonalexander.GlobalShell;
import belokonalexander.SharedAppPrefs;
import butterknife.BindView;
import butterknife.ButterKnife;
import ru.belokonalexander.poema.MainActivity;
import ru.belokonalexander.poema.R;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Alexander on 09.03.2017.
 */

public class SettingsFragment extends PreferenceFragmentCompat {

     @BindView(R.id.app_toolbar)
     Toolbar toolbar;

    SharedPreferences sp;

    @Override
    public void onCreatePreferences(Bundle bundle, String s) {

        getPreferenceManager().setSharedPreferencesName(SharedAppPrefs.getInstance().getMainUserSettings());

        addPreferencesFromResource(R.xml.app_prefs);

        Preference caching = findPreference("_clear_cache");
        caching.setOnPreferenceClickListener(preference -> {
            Observable.just("")
                    .observeOn(Schedulers.newThread())
                    .map(s1 -> {
                        SugarPoemaDB.getInstance().clearDatabase();
                        return getResources().getString(R.string.size)+": " + SugarPoemaDB.getInstance().getSizeOfDB();
                    }).observeOn(AndroidSchedulers.mainThread()).subscribe(caching::setSummary);

            return true;
        });
        caching.setSummary(getResources().getString(R.string.processing));
        Observable.just("").observeOn(Schedulers.newThread()).map(s12 -> getResources().getString(R.string.size)+": " + SugarPoemaDB.getInstance().getSizeOfDB()).observeOn(AndroidSchedulers.mainThread()).subscribe(caching::setSummary);

        findPreference("_use_caching").setOnPreferenceChangeListener((preference, o) -> {
            Boolean res = (Boolean) o;
            if(!res){
                Observable.just("")
                        .observeOn(Schedulers.newThread())
                        .map(s1 -> {
                            SugarPoemaDB.getInstance().clearDatabase();
                            return getResources().getString(R.string.size)+": " + SugarPoemaDB.getInstance().getSizeOfDB();
                        }).observeOn(AndroidSchedulers.mainThread()).subscribe(caching::setSummary);

            }
            return true;
        });


    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        toolbar.setContentInsetStartWithNavigation(0);
        toolbar.setTitle(getResources().getString(R.string.settings));
        toolbar.setNavigationIcon(R.drawable.ic_menu_white_36dp);

        toolbar.setNavigationOnClickListener(v -> ((MainActivity) getActivity()).openNavigation());

    }


}
