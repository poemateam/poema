package ru.belokonalexander.poema.Fragments;

import android.animation.Animator;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;


import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.io.File;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import StaticHelper.CommandWithProperty;
import StaticHelper.Decorator;
import StaticHelper.LogSystem;
import StaticHelper.Command;
import StaticHelper.PopupCallback;
import StaticHelper.Settings;
import belokonalexander.Api.CurrentApi;
import belokonalexander.Api.LocalQueryExecutorExplicit;
import belokonalexander.Api.Models.FriendsRelate;
import belokonalexander.Api.Models.SugarAdditional.SugarPoemaDB;
import belokonalexander.Api.Models.User;
import belokonalexander.Api.QueryExecutor;
import belokonalexander.Api.QueryExecutorExplicit;
import belokonalexander.DevicePrefs;
import belokonalexander.GlobalShell;
import belokonalexander.RxCommonElements.AnswerApiRx;
import belokonalexander.RxCommonElements.UserRelation.UserRelationController;
import butterknife.BindDimen;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.belokonalexander.poema.CustomViews.CustomScrollView;
import ru.belokonalexander.poema.CustomViews.GlideCircleTransformation;
import ru.belokonalexander.poema.MainActivity;
import ru.belokonalexander.poema.PageManagers.ApiCachedFragment;
import ru.belokonalexander.poema.PageManagers.ApiFragment;
import ru.belokonalexander.poema.PageManagers.Types.CustomData;
import ru.belokonalexander.poema.R;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by admin on 10.08.2016.
 */
public class Profile extends ApiCachedFragment implements CustomData{

    @BindView(R.id.main_scroller)
    CustomScrollView pageScroller;

    @BindView(R.id.short_friends_list)
    ViewGroup shortFriendList;

    @BindView(R.id.profile_header)
    RelativeLayout headerLayout;

    @BindView(R.id.user_name_image)
    TextView userNameImage;

    @BindView(R.id.profile_avatar)
    ImageView userAvatar;



    @BindView(R.id.avatar_progress_bar)
    ProgressBar avatarProgressBar;


    @BindView(R.id.friends_show_all)
    View friendsShowMore;

    @BindView(R.id.animate_layout)
    LinearLayout animateLayout;
    private List<User> viewedUserShortFriendList;

    @OnClick(R.id.friends_show_all)
    void onClick(){

        FriendList openFriendList = new FriendList();
        openFriendList.setOwner(viewedUser);

        openNewFragment(openFriendList);

    }


    @BindDimen(R.dimen.text_size_normal)
    int textNormalSize;

    @BindDimen(R.dimen.fab_small)
    int fabSize;

    @BindView(R.id.app_toolbar)
    Toolbar toolbar;

    @BindView(R.id.user_relation_controller)
    UserRelationController userRelationController;

    @BindView(R.id.user_content)
    ViewGroup userContent;

    @BindView(R.id.user_havent_friends)
    View haventFriendsTitle;

    @BindView(R.id.friends_content)
    ViewGroup friendsContent;

    @BindView(R.id.friends_header)
    View friendsHeader;

    User viewedUser;

    @BindViews({R.id.user_name_edit, R.id.user_birthday_edit, R.id.user_city_edit, R.id.user_phone_edit, R.id.user_email_edit, R.id.user_sex_checkbox_edit })
    List<View> userInfoFields;

    @OnClick(R.id.user_header)
    void headerUserInfoOpenClose(View view)
    {
        closeOpenMenu((ViewGroup) view,userContent);
    }

    @OnClick(R.id.friends_header)
    void headerFriendsOpenClose(View view)
    {
        closeOpenMenu((ViewGroup) view,friendsContent);
    }

    boolean miniatureIsVisible = false;


    public void showPostObjects() {

        if(viewedUser.getObjectId().equals(CurrentApi.getInstanse().getCurrentUser().getObjectId()))
            return;


        userRelationController.setUser(viewedUser, (from, user) -> {
            // i deleted user from friend list
            if (from == FriendsRelate.RelateStates.FRIENDS && user.getFriendStatusDesc() == FriendsRelate.RelateStates.NONE) {
                for (int i = 0; i < viewedUserShortFriendList.size(); i++) {
                    if (viewedUserShortFriendList.get(i).getObjectId().equals(CurrentApi.getInstanse().getCurrentUser().getObjectId())) {
                        viewedUserShortFriendList.remove(i);
                        updateUserArea();
                        break;
                    }
                }
            } else if (from == FriendsRelate.RelateStates.I_GOT_FRIENDSHIP && user.getFriendStatusDesc() == FriendsRelate.RelateStates.FRIENDS) {
                viewedUserShortFriendList.add(CurrentApi.getInstanse().getCurrentUser());
                updateUserArea();
            }

        });
    }

    @Override
    public ApiFragment getFragment() {
        return this;
    }

    public interface PhotoLogic
    {
        void uploadNewPhoto(File file);
    }

    private PhotoLogic photoLogic;

    public PhotoLogic getPhotoLogic() {

        if(photoLogic==null)
            photoLogic = file -> Observable.just(file)
                    .filter(f->
                    {

                        //LogSystem.makeToast(getActivity(),GlobalShell.getFileExtension(file), LogSystem.ToastType.Success,LogSystem.TOAST_POSITION_CENTER);

                        if(Settings.isImageTypeSupported(GlobalShell.getFileExtension(file))){
                                imageIsUpdating();
                                return true;
                        }

                        imageUpdateError(getString(R.string.incorrect_extension_of_file));
                        return false;
                    })
                    .observeOn(Schedulers.newThread())
                    .flatMap(func -> Observable.create(obs -> {
                        waiter = new QueryExecutor(GlobalShell.getParentHash(Profile.class));
                        obs.onNext(updateUserImage(func));
                        obs.onCompleted();
                    }))
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(r ->
                    {
                        AnswerApiRx.customizeRxSubsribeAction(r, new AnswerApiRx.CustomizeAction() {
                            @Override
                            public void onSuccess() {
                                //TODO  if exists
                                try {
                                    Settings.setCommonOptions(Glide.with(getContext()).load(file).error(Settings.getDefaultImage(getContext(), viewedUser))).into(userAvatar);
                                    imageFinishedUpdate();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onFailure(String message) {
                                imageUpdateError(message);
                            }

                            @Override
                            public void onCanceled() {
                                if(!waiter.isRunning())
                                    imageLoadingCanceled();
                            }
                        });

                    });
        return photoLogic;
    }


    public void imageLoadingCanceled()
    {
        avatarProgressBar.setVisibility(View.GONE);
    }

    public void imageIsUpdating()
    {
        avatarProgressBar.setVisibility(View.VISIBLE);
    }

    public void imageFinishedUpdate()
    {
        avatarProgressBar.setVisibility(View.GONE);
        ((MainActivity)getActivity()).updateHeaderProfile();
        LogSystem.makeToast(getActivity(), getString(R.string.image_success_upload), LogSystem.ToastType.Success, LogSystem.TOAST_POSITION_BOTTOM);
    }

    public void imageUpdateError(String string)
    {
        try {
            avatarProgressBar.setVisibility(View.GONE);
            LogSystem.makeToast(getActivity(), string, LogSystem.ToastType.Error, LogSystem.TOAST_POSITION_BOTTOM);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    QueryExecutor waiter;
    private Object updateUserImage(File file) {
        return waiter.execute(resultWaiter -> CurrentApi.getInstanse().getApplicationApi().uploadUserImage(file,resultWaiter));
    }





    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if(getRootView()!=null)
            return getRootView();



        View view = inflater.inflate(R.layout.fragment_profile,container,false);
        setRootView(view);

        ButterKnife.bind(this, view);

        initApiFragment();
        calculatePageSizes();

        (getToolbar().getBackground()).setAlpha(255);


        pageScroller.setOnScrollViewListener(GlobalShell.customHeaderScroll(getToolbar(), headerLayout.getLayoutParams().height - DevicePrefs.getActinBarSize(getContext()),null,null));

        fillUserData();

        return view;
    }

    @Override
    public Toolbar getToolbar() {

        return toolbar;
    }


    Subscriber<Object> dataBindingSubscriber = null;


    private void updateUserArea(){



        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        userNameImage.setText(viewedUser.getName());
        if(viewedUser.getObjectId().equals(CurrentApi.getInstanse().getCurrentUser().getObjectId()))
            userNameImage.setVisibility(View.VISIBLE);
        else {
            getToolbar().setTitle(viewedUser.getName());
            userNameImage.setVisibility(View.GONE);
        }

        if(shortFriendList.getChildCount()==0)
        {
            Collections.shuffle(viewedUserShortFriendList);
        } else {

            //region remove unused
            for(int i =0; i<shortFriendList.getChildCount(); i++){
                View item = shortFriendList.getChildAt(i);
                String objectID = (String) item.getTag();

                boolean solution = true;

                for(int j =0; j< viewedUserShortFriendList.size(); j++){
                    if(viewedUserShortFriendList.get(j).getObjectId().equals(objectID)){
                        solution = false;
                        break;
                    }
                }

                if(solution) {

                    Animation animation = new AlphaAnimation(1f,0f);
                    animation.setDuration(300);

                    animation.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }
                        @Override
                        public void onAnimationEnd(Animation animation) {
                            shortFriendList.removeView(item);
                        }
                        @Override
                        public void onAnimationRepeat(Animation animation) {
                        }
                    });

                    item.startAnimation(animation);

                }
            }
            //endregion

        }

        //region wrap if open or unwrap if close
        if(viewedUserShortFriendList.size()==0){

            if(friendsContent.getVisibility()==View.VISIBLE)
                friendsHeader.callOnClick();

            friendsShowMore.setVisibility(View.GONE);
            haventFriendsTitle.setVisibility(View.VISIBLE);
            return;
        } else {

            if(friendsContent.getVisibility()!=View.VISIBLE)
                friendsHeader.callOnClick();

            haventFriendsTitle.setVisibility(View.GONE);

        }
        //endregion

            //add new items
            for(int i =0 ; i < viewedUserShortFriendList.size(); i++){

                User user = viewedUserShortFriendList.get(i);

                View item = shortFriendList.findViewWithTag(user.getObjectId());
                if(item==null){
                    //region add view
                    item = inflater.inflate(R.layout.item_friend_short,null);
                    item.setTag(user.getObjectId());

                    ((TextView)item.findViewById(R.id.user_name)).setText(user.getName());
                    ImageView imageView = ((ImageView)item.findViewById(R.id.user_avatar));
                    Settings.setCommonOptions(Glide.with(getContext()).load(user.getSmallImage()).override(GlobalShell.dpToPixels(80),GlobalShell.dpToPixels(80)).transform(new GlideCircleTransformation(getContext())).error(Settings.getDefaultImage(getContext(), user))).into(imageView);

                    if(i==0){
                        item.setPadding(8,0,4,0);
                    } else if(i==viewedUserShortFriendList.size()-1)
                        item.setPadding(4,0,8,0);

                    item.setOnClickListener(view -> {
                        Profile openUserProfile = new Profile();
                        Bundle bundle = new Bundle();
                        bundle.putString("UserID", user.getObjectId());
                        bundle.putString("Name", user.getName());
                        openUserProfile.setArguments(bundle);
                        openNewFragment(openUserProfile);
                    });


                    shortFriendList.addView(item);

                    Animation animation = new AlphaAnimation(.5f,1f);
                    animation.setDuration(600);
                    item.startAnimation(animation);

                    //endregion
                }


        }

        //show or hide @more button
        if(viewedUserShortFriendList.size()==Settings.SHORT_USER_LIST_COUNT)
            friendsShowMore.setVisibility(View.VISIBLE);
        else friendsShowMore.setVisibility(View.GONE);//




    }

    public User getViewedUser(){
        return viewedUser;
    }




    private void fillUserData() {

        String viewTitle = "";

       // viewedUser = SugarPoemaDB.getUserById()

        if(viewedUser==null) {
            if (getArguments() == null || (getArguments().isEmpty() || getArguments().getString("UserID").equals(CurrentApi.getInstanse().getCurrentUser().getObjectId()))) {
                viewedUser = CurrentApi.getInstanse().getCurrentUser();

                viewTitle = getResources().getString(R.string.my_profile);
                QueryExecutorExplicit getShortFriendList = new QueryExecutorExplicit(GlobalShell.getParentHash(this.getClass()),
                        resultWaiter -> CurrentApi.getInstanse().getApplicationApi().getShortFriendsList(viewedUser.getObjectId(),resultWaiter));

                setBackendTasks(getShortFriendList);

                LocalQueryExecutorExplicit getShortFriendListL = new LocalQueryExecutorExplicit(GlobalShell.getParentHash(this.getClass()),
                        resultWaiter -> resultWaiter.setResult(SugarPoemaDB.getInstance().getShortFriends(viewedUser.getObjectId(),Settings.SHORT_USER_LIST_COUNT)));

                setLocalSettings(false, getShortFriendListL);

            } else {

                viewTitle = getArguments().getString("Name");

                QueryExecutorExplicit getUserData = new QueryExecutorExplicit(GlobalShell.getParentHash(this.getClass()),
                        resultWaiter -> CurrentApi.getInstanse().getApplicationApi().getUser(getArguments().getString("UserID"), resultWaiter));

                QueryExecutorExplicit getShortFriendList = new QueryExecutorExplicit(GlobalShell.getParentHash(this.getClass()),
                        resultWaiter -> CurrentApi.getInstanse().getApplicationApi().getShortFriendsList(getArguments().getString("UserID"),resultWaiter));

                //endregion
                setBackendTasks(getUserData, getShortFriendList);

                LocalQueryExecutorExplicit getUserDataL = new LocalQueryExecutorExplicit(GlobalShell.getParentHash(this.getClass()),
                        resultWaiter -> resultWaiter.setResult(SugarPoemaDB.getInstance().getUser(getArguments().getString("UserID"))));

                LocalQueryExecutorExplicit getShortFriendListL = new LocalQueryExecutorExplicit(GlobalShell.getParentHash(this.getClass()),
                        resultWaiter -> resultWaiter.setResult(SugarPoemaDB.getInstance().getShortFriends(getArguments().getString("UserID"),Settings.SHORT_USER_LIST_COUNT)));

                setLocalSettings(getUserDataL, getShortFriendListL);


            }
        }

        fillToolbar(viewTitle);

        buildFragment();
    }



    @Override
    public void showView(List<Object> data, boolean fromCache) {

        if(data.size()>1){
            viewedUser = (User) data.get(0);
            if(viewedUser==null && fromCache)
                return;

            viewedUserShortFriendList = (List<User>) data.get(1);

        } else {
            viewedUserShortFriendList = (List<User>) data.get(0);
        }



        if(((((ColorDrawable) getToolbar().getBackground()).getColor()) >>> 24) > 250) {
            hideoffToolbar();
        }


            showPostObjects();


        afterUserDataWasLoaded(fromCache);
    }



    private void hideoffToolbar() {

        ObjectAnimator animator = ObjectAnimator.ofObject(getToolbar(), "backgroundColor", new ArgbEvaluator(), ContextCompat.getColor(getContext(),R.color.colorPrimaryAnother), ContextCompat.getColor(getContext(),R.color.colorPrimaryTransparent));

            animator.setDuration(600);

        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                try {
                    getToolbar().setBackgroundColor(ContextCompat.getColor(getContext(),R.color.colorPrimary));
                    getToolbar().getBackground().setAlpha(0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

        animator.start();
    }


    private RequestListener<String, GlideDrawable> requestListener = new RequestListener<String, GlideDrawable>() {
        @Override
        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
            // todo log exception
            LogSystem.logThis(" EXCEPTION"  + e);
            return false;
        }

        @Override
        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {

            return false;
        }
    };


    //add photo button already set
    boolean toolbarItemsAlreadyCreated = false;

    public void afterUserDataWasLoaded(boolean fromCache){

        updateUserArea();

        if(viewedUser.getObjectId().equals(CurrentApi.getInstanse().getCurrentUser().getObjectId())){

            if(!toolbarItemsAlreadyCreated){
                MenuItem updatePhotoMenuItem = getToolbar().getMenu()
                        .add(getResources().getString(R.string.update_photo));
                updatePhotoMenuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
                updatePhotoMenuItem.setIcon(R.drawable.ic_add_a_photo_white_36dp);
                updatePhotoMenuItem.setOnMenuItemClickListener(menuItem -> GlobalShell.startImageChooser(getActivity(),GlobalShell.PROFILE_IMAGE_CHANGED));
                toolbarItemsAlreadyCreated = true;
            }

        }
        bindingUserInfo();
        Settings.setCommonOptions(Glide.with(getContext()).load(viewedUser.getImageUrl()).listener(requestListener).error(Settings.getDefaultImage(getContext(), viewedUser))).into(userAvatar);

    }



    private void calculatePageSizes() {


        avatarProgressBar.bringToFront();
        int minHeaderHeight = getResources().getDimensionPixelOffset(R.dimen.image_header_profile_height);
        int headerHeight = Math.max((int)(DevicePrefs.getDisplay().heightPixels/2),minHeaderHeight);

        headerLayout.getLayoutParams().height = headerHeight;

    }


    void closeOpenMenu(ViewGroup parent, View child)
    {

        ImageView imageView = null;

        //Using CardView click

        ViewGroup header = ((ViewGroup) parent.getChildAt(0));
        for(int i =0; i < header.getChildCount(); i++)
        {
            View v = header.getChildAt(i);
            if(v instanceof ImageView)
                imageView = (ImageView) v;
        }

        if(child.getVisibility()!=View.VISIBLE) {
            child.setVisibility(View.VISIBLE);

            imageView.setImageResource(R.drawable.ic_keyboard_arrow_up_white_36dp);

            int transition = child.getMeasuredHeight();
            if(transition<=0){
                child.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
                transition = child.getMeasuredHeight();
            }

            final int finalTransition = transition;
            parent.setEnabled(false);

            Observable.just("")
                    .delay(300, TimeUnit.MILLISECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .filter(o -> {
                        LogSystem.logThis(" SCROLLING !!111 : " +  pageScroller.getCurrentScrollY() + " /// " + finalTransition);

                        pageScroller.smoothScrollTo(0, pageScroller.getCurrentScrollY() + finalTransition);
                        return true;
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(o -> {
                        parent.setEnabled(true);
                    });


        }
        else {
            child.setVisibility(View.GONE);
            imageView.setImageResource(R.drawable.ic_keyboard_arrow_down_white_36dp);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(dataBindingSubscriber!=null)
            dataBindingSubscriber.unsubscribe();
    }

    private void bindingUserInfo(){

        boolean isMyProfile = viewedUser.getObjectId().equals(CurrentApi.getInstanse().getCurrentUser().getObjectId());

        for(View field : userInfoFields)
        {

            if(!isMyProfile){
                field.setEnabled(false);
            }

            if(field instanceof EditText){
                Drawable[] d = ((EditText) field).getCompoundDrawables();
                for(int i=0; i<d.length; i++){
                   if(d[i]==null) continue;
                    d[i].setColorFilter(getResources().getColor(R.color.normal_text_color), PorterDuff.Mode.MULTIPLY);
                }
                ((EditText) field).setCompoundDrawablesWithIntrinsicBounds(d[0], d[1], d[2], d[3]);
            }

            CommandWithProperty delegate = null;
            AlertDialog popup = null;
            final PopupCallback[] popupCallback = {null};

            switch (field.getId())
            {
                case R.id.user_name_edit:

                    EditText userName = (EditText) field;

                    (userName).setText(viewedUser.getReal_name());


                    delegate = value -> {
                        String propertyName = viewedUser.setReal_name(value.toString());
                        userName.setText(value.toString());
                        return propertyName;
                    };
                    break;

                case R.id.user_birthday_edit:

                    EditText  stringBirthday = (EditText) field;

                    stringBirthday.setText(viewedUser.getStringBirthday());

                    delegate = value -> {

                        try {
                            String propertyName =viewedUser.setBirthday(value.toString());
                            stringBirthday.setText(viewedUser.getStringBirthday());
                            return propertyName;
                        } catch (ParseException e) {
                            LogSystem.logThis("ERROR WITH DATE");
                        }

                        return null;
                    };

                    Calendar initPopup = Calendar.getInstance();
                    if (viewedUser.getBirthday()!=null) {
                    initPopup = viewedUser.getBirthdayCalendar();
                    }
                    popup = new DatePickerDialog(getContext(), (view, year, monthOfYear, dayOfMonth) -> {
                        Calendar cal = Calendar.getInstance();
                        cal.set(year, monthOfYear, dayOfMonth);
                        popupCallback[0].onResult(Settings.dateFormat.format(cal.getTime()));
                    }, initPopup.get(Calendar.YEAR), initPopup.get(Calendar.MONTH), initPopup.get(Calendar.DAY_OF_MONTH));


                    break;

                case R.id.user_city_edit:

                    EditText address = (EditText) field;

                    address.setText(viewedUser.getAddress());

                    delegate = value -> {
                        String propertyName = viewedUser.setAddress(value.toString());
                        address.setText(value.toString());
                        return propertyName;
                    };
                    break;

                case R.id.user_phone_edit:

                    EditText userPhone = (EditText) field;

                    userPhone.setText(viewedUser.getPhone());

                    delegate = value -> {
                        String propertyName = viewedUser.setPhone(value.toString());
                        ((EditText)field).setText(value.toString());
                        return propertyName;
                    };
                    break;

                case R.id.user_email_edit:
                    ((EditText)field).setText(viewedUser.getEmail());
                    if(!isMyProfile){
                        field.setVisibility(View.GONE);
                    }
                    break;

                case R.id.user_sex_checkbox_edit:

                    RadioGroup sexSelectArea = (RadioGroup)field;

                    if(viewedUser.getSex()==null)
                        ((RadioButton)(sexSelectArea.getChildAt(0))).setChecked(true);
                    else if (viewedUser.getSex())
                        ((RadioButton)(sexSelectArea.getChildAt(1))).setChecked(true);
                    else
                        ((RadioButton)(sexSelectArea.getChildAt(2))).setChecked(true);

                    delegate = value -> {
                        Integer val = (Integer) value;
                        String propertyName;
                        if(val==0)
                            propertyName = viewedUser.setSex(null);
                        else if (val==1) {
                            propertyName = viewedUser.setSex(true);
                        } else {
                            propertyName = viewedUser.setSex(false);
                        }

                        ((RadioButton)(sexSelectArea.getChildAt(val))).setChecked(true);
                        return propertyName;
                    };

                    break;



                default:

                    break;

            }




            if(!isMyProfile)
                resolveViewShowing(field);

            final CommandWithProperty delegateFinal = delegate;
            final AlertDialog popupFinal = popup;


            if(field instanceof EditText && field.isFocusable()){

                ((EditText)field).setSelection(((EditText)field).length());

                field.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                public String beforeValue;
                public String afterValue;

                @Override
                public void onFocusChange(View v, boolean hasFocus) {

                    //LogSystem.logThis("FOCUS CHANGED");

                    if (!hasFocus) {
                        afterValue = ((EditText) v).getText().toString();

                        if(afterValue.equals(beforeValue)) return;

                        //LogSystem.logThis("BEFORE: " + beforeValue);
                        //LogSystem.logThis("AFTER: " + afterValue);
                        //TODO check validation

                        String property = delegateFinal.setter(((EditText) v).getText());

                        updateUser(v,delegateFinal,beforeValue, property, null);

                    } else {
                        beforeValue = ((EditText) v).getText().toString();
                    }



                }
            });}
            else if (field instanceof EditText && !((EditText)field).isFocusable())
            {
                //region popup actions
                // this fields types open popup and set result in Edittext
                // so update should be after edit text changed
                EditText purpose = (EditText) field;
                final String[] beforeVal = {purpose.getText().toString()};
                purpose.setOnClickListener(v -> {
                    // delegate change textedit to popup
                    purpose.setFocusableInTouchMode(true);
                    purpose.requestFocus();
                    popupFinal.show();
                    popupFinal.setOnDismissListener(dialog -> {
                                LogSystem.logThis("DISMISS");
                                purpose.setFocusableInTouchMode(false);
                                purpose.clearFocus();
                            }
                    );

                    popupCallback[0] = result -> {
                        if(!result.toString().equals(beforeVal[0])){
                            String property = delegateFinal.setter(result.toString());
                            updateUser(purpose, delegateFinal, beforeVal[0], property, value -> beforeVal[0] = result.toString());
                        }
                    };
                });
                //endregion
            }
            else if (field instanceof Spinner)
            {
                Spinner spinner = (Spinner) field;
                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    boolean firstInit = true;
                    int beforePosition;
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        if (firstInit) {
                            LogSystem.logThis("First init");
                            beforePosition = position;
                            firstInit = false;
                        } else {
                            if (beforePosition == position) return;
                            String property = delegateFinal.setter(position);
                            updateUser(spinner, delegateFinal, beforePosition, property, beforeValue -> beforePosition = position);
                        }
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            } else if (field instanceof RadioGroup){

                //TODO
                RadioGroup group = (RadioGroup) field;

                final int finalPos = GlobalShell.getCurrentCheckedPosition((RadioGroup)field);
                group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

                    int beforePosition = finalPos;

                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        RadioButton selected = null;
                        int pos = 0;
                        for(int i =0; i < group.getChildCount(); i++){
                            RadioButton button = (RadioButton) group.getChildAt(i);
                            if(button.isChecked()){
                                pos = i;
                                selected = button;
                                break;
                            }
                        }

                        LogSystem.logThis("CHECKED: " + pos + " BEFORE: " + beforePosition);
                        String property = delegateFinal.setter(pos);
                        final int finalPos1 = pos;
                        updateUser(selected, delegateFinal, beforePosition, property, v -> beforePosition = finalPos1);
                    }
                });

            }


        }
    }

    private View getWrapper(View caller){
        View callerEditable = caller;

        while (!((callerEditable = (View) callerEditable.getParent()) instanceof RelativeLayout)){
            if(callerEditable.getRootView()==callerEditable.getParent()){
                throw new NullPointerException();

            }
        }

        RelativeLayout wrapper = (RelativeLayout)callerEditable;

        return wrapper;
    }

    private void resolveViewShowing(View caller) {

        boolean isHidden = false;

        if(caller instanceof EditText){
            if(((EditText) caller).getText().length()==0){
                isHidden = true;
            }
        } else if(caller instanceof RadioGroup) {
            RadioGroup radioGroup = (RadioGroup) caller;
            if(((RadioButton)radioGroup.getChildAt(0)).isChecked()){
                isHidden = true;
            } else {
                for(int i =0; i < radioGroup.getChildCount(); i++){
                    RadioButton button = (RadioButton) radioGroup.getChildAt(i);
                    if (!button.isChecked()){
                        button.setVisibility(View.GONE);
                    } else {

                        button.setVisibility(View.VISIBLE);
                        ((LinearLayout.LayoutParams)button.getLayoutParams()).setMargins(0,0,0,0);

                    }
                }
            }
        }

        View callerWrapper = getWrapper(caller);

        if(isHidden) callerWrapper.setVisibility(View.GONE);
         else callerWrapper.setVisibility(View.VISIBLE);

    }

    private void updateUser(View caller, CommandWithProperty action, Object beforeValue, String propertyName, Command beforeValueUpdater) {

        //wrapper - is parent RL, so wee need to deepen from caller and find this


        RelativeLayout wrapper = (RelativeLayout)getWrapper(caller);


        ProgressBar loadingIcon = GlobalShell.inflateWithProgressBar(getContext(), wrapper, 30, View.GONE, (caller instanceof RadioButton) ? 2 : 0, 0);

        loadingIcon.setVisibility(View.VISIBLE);

        Observable.just("")
                .observeOn(Schedulers.newThread())
                .flatMap(func -> Observable.create(obs -> {
                    QueryExecutor waiter = new QueryExecutor(GlobalShell.getParentHash(this.getClass(),action.toString()));
                    obs.onNext(waiter.execute(resultWaiter -> CurrentApi.getInstanse().getApplicationApi().updateCurrentUser(resultWaiter)));
                    obs.onCompleted();
                }))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(r ->
                {

                    AnswerApiRx.customizeRxSubsribeAction(r, new AnswerApiRx.CustomizeAction() {
                        @Override
                        public void onSuccess() {

                            try {
                                if(beforeValueUpdater!=null)
                                    beforeValueUpdater.setter(beforeValue);
                                Decorator.runResultUpdateAnimation(getContext(), caller,true);
                                loadingIcon.setVisibility(View.GONE);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onFailure(String message) {
                            try {
                                action.setter(beforeValue);
                                loadingIcon.setVisibility(View.GONE);
                                Decorator.runResultUpdateAnimation(getContext(), caller, false);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onCanceled() {

                        }
                    });

                });
    }


    @Override
    public void onStart() {
        super.onStart();
        //EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        //EventBus.getDefault().unregister(this);
        super.onStop();
    }

    /*@Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(SugarEvent event){
        LogSystem.makeToast(getActivity(),event.message, LogSystem.ToastType.Success, LogSystem.TOAST_POSITION_CENTER);
    }*/
}
