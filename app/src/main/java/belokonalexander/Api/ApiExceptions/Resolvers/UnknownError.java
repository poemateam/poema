package belokonalexander.Api.ApiExceptions.Resolvers;

import android.content.Context;

import StaticHelper.LogSystem;

import belokonalexander.Api.ApiExceptions.PoemaApiError;
import belokonalexander.Api.ApiExceptions.PoemaErrorResolver;

import ru.belokonalexander.poema.PoemaApplication;
import ru.belokonalexander.poema.R;

/**
 * Created by admin on 26.08.2016.
 */
public class UnknownError extends PoemaErrorResolver{

    public UnknownError(Context context) {
        super(context);
    }


    Exception e;

    @Override
    protected Boolean resolve(PoemaApiError error) {

        e = error;

            int errorCode = PoemaErrorResolver.convertInAppErrorCode(error);
            resolveAction();
        if(errorCode==14005 || errorCode==15000) {
            LogSystem.makeToast(((PoemaApplication)context.getApplicationContext()).getCurrentActivity(),context.getResources().getString(R.string.unknown_error_server)+": " + errorCode, LogSystem.ToastType.Error,LogSystem.TOAST_POSITION_BOTTOM);
        }
        else
            LogSystem.makeToast(((PoemaApplication)context.getApplicationContext()).getCurrentActivity(),context.getResources().getString(R.string.unknown_error)+": " + errorCode, LogSystem.ToastType.Error,LogSystem.TOAST_POSITION_BOTTOM);

        return true;
    }

    @Override
    protected void resolveAction() {

        //empty

        }


}
