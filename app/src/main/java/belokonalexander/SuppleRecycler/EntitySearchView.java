package belokonalexander.SuppleRecycler;

import android.content.Context;
import android.graphics.PorterDuff;
import android.support.v7.app.AlertDialog;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.jakewharton.rxbinding.support.v7.widget.RxSearchView;
import com.jakewharton.rxbinding.support.v7.widget.SearchViewQueryTextEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import StaticHelper.LogSystem;
import StaticHelper.Settings;

import belokonalexander.Api.Models.Annotations.DefaultAdditionSearchData;
import belokonalexander.Api.Models.Annotations.SearchItem;
import belokonalexander.Api.Models.SearchedEntity;
import ru.belokonalexander.poema.R;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;

/**
 * Created by alexander on 10.10.2016.
 */
public class EntitySearchView extends android.support.v7.widget.SearchView {


    public EntitySearchView(Context context) {
        super(context);
    }

    public EntitySearchView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

     int keyPosition = 0;


    //EditText searchTextView;

    AlertDialog.Builder alertDialog;


    EditText queryArea;

    public enum QueryTypeSettings{
        LOCAL_QUERY(Settings.SEARCH_DEBOUNCE_LOCAL,Settings.THROTTLE_CLICK_VALUE_LOCAL),
        REMOTE_QUERY(Settings.SEARCH_DEBOUNCE,Settings.THROTTLE_CLICK_VALUE);

        int debounce;
        int throttle;

        public int getDebounce() {
            return debounce;
        }

        public int getThrottle() {
            return throttle;
        }

        QueryTypeSettings(int searchDebounce, int throttleValue) {
            debounce = searchDebounce;
            throttle = throttleValue;
        }
    }

    QueryTypeSettings queryTypeSettings = QueryTypeSettings.REMOTE_QUERY;

    List<SearchItem> searchParams = new ArrayList<>();

    Class < ? extends SearchedEntity> searchedEntity;
    boolean dummyFirstEmmit = false;
    LinearLayout container;

    public EditText getQueryArea(){
        return queryArea;
    }

    Context context;

    public void initSearch(Class < ? extends SearchedEntity> searchedEntity, Context context) {

        this.context = context;

        this.initStyles();

        this.searchedEntity = searchedEntity;

        searchParams = DefaultAdditionSearchData.getSearchFieldsAndType(searchedEntity, context);

        this.initSegmentController();

    }

    public void initSearch(Class < ? extends SearchedEntity> searchedEntity, Context context, QueryTypeSettings queryTypeSettings) {

        this.queryTypeSettings = queryTypeSettings;
        this.initSearch(searchedEntity, context);

    }

    private void initStyles() {


        //тектовое поле поиска
        EditText searchTextView = (EditText) this.findViewById(R.id.search_src_text);
        queryArea = searchTextView;
        ViewGroup searchPlate = (ViewGroup) this.findViewById(R.id.search_plate);
        //кнопка отмены
        ImageView closeBtn = (ImageView) this.findViewById(R.id.search_close_btn);
        closeBtn.setPadding(0, 0, 8, 0);
        searchTextView.setPadding(8, 0, 0, 0);
        //кнопка для фильтра (выбор параметра поиска)
        ImageView imageView = new ImageView(getContext());
        imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_receipt_white_24dp));
        imageView.setColorFilter(getResources().getColor(android.R.color.white), PorterDuff.Mode.MULTIPLY);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
        imageView.setPadding(8, 0, 8, 0);
        imageView.setLayoutParams(lp);
        imageView.setClickable(true);
        imageView.setOnClickListener(v -> onParamsKeyClick());
        searchPlate.addView(imageView, 0);

        container = (LinearLayout) this.findViewById(R.id.search_edit_frame);
        ((LinearLayout.LayoutParams)container.getLayoutParams()).setMargins(0, 0, 0, 0);


    }



    private void initSegmentController() {

        alertDialog = new AlertDialog.Builder(getContext());
        alertDialog.setTitle(getContext().getResources().getString(R.string.search_params));
        alertDialog.create();
        onChangeSearchParameter(keyPosition);

    }

    private void onParamsKeyClick()
    {
        if(searchParams.size()==0) return;

        String[] array = new String[searchParams.size()];
        for(int i=0; i<searchParams.size();i++)
            array[i] = searchParams.get(i).getAlias().substring(0,1).toUpperCase() + searchParams.get(i).getAlias().substring(1);

        alertDialog.setSingleChoiceItems(array, keyPosition, (dialog, which) -> {


            onChangeSearchParameter(which);

            Timer timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    dialog.dismiss();
                }
            }, 200);
        });

        alertDialog.show();
    }



    private SearchItem getCurrentSearchItem()
    {
        return searchParams.get(keyPosition);
    }


    public void onChangeSearchParameter(int index)
    {
        //TODO do one rxmethod for debounce and throttle
        keyPosition = index;

        queryArea.setHint(searchParams.get(keyPosition).getAlias().substring(0, 1).toUpperCase() + searchParams.get(keyPosition).getAlias().substring(1));

        this.setQuery("", false);
        if(getCurrentSearchItem().getLazyType()) {
            this.setSubmitButtonEnabled(false);

            rxLazy();
        }
        else {
            this.setSubmitButtonEnabled(true);

            rxNotLazy();
        }

    }


    private void rxLazy()
    {

        dummyFirstEmmit = false;



        RxSearchView.queryTextChangeEvents(this)
                .filter(f->
                {
                    if(!dummyFirstEmmit)
                    {
                        dummyFirstEmmit = true;
                        return false;
                    }
                    return dummyFirstEmmit;
                })
                .debounce(queryTypeSettings.getDebounce(), TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .filter(f -> {
                    if(this.getQuery().length() == 0 ){
                        startEmpty();
                        return false;
                    }
                    return true;
                })
                .filter(f -> (getCurrentSearchItem().getLazyType() || (!getCurrentSearchItem().getLazyType() && f.isSubmitted())))
                .subscribe(f -> startSearch());
    }



    private void rxNotLazy()
    {

        dummyFirstEmmit = false;
        final int[] throttleTimeout = {1};
        RxSearchView.queryTextChangeEvents(this)
                .filter(f ->
                {
                    if (!dummyFirstEmmit) {
                        dummyFirstEmmit = true;
                        return false;
                    }
                    throttleTimeout[0] = queryTypeSettings.getThrottle();
                    return dummyFirstEmmit;
                })
                .throttleFirst(throttleTimeout[0], TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .filter(f -> {
                    if(this.getQuery().length() == 0 ){
                        startEmpty();
                        return false;
                    }
                    return true;
                })
                .filter(f -> (getCurrentSearchItem().getLazyType() || (!getCurrentSearchItem().getLazyType() && f.isSubmitted())))
                .subscribe(f -> startSearch());
    }


    SearchTask searchTask;

    public EntitySearchView setSearchTask(SearchTask searchTask) {
        this.searchTask = searchTask;
        return this;
    }

    void startSearch(){
        searchTask.startSearch();
    }

    void startEmpty(){
        searchTask.startEmpty();
    }

    public interface SearchTask{
        void startSearch();
        void startEmpty();
    }

}
