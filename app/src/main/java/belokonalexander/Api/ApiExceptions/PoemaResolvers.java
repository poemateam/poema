package belokonalexander.Api.ApiExceptions;

import android.content.Context;

import belokonalexander.Api.ApiExceptions.Resolvers.DublicateError;
import belokonalexander.Api.ApiExceptions.Resolvers.InternetConnectionError;
import belokonalexander.Api.ApiExceptions.Resolvers.TokenExpired;
import belokonalexander.Api.ApiExceptions.Resolvers.UnknownError;

/**
 * Created by admin on 26.08.2016.
 */
public class PoemaResolvers {

    public PoemaErrorResolver getInstance(Context context)
    {
        if(errorResolver==null)
        {
            //create queue of resolvers

            errorResolver = new TokenExpired(context);
            errorResolver.setNext(new InternetConnectionError(context)).setNext(new DublicateError(context)).setNext(new UnknownError(context));

        }

        return errorResolver;
    }


    PoemaErrorResolver errorResolver;




}
