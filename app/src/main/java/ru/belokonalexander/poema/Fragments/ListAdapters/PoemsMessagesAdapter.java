package ru.belokonalexander.poema.Fragments.ListAdapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import StaticHelper.LogSystem;
import StaticHelper.Settings;
import belokonalexander.Api.CurrentApi;
import belokonalexander.Api.Models.Poema;
import belokonalexander.Api.Models.PoemaMessage;
import belokonalexander.Api.Models.User;
import belokonalexander.GlobalShell;
import ru.belokonalexander.poema.CustomViews.GlideCircleTransformation;
import ru.belokonalexander.poema.Fragments.EditMessageDialog;
import ru.belokonalexander.poema.Fragments.NavigationFragment;
import ru.belokonalexander.poema.Fragments.Profile;
import ru.belokonalexander.poema.R;

/**
 * Created by alexander on 26.09.2016.
 */
public class PoemsMessagesAdapter extends CommonAdapter<PoemaMessage>  {

    Poema poema;

    public PoemsMessagesAdapter(List items, Fragment fragmentHolder, Poema poema) {
        super(items, fragmentHolder);
        this.poema = poema;
    }

    @Override
    RecyclerView.ViewHolder onCreateVH(ViewGroup parent, int viewType) {
        View v;
        if(viewType==1)
             v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_poem_messages_reverse,parent,false);
        else
             v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_poem_messages,parent,false);


        return new PoemsMessagesViewHolder(v, new Clicks() {
            @Override
            public void mainClick(PoemaMessage poemMessage) {

                LogSystem.logThis(" ID: " + poemMessage.getObjectId());

                int position = getPosition(poemMessage);




                if(poemMessage.isMyMessage()) {
                    if(poemMessage.getObjectId().length()>0) {
                        EditMessageDialog editMessageDialog = new EditMessageDialog();
                        editMessageDialog.show(fragmentHolder.getFragmentManager(), poema, poemMessage, PoemsMessagesAdapter.this);

                    } else {
                        LogSystem.makeToast(fragmentHolder.getActivity(),context.getResources().getString(R.string.message_is_loading), LogSystem.ToastType.Info, LogSystem.TOAST_POSITION_CENTER);
                    }
                } else {
                    LogSystem.makeToast(fragmentHolder.getActivity(),context.getResources().getString(R.string.you_can_edit), LogSystem.ToastType.Info, LogSystem.TOAST_POSITION_CENTER);
                }


            }

            @Override
            public void onLingClick(PoemaMessage poema) {

                LogSystem.logThis("LongClick");

            }
        });
    }

    @Override
    public int getItemViewType(int position) {

        PoemaMessage poemaMessage = items.get(position);

        if(poemaMessage.isMyMessage())
            return 1;

        return super.getItemViewType(position);
    }

    @Override
    void onBindVH(RecyclerView.ViewHolder holder, int position) {
        PoemsMessagesViewHolder holderMain = (PoemsMessagesViewHolder) holder;

        PoemaMessage poemaMessage = items.get(position);

        holderMain.message.setText(poemaMessage.getMessage());
        holderMain.messageTime.setText(Settings.getDate(poemaMessage.getCreated(), Settings.DateFormat.Short));




       try {
            if(GlobalShell.dayCount(items.get(position+1).getCreated())!=GlobalShell.dayCount(items.get(position).getCreated())) {
                showTimeLine(holderMain, poemaMessage);
            }
            else {
                holderMain.lineOne.setVisibility(View.GONE);
                holderMain.lineTwo.setVisibility(View.GONE);
                holderMain.messageDate.setVisibility(View.GONE);
            }
        } catch (IndexOutOfBoundsException e) {
          showTimeLine(holderMain, poemaMessage);
        }






        if(poemaMessage.isMyMessage()) {
            Settings.setCommonOptions(Glide.with(context).load(CurrentApi.getInstanse().getCurrentUser().getSmallImage()).override(GlobalShell.dpToPixels(40), GlobalShell.dpToPixels(40)).error(Settings.getDefaultImage(context,CurrentApi.getInstanse().getCurrentUser()))).transform(new GlideCircleTransformation(context)).into(holderMain.senderImage);
            holderMain.outerView.setBackgroundColor(ContextCompat.getColor(context, R.color.message_my));
        }
        else {
            Settings.setCommonOptions(Glide.with(context).load(poema.getAnotherUser().getSmallImage()).override(GlobalShell.dpToPixels(40), GlobalShell.dpToPixels(40)).error(Settings.getDefaultImage(context,poema.getAnotherUser()))).transform(new GlideCircleTransformation(context)).into(holderMain.senderImage);
            holderMain.outerView.setBackgroundColor(ContextCompat.getColor(context,R.color.message_other));
        }




        //Glide.with(context).load(user.getSmallImage()).override(GlobalShell.dpToPixels(40), GlobalShell.dpToPixels(40)).transform(new GlideCircleTransformation(context)).crossFade().into(portret);


    }

    private void showTimeLine(PoemsMessagesViewHolder holderMain, PoemaMessage poemaMessage) {
        holderMain.messageDate.setVisibility(View.VISIBLE);
        holderMain.messageDate.setText(Settings.getDate(poemaMessage.getCreated(), Settings.DateFormat.StandartDate));
        holderMain.lineOne.setVisibility(View.VISIBLE);
        holderMain.lineTwo.setVisibility(View.VISIBLE);
    }


    //callback for all methods which update adapter's items
    private class PoemsMessagesViewHolder extends RecyclerView.ViewHolder {

         CardView cv;
         RelativeLayout outerView;
         TextView message;
         ImageView senderImage;
         TextView messageTime;
         TextView messageDate;

         View lineOne;
         View lineTwo;


         PoemsMessagesViewHolder(View itemView, Clicks clicks) {
            super(itemView);

            cv = (CardView)itemView.findViewById(R.id.item_view);

            messageTime = (TextView) itemView.findViewById(R.id.messasge_time);
            message = (TextView) itemView.findViewById(R.id.message);
            messageDate = (TextView) itemView.findViewById(R.id.messasge_date);
            senderImage = (ImageView) itemView.findViewById(R.id.sender_image);
            outerView = (RelativeLayout) itemView.findViewById(R.id.outer_field);
            lineOne = itemView.findViewById(R.id.line_one);
            lineTwo = itemView.findViewById(R.id.line_two);

            cv.setOnClickListener(view -> clicks.mainClick(getItem(getLayoutPosition())));
            cv.setOnLongClickListener(v -> {
                clicks.onLingClick(getItem(getLayoutPosition()));
                return false;
            });

             senderImage.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     Profile openFriendProfile = new Profile();

                     Bundle bundle = new Bundle();
                     bundle.putString("UserID", getItem(getLayoutPosition()).getOwner());
                     //bundle.putString("Name", getItem(getLayoutPosition()).get);
                     openFriendProfile.setArguments(bundle);
                     ((NavigationFragment)getFragmentHolder()).openNewFragment(openFriendProfile);
                 }
             });

        }
    }

    private interface Clicks{
        void mainClick(PoemaMessage poem);
        void onLingClick(PoemaMessage poema);
    }
}

