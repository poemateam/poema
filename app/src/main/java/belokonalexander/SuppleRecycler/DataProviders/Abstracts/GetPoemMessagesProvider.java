package belokonalexander.SuppleRecycler.DataProviders.Abstracts;

import java.util.ArrayList;
import java.util.List;

import StaticHelper.LogSystem;
import belokonalexander.Api.CurrentApi;
import belokonalexander.Api.InputQueryDataHolders.MassiveQueryInputData;
import belokonalexander.Api.LocalQueryExecutorExplicit;
import belokonalexander.Api.Models.Poema;
import belokonalexander.Api.Models.PoemaMessage;
import belokonalexander.Api.Models.SugarAdditional.SugarPoemaDB;
import belokonalexander.Api.Models.User;
import belokonalexander.Api.QueryExecutor;
import belokonalexander.GlobalShell;

/**
 * Created by alexander on 10.10.2016.
 */
public class GetPoemMessagesProvider extends SolidProvider<PoemaMessage> {

    Poema poema;

    public GetPoemMessagesProvider(Poema poema) {
        this.poema = poema;
    }

    @Override
    public List<PoemaMessage> getData(MassiveQueryInputData state) {

        QueryExecutor task = new QueryExecutor(GlobalShell.getParentHash(getClass()));

        Object data = task.execute(resultWaiter -> CurrentApi.getInstanse().getApplicationApi().getPoemsMessage(poema, state, resultWaiter));;
        if(data instanceof List)
            return (List<PoemaMessage>) data;

        return null;
    }


    @Override
    public List getCacheData(int pageSize) {

        String[] show = {"getMessages?" + poema.getObjectId(),"createMessages?"+ poema.getObjectId(), "updateMessages?"+ poema.getObjectId()};


        LocalQueryExecutorExplicit getPoemsMessagesLocal = new LocalQueryExecutorExplicit(GlobalShell.getParentHash(this.getClass()),
                resultWaiter -> resultWaiter.setResult(SugarPoemaDB.getInstance().getTopPoemsMessages(pageSize, show)), true);

        List result = null;
        try {
            result = (List) getPoemsMessagesLocal.execute();
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList();
        }

        return result==null ? new ArrayList<>() : result;

    }
}
