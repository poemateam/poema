package ru.belokonalexander.poema.Fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.MenuItemHoverListener;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.liucanwen.app.headerfooterrecyclerview.HeaderAndFooterRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;


import StaticHelper.LogSystem;
import belokonalexander.Api.CurrentApi;
import belokonalexander.Api.Models.User;
import belokonalexander.GlobalShell;
import belokonalexander.SuppleRecycler.ActionRecycler;
import belokonalexander.SuppleRecycler.AutoRefreshActionRecycler;
import belokonalexander.SuppleRecycler.DataProviders.Abstracts.GetFriendsProvider;
import butterknife.BindView;
import butterknife.ButterKnife;
import ru.belokonalexander.poema.FragmentManagmentActivity;
import ru.belokonalexander.poema.Fragments.ListAdapters.CommonAdapter;
import ru.belokonalexander.poema.Fragments.ListAdapters.FriendsAdapter;
import ru.belokonalexander.poema.Fragments.ListAdapters.FriendsSelectorAdapter;
import ru.belokonalexander.poema.PageManagers.Types.ListData;
import ru.belokonalexander.poema.R;

import static StaticHelper.Settings.SHOW_ITEMS;

/**
 * Created by alexander on 28.11.2016.
 */
public class FriendsSelector extends NavigationFragment implements BundleKeeper, ListData{

    @BindView(R.id.recycler_my_freinds)
    AutoRefreshActionRecycler<User> recyclerMyFriends;



    @BindView(R.id.app_toolbar)
    Toolbar toolbar;

    String titleName;
    EditText titleEditText;

    User owner = null;
    AlertDialog.Builder titleDialog;

    public String getTitleName() {
        return titleName;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_my_friends,container,false);
        ButterKnife.bind(this, view);

        List<User> userList = new ArrayList<>();



        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        recyclerMyFriends.setVisibility(View.INVISIBLE);
        recyclerMyFriends.setLayoutManager(llm);
        HeaderAndFooterRecyclerViewAdapter adapterWithHeader = new HeaderAndFooterRecyclerViewAdapter(new FriendsSelectorAdapter(userList,this));
        recyclerMyFriends.setAdapter(adapterWithHeader);


        poemaTitleMenuItem = getToolbar().getMenu().add(0,10,10,titleName);
        poemaTitleMenuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        poemaTitleMenuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                                                          @Override
                                                          public boolean onMenuItemClick(MenuItem item) {
                                                              openTitleDialog();
                                                              return false;
                                                          }
                                                      });

        recyclerMyFriends.init(toolbar);
        recyclerMyFriends.setProvider(new GetFriendsProvider(owner ==null ? CurrentApi.getInstanse().getCurrentUser() : owner));
        recyclerMyFriends.setAfterCacheData(() -> getActivity().runOnUiThread(() -> {
            recyclerMyFriends.setVisibility(View.VISIBLE);
            ButterKnife.apply(recyclerMyFriends, SHOW_ITEMS);
        }));
        recyclerMyFriends.getData(ActionRecycler.UpdateMode.INITIAL);


        fillToolbar(getString(R.string.friends));




        openTitleDialog();




        return view;
    }

    MenuItem poemaTitleMenuItem;

    private void openTitleDialog() {

        titleEditText = new EditText(getContext());

        titleEditText.setText(titleName);


        if (titleName != null)
            titleEditText.setSelection(titleName.length());

        titleDialog = new AlertDialog.Builder(getContext());
        titleDialog.setTitle(R.string.title_poema_dialog);
        titleDialog.setView(titleEditText);
        titleDialog.setPositiveButton(R.string.title_poema_dialog_ok, getDialogClickListener());
        titleDialog.setOnCancelListener(getDialogCancelClickListener());
        titleDialog.create();

        Handler h = new Handler(getContext().getMainLooper());
        Runnable runnable = () -> titleDialog.show();
        h.postDelayed(runnable, 200);
    }


    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }


    DialogInterface.OnClickListener getDialogClickListener() {
        return (dialog, which) -> {
            titleName = titleEditText.getText().toString();

            poemaTitleMenuItem.setTitle(titleName);

        };
    }

    public DialogInterface.OnCancelListener getDialogCancelClickListener() {
        return dialog -> {
            if(poemaTitleMenuItem==null ||  poemaTitleMenuItem.getTitle()==null || poemaTitleMenuItem.getTitle().length()==0){
                getOnNavigationClick().onClick(null);
            }
        };
    }

    @Override
    public Bundle getBundle() {

        Bundle bundle = new Bundle();
        bundle.putString("TITLE",titleName);

        return bundle;
    }

    @Override
    public ActionRecycler getActionRecycler() {
        return recyclerMyFriends;
    }
}
