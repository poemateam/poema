package belokonalexander.SuppleRecycler.DataProviders.Abstracts;

import belokonalexander.Api.InputQueryDataHolders.SearchInputData;

/**
 * Created by Alexander on 22.02.2017.
 */

public interface ISearchProvider {
    void updateSearchInputData(SearchInputData searchInputData);
    boolean onEmptyInputData();
}
