package belokonalexander.Api;

/**
 * Created by alexander on 16.11.2016.
 */
public class LocalQueryExecutor  {


    ResultWaiter resultWaiter;

    public LocalQueryExecutor() {
        this.resultWaiter = new ResultWaiter();
    }

    public Object execute(QueryExecutor.Actions actions) {

        actions.mainTask(resultWaiter);

        return resultWaiter.getResult();
    }

    public LocalQueryExecutor(String hash) {
        //todo
    }
}
