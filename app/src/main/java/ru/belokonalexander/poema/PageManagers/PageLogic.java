package ru.belokonalexander.poema.PageManagers;

/**
 * Created by alexander on 03.11.2016.
 */

import java.util.List;

public interface PageLogic {
     List<Object> getData();
     void fillData();
}
