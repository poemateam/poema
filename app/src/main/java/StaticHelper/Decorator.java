package StaticHelper;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import ru.belokonalexander.poema.R;

/**
 * Created by alexander on 05.09.2016.
 */
public class Decorator {



    public static int evalDuration = 2000;

    public static void runResultUpdateAnimation(Context context, View v, boolean result){
        int colorFrom;
        if(result) colorFrom = ContextCompat.getColor(context, R.color.success_color);
            else   colorFrom = ContextCompat.getColor(context, R.color.errorColor);
        int colorTo = 0x00FFFFFF;
        ObjectAnimator animator;
        if(v instanceof TextView) {
            colorTo = ((TextView) v).getCurrentTextColor();
            animator = ObjectAnimator.ofObject(v, "textColor", new ArgbEvaluator(), colorFrom, colorTo);
        }
            else {
             animator = ObjectAnimator.ofObject(v,"backgroundColor", new ArgbEvaluator(), colorFrom, colorTo);
        }
        animator.setDuration(evalDuration).start();
    }





}
