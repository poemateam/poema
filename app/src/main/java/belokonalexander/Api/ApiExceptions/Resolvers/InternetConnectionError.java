package belokonalexander.Api.ApiExceptions.Resolvers;

import android.content.Context;
import android.widget.Toast;

import java.util.Date;

import StaticHelper.LogSystem;
import StaticHelper.PoemaErrors;
import belokonalexander.Api.ApiExceptions.PoemaApiError;
import belokonalexander.Api.ApiExceptions.PoemaErrorResolver;
import ru.belokonalexander.poema.PoemaApplication;
import ru.belokonalexander.poema.R;

/**
 * Created by admin on 26.08.2016.
 */
public class InternetConnectionError extends PoemaErrorResolver{

    public InternetConnectionError(Context context) {
        super(context);
    }

    private Long lastReaction = null;

    private final int REACTION_TIMEOUT = 2000;

    @Override
    protected Boolean resolve(PoemaApiError error) {

        int errorCode = PoemaErrorResolver.convertInAppErrorCode(error);

          if(errorCode== PoemaErrors.INTERNET_CONNECTION_ERROR)
        {
            resolveAction();

            return true;
        }

        return false;
    }

    @Override
    protected void resolveAction() {

        if(lastReaction==null || (new Date().getTime()-lastReaction)>REACTION_TIMEOUT){
            LogSystem.logThis(" Разрешаем ошибку с интернетом");
            LogSystem.makeToast(((PoemaApplication)context.getApplicationContext()).getCurrentActivity(), context.getString(R.string.connection_error), LogSystem.ToastType.Error,LogSystem.TOAST_POSITION_BOTTOM);
            lastReaction = new Date().getTime();
        }

    }

}
