package belokonalexander.SuppleRecycler.DataProviders.Abstracts;

import java.util.ArrayList;
import java.util.List;

import belokonalexander.Api.CurrentApi;
import belokonalexander.Api.InputQueryDataHolders.MassiveQueryInputData;
import belokonalexander.Api.InputQueryDataHolders.SearchInputData;
import belokonalexander.Api.Models.Poema;
import belokonalexander.Api.Models.SearchedEntity;
import belokonalexander.Api.QueryExecutor;
import belokonalexander.GlobalShell;

/**
 * Created by alexander on 06.10.2016.
 */
public class SearchProvider extends SolidProvider<SearchedEntity> implements ISearchProvider{

    public SearchProvider(SearchInputData searchInputData) {
        this.searchInputData = searchInputData;
        this.initialSearchData = searchInputData;
    }

    public SearchProvider(){

    }

    public SearchInputData initialSearchData;

    public SearchInputData searchInputData;

    @Override
    public List<SearchedEntity> getData(MassiveQueryInputData state) {



        searchInputData.setPageSize(state.getPageSize());
        searchInputData.setOffset(state.getOffset());
        QueryExecutor task = new QueryExecutor(GlobalShell.getParentHash(getClass()));

        Object data = task.execute(resultWaiter -> CurrentApi.getInstanse().getApplicationApi().searchEntity(searchInputData, resultWaiter));
        if(data instanceof List)
            return (List<SearchedEntity>) data;

        return null;

    }

    @Override
    public List getCacheData(int pageSize) {
        return new ArrayList();
    }

    @Override
    public void updateSearchInputData(SearchInputData searchInputData) {
        this.searchInputData = searchInputData;
    }

    @Override
    public boolean onEmptyInputData() {

        searchInputData = initialSearchData;

        return true;
    }
}
