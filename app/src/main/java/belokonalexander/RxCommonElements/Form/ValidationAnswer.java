package belokonalexander.RxCommonElements.Form;

import java.util.List;

/**
 * Created by admin on 02.08.2016.
 */
public class ValidationAnswer {

    private boolean success;
    private List<String> errors;

    public ValidationAnswer(boolean success, List<String> errors) {
        this.success = success;
        this.errors = errors;
    }

    public boolean isSuccess() {
        return success;
    }

    public List<String> getErrors() {
        return errors;
    }
}
