package StaticHelper;

/**
 * Created by alexander on 29.08.2016.
 */
public class PoemaErrors {

    public static final int API_TOKEN_EXPIRED_ERROR = 1001;
    public static final int INTERNET_CONNECTION_ERROR = 1002;
    public static final int DUBLICATE_ERROR = 1003;

}
