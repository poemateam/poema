package belokonalexander.Api;

import android.app.Activity;
import android.content.Context;

import com.backendless.Subscription;
import com.orm.SugarRecord;

import java.io.File;
import java.util.HashMap;
import java.util.List;

import belokonalexander.Api.InputQueryDataHolders.MassiveQueryInputData;

import belokonalexander.Api.Models.Poema;
import belokonalexander.Api.Models.PoemaMessage;
import belokonalexander.Api.Models.SugarAdditional.SugarPoemaDB;
import belokonalexander.Api.Models.User;
import belokonalexander.Api.InputQueryDataHolders.SearchInputData;

/**
 * Created by alexander on 26.08.2016.
 */
public class ApiStarter implements ApiItem {

    protected String requestDescription;

    @Override
    public void login(HashMap<String, String> data, ResultWaiter waiter) {
        waiter.start(data, "login");
        requestDescription = "login?"+data;
        waiter.setMetaData(requestDescription);
    }

    @Override
    public void registration(HashMap<String, String> data, ResultWaiter waiter) {
        waiter.start(data, "registration");
        requestDescription = "registration?"+data;
        waiter.setMetaData(requestDescription);
    }

    @Override
    public String getSavedToken() {
        return null;
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public void logout(Context redirectContext) {



    }

    @Override
    public void searchEntity(SearchInputData data, ResultWaiter waiter) {
        waiter.start(data, "searchEntity");
        requestDescription = "searchEntity?"+data;
        waiter.setMetaData(requestDescription, data);
    }

    @Override
    public void updateCurrentUser(ResultWaiter waiter) {
        waiter.start(null, "updateCurrentUser");
        requestDescription = "updateCurrentUser";

    }

    @Override
    public void updateCurrentUserProperty(String property, ResultWaiter waiter) {
        waiter.start(property, "updateCurrentUserProperty");
    }

    @Override
    public boolean isSessionValid() {
        return false;
    }

    @Override
    public void uploadUserImage(File file, ResultWaiter waiter) {
        waiter.start(file, "uploadUserImage");

    }

    @Override
    public User getCurrentUser() {
        return null;
    }

    @Override
    public void getUser(String userId, ResultWaiter waiter) {
        waiter.start(userId, "getUser");
        requestDescription = "getUser?"+userId;
        waiter.setMetaData(requestDescription);
    }

    @Override
    public void offerFriendship(User user, ResultWaiter waiter) {
        waiter.start(user.getObjectId(), "offerFriendship");
        requestDescription = "offerFriendship?"+user;
        waiter.setMetaData(requestDescription);
    }

    @Override
    public void cancelFriendship(User user, ResultWaiter waiter) {
        waiter.start(user.getObjectId(), "deleteFriendship");
        requestDescription = "deleteFriendship?"+user;
        waiter.setMetaData(requestDescription);
    }

    @Override
    public void confirmFriendship(User user, ResultWaiter waiter) {
        waiter.start(user.getObjectId(), "confirmFriendship");
        requestDescription = "confirmFriendship?"+user;
        waiter.setMetaData(requestDescription);
    }

    @Override
    public void getFriends(String userId, MassiveQueryInputData data, int status, ResultWaiter waiter, boolean includeInviteForMe) {
        waiter.start(data, "getFriends");
        requestDescription = "getFriends?" + userId;
        waiter.setMetaData(requestDescription, data);
    }

    @Override
    public int convertInAppError(int errorCodeApi) {
        return 0;
    }

    @Override
    public int convertInAppError(String message) {
        return 0;
    }

    @Override
    public void getShortFriendsList(String userId, ResultWaiter waiter) {
        waiter.start(userId,"getShortFriendsList");
        requestDescription = "getShortFriendsList?" + userId;
        waiter.setMetaData(requestDescription);
    }

    @Override
    public void createPoema(Poema poema, ResultWaiter waiter) {
        waiter.start(poema,"createPoema");
        requestDescription = "createPoema?" + CurrentApi.getInstanse().getCurrentUser().getObjectId();
        waiter.setMetaData(requestDescription);
    }

    @Override
    public void getPoems(String userId, MassiveQueryInputData data, ResultWaiter waiter) {
        waiter.start(userId, "getPoema");
        requestDescription = "getPoema?" + userId;
        waiter.setMetaData(requestDescription, data);
        waiter.addDeletedCommand("create");
        waiter.addDeletedCommand("update");
    }

    @Override
    public void getPoemsMessage(Poema poema, MassiveQueryInputData data, ResultWaiter waiter) {
        waiter.start(poema.getObjectId(),"getMessages");
        requestDescription = "getMessages?"+poema.getObjectId();
        waiter.setMetaData(requestDescription,data);
        waiter.addDeletedCommand("create");
        //waiter.addDeletedCommand("update");

    }

    @Override
    public void deletePoems(Poema poema, ResultWaiter waiter) {
        waiter.start(poema, "deletePoema");
        requestDescription = "deletePoema?" + poema.getObjectId();
        waiter.setMetaData(requestDescription);


    }

    @Override
    public void startMessagingService() {

    }

    @Override
    public void stopMessageingService() {

    }

    @Override
    public void sendMessage(Poema poema, String poemaMessage, ResultWaiter waiter) {
        waiter.start(poema.getObjectId(),"createMessages");
        requestDescription = "createMessages?" + poema.getObjectId();
        waiter.setMetaData(requestDescription);
    }

    @Override
    public void readMessage(Poema poema, ResultWaiter waiter) {
        waiter.start(poema.getObjectId(),"readMessages");
        requestDescription = "readMessages?" + poema.getObjectId();
        waiter.setMetaData(requestDescription);
    }

    @Override
    public void editMessage(PoemaMessage poemaMessage, String newText, ResultWaiter waiter) {
        waiter.start(poemaMessage.getObjectId(),"editMessage");
        requestDescription= "editMessages?" + poemaMessage.getObjectId();
        waiter.setMetaData(requestDescription);
    }

    @Override
    public void getPoema(String poemaId, ResultWaiter waiter) {
        waiter.start(poemaId,"editMessage");
        requestDescription= "updatePoema?" + CurrentApi.getInstanse().getCurrentUser().getObjectId();
        waiter.setMetaData(requestDescription);
    }

    @Override
    public void restorePassword(String userId, ResultWaiter waiter) {
        waiter.start(userId,"restorePassword");
        requestDescription= "restorePassword?" + userId;
        waiter.setMetaData(requestDescription);
        waiter.setEmptyCommand(true);

    }

    @Override
    public void registerDevice(String userId, ResultWaiter waiter) {
        waiter.start(userId,"registerDevice");
        requestDescription= "registerDevice?"+userId;
        waiter.setMetaData(requestDescription);
        waiter.setEmptyCommand(true);
    }

    @Override
    public void unregisterDevice(ResultWaiter waiter) {
        waiter.start("empty","unregisterDevice");
        requestDescription= "unregisterDevice";
        waiter.setMetaData(requestDescription);
        waiter.setEmptyCommand(true);
    }

    @Override
    public void getAllPoema(String poemaId, ResultWaiter waiter) {
        waiter.start(poemaId,"getAllPoema");
        requestDescription= "getAllPoema?"+poemaId;
        waiter.setMetaData(requestDescription);
        waiter.setEmptyCommand(true);
    }

    @Override
    public void sendFeedback(String userId, String category, String message, ResultWaiter waiter) {
        waiter.start(userId,"sendFeedback");
        requestDescription= "sendFeedback?"+userId;
        waiter.setMetaData(requestDescription);
        waiter.setEmptyCommand(true);
    }
}
