package ru.belokonalexander.poema.PageManagers.Types;

import ru.belokonalexander.poema.PageManagers.ApiFragment;

/**
 * Created by Alexander on 16.02.2017.
 */

public interface CustomData {
    ApiFragment getFragment();
}
