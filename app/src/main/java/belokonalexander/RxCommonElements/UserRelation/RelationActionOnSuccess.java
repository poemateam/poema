package belokonalexander.RxCommonElements.UserRelation;

/**
 * Created by alexander on 20.10.2016.
 */
public interface RelationActionOnSuccess {
    void onSuccess();
}
