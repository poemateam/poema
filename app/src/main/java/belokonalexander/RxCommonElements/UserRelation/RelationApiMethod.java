package belokonalexander.RxCommonElements.UserRelation;

import belokonalexander.Api.Models.User;
import belokonalexander.Api.ResultWaiter;

/**
 * Created by alexander on 23.09.2016.
 */
public interface RelationApiMethod {
    void execute(User user, ResultWaiter resultWaiter );
}
