package belokonalexander.Api.Models.SugarAdditional;

import java.util.Date;

import belokonalexander.Api.Models.QueryLog;

/**
 * Created by alexander on 25.11.2016.
 */
public interface SugarPersistence {
    void setUpdateDate(Date date);
    Long getUpdateDate();
    Long getId();
    void generateId();
    void saveRelateObjects();

}
