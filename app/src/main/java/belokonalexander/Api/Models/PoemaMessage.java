package belokonalexander.Api.Models;

import com.backendless.messaging.Message;

import java.util.Date;

import belokonalexander.Api.CurrentApi;
import belokonalexander.Api.Models.Annotations.Field;
import belokonalexander.Api.Models.Annotations.Table;
import belokonalexander.Api.Models.SugarAdditional.SugarPersistence;

/**
 * Created by Alexander on 06.02.2017.
 */

@com.orm.dsl.Table(name = "Messages")
@Table(name="Messages")
public class PoemaMessage  extends  ApiEntity implements SugarPersistence {

    Long id;

    @Field
    String messageId ="";

    @Field
    String message = "";

    @Field
    String poemaId = "";

    @Field
    Date created = null;

    @Field
    String owner = "";

    @Field
    Boolean actual = true;



    public PoemaMessage() {

    }

    public Boolean getActual() {
        return actual;
    }

    public PoemaMessage(String messageId, String message, String poema, String owner) {
        this.messageId = messageId;
        this.message = message;
        this.poemaId = poema;
        this.owner = owner;
        this.created = new Date();

        if(isMyMessage())
            actual = false;
    }

    public boolean isMyMessage(){
        return CurrentApi.getInstanse().getCurrentUser().getObjectId().equals(owner);
    }

    public String getMessage() {
        return message;
    }

    public String getPoema() {
        return poemaId;
    }

    public Date getCreated() {
        return created;
    }

    public String getOwner() {
        return owner;
    }

    @Override
    public String getObjectId() {
        return messageId;
    }

    @Override
    public void afterInit() {

    }

    @Override
    public void setUpdateDate(Date date) {
        lastUpdate = date;
    }

    @Override
    public Long getUpdateDate() {
        return lastUpdate.getTime();
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void generateId() {
        id = CurrentApi.getInstanse().getLongHashOfItem(messageId);
    }

    @Override
    public void saveRelateObjects() {
        //

    }

    public PoemaMessage(Message message){
            this.messageId = message.getMessageId();
            this.message = (String) message.getData();
            this.poemaId = message.getHeaders().get("poemaId");
            this.created = new Date(message.getTimestamp());
            this.owner = message.getPublisherId();
            this.actual = true;

    }

    public PoemaMessage(String messageId, String message, String poema, String owner, Long created) {
        this.messageId = messageId;
        this.message = message;
        this.poemaId = poema;
        this.owner = owner;
        this.created = new Date(created);
        this.actual = true;
    }

    @Override
    public String toString() {
        return "PoemaMessage{" +
                "id=" + id +
                ", messageId='" + messageId + '\'' +
                ", message='" + message + '\'' +
                ", poema='" + poemaId + '\'' +
                ", created=" + created +
                ", owner=" + owner +
                ", actual=" + actual +
                '}';
    }

    @Override
    public boolean equals(Object obj) {

        return obj instanceof PoemaMessage && messageId.equals(((PoemaMessage) obj).getObjectId()) && message.equals(((PoemaMessage) obj).getMessage());
    }

    public void setActual(Boolean actual) {
        this.actual = actual;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
