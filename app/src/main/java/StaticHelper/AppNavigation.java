package StaticHelper;

import android.content.Context;

import belokonalexander.GlobalShell;
import ru.belokonalexander.poema.Login;
import ru.belokonalexander.poema.SplashScreen;

/**
 * Created by admin on 10.08.2016.
 */
public class AppNavigation {

    public static void restartAppForLogin(Context context){

        GlobalShell.openActivityWithCloseThis(context, Login.class);
    }

    public static String BACKPRESS_ENABLE = "BACKPRESS";

}
