package belokonalexander.Api.Models;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.orm.dsl.Ignore;

import java.util.Date;

import belokonalexander.Api.CurrentApi;
import belokonalexander.Api.Models.Annotations.SearchField;
import belokonalexander.Api.Models.Poema;
import belokonalexander.Api.Models.SugarAdditional.SugarPersistence;
import belokonalexander.Api.Models.SugarAdditional.SugarPoemaDB;
import belokonalexander.Api.Models.User;
import ru.belokonalexander.poema.Fragments.ListAdapters.SearchAdapter;

/**
 * Created by Alexander on 21.02.2017.
 */
@com.orm.dsl.Table(name = "CompletePoema")
public class CompletePoema implements SugarPersistence, SearchedEntity {


    Long id;

    public CompletePoema() {
    }

    public CompletePoema(Poema poema, String text){
        this.text = text;
        title = poema.getTitle();

        StringBuilder authors = new StringBuilder();
        for(User user : poema.getMembers()){
            authors.append(user.getName()).append("/");
        }
        authors.deleteCharAt(authors.length()-1);

        this.authors = authors.toString();
        this.poemaId = poema.getObjectId();
        this.poemaLastUpdate = poema.getLastUpdate();
        this.ownerId = CurrentApi.getInstanse().getCurrentUser().getObjectId();
    }

    public CompletePoema(String text, String aut, String title){
        this.text = text;
        this.authors = aut;
        this.title = title;
    }

    String text;

    @SearchField(lazySearch = true, alias = "Title")
    String title;

    String authors;

    String poemaId;

    String ownerId;

    Boolean isFavorite = false;

    Date poemaLastUpdate;

    Date lastUpdate;

    @Override
    public Long getUpdateDate() {
        return lastUpdate.getTime();
    }

    @Override
    public void setUpdateDate(Date date) {
        lastUpdate = date;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void generateId() {
        id = CurrentApi.getInstanse().getLongHashOfItem(poemaId+ownerId);
    }

    @Override
    public void saveRelateObjects() {

    }

    public Boolean getFavorite() {
        return isFavorite;
    }

    public void setFavorite(Boolean favorite) {
        isFavorite = favorite;
    }

    public Boolean changeFavorite() {
        isFavorite = !isFavorite;
        SugarPoemaDB.getInstance().updateFavorite(this);
        return isFavorite;
    }

    @Override
    public String toString() {
        return "CompletePoema{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", title='" + title + '\'' +
                ", authors='" + authors + '\'' +
                ", poemaId='" + poemaId + '\'' +
                ", poemaLastUpdate=" + poemaLastUpdate +
                ", lastUpdate=" + lastUpdate +
                '}';
    }

    public String getText() {
        return text;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthors() {
        return authors;
    }

    public String getPoemaId() {
        return poemaId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public Date getPoemaLastUpdate() {
        return poemaLastUpdate;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }


    public void selfDelete() {
        SugarPoemaDB.getInstance().deleteComplete(this);
    }

    public CharSequence getFullText() {
        StringBuilder sb = new StringBuilder();
        return sb.append(title). append(" (").append(authors).append(") \n\n").append(text);
    }
}
