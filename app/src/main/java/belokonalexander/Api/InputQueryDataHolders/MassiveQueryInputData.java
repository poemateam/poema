package belokonalexander.Api.InputQueryDataHolders;

/**
 * Created by alexander on 23.09.2016.
 */
public class MassiveQueryInputData {




     int pageSize;
     int offset;

    public void nextStep(){
        offset+=pageSize;
    }


    public int getPageSize() {


        return pageSize;
    }

    public Integer getOffset() {
        return offset;
    }


    public MassiveQueryInputData(){
        pageSize = 100;
        offset = 0;
    }

    public MassiveQueryInputData setPageSize(int pageSize) {
        this.pageSize = pageSize;
        return this;
    }

    public MassiveQueryInputData setOffset(int offset) {
        this.offset = offset;
        return this;
    }

    public MassiveQueryInputData(int pageSize, int offset) {
        this.pageSize = pageSize;
        this.offset = offset;
    }

    public MassiveQueryInputData(int pageSize) {
        this.pageSize = pageSize;
        this.offset=0;
    }

    public void reset(){
        offset=0;

    }

    @Override
    public String toString() {
        return "MassiveQueryInputData{" +
                "pageSize=" + pageSize +
                ", offset=" + offset +
                '}';
    }

    public void addOffset(int val){
        offset+=val;
    }

}
