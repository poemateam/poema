package belokonalexander.Api.Models;

import com.orm.dsl.Table;

import java.util.Date;

import belokonalexander.Api.CurrentApi;
import belokonalexander.Api.Models.SugarAdditional.SugarPersistence;


/**
 * Created by alexander on 25.11.2016.
 */
@Table (name = "ShortFriendListRow")
public class ShortFriendListRow implements SugarPersistence {

    private Date lastUpdate;

    private Long id;

    private String userOne;
    private String userTwo;

    public ShortFriendListRow(String userOne, String userTwo) {
        this.userOne = userOne;
        this.userTwo = userTwo;
        this.id = (CurrentApi.getInstanse().getLongHashOfItem(this.userOne+this.userTwo));

    }

    @Override
    public void setUpdateDate(Date date) {
        lastUpdate = date;
    }

    @Override
    public Long getUpdateDate() {
        if(lastUpdate == null)
            return (new Date()).getTime();

        return lastUpdate.getTime();
    }

    @Override
    public void generateId() {
        this.id = (CurrentApi.getInstanse().getLongHashOfItem(this.userOne+this.userTwo));
    }

    @Override
    public void saveRelateObjects() {

    }

    @Override
    public String toString() {
        return "USER ONE: " + userOne + " USER TWO: " + userTwo + " id: " + id;
    }

    public Long getId() {
        return id;
    }



    public ShortFriendListRow() {
    }

    public String getUserOne() {
        return userOne;
    }

    public String getUserTwo() {
        return userTwo;
    }
}
