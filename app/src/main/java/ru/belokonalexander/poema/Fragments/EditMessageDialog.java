package ru.belokonalexander.poema.Fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import StaticHelper.LogSystem;
import belokonalexander.Api.CurrentApi;
import belokonalexander.Api.Models.Poema;
import belokonalexander.Api.Models.PoemaMessage;
import belokonalexander.Api.QueryExecutorExplicit;
import belokonalexander.GlobalShell;
import ru.belokonalexander.poema.Events.PoemaEvent;
import ru.belokonalexander.poema.Fragments.ListAdapters.CommonAdapter;
import ru.belokonalexander.poema.PageManagers.BackendRunner;
import ru.belokonalexander.poema.R;

/**
 * Created by Alexander on 12.02.2017.
 */

public class EditMessageDialog extends DialogFragment {


    PoemaMessage poemaMessage;
    EditText message;
    CommonAdapter<PoemaMessage> adapter;
    Poema currentPoema;
    int position;

    public void show(FragmentManager manager, Poema currentPoema, PoemaMessage poemaMessage,CommonAdapter<PoemaMessage>  adapter){
        super.show(manager,"tag");
        this.poemaMessage = poemaMessage;
        this.adapter = adapter;
        this.currentPoema = currentPoema;

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.fragment_edit_message, null);

        message = (EditText) view.findViewById(R.id.message_text);

        message.setText(poemaMessage.getMessage());
        if (message != null)
            message.setSelection(poemaMessage.getMessage().length());

        builder.setView(view);

        builder.setView(view)
                .setMessage(R.string.poema_edit_message)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // FIRE ZE MISSILES!
                        if(!message.getText().toString().equals(poemaMessage.getMessage()) && message.getText().toString().length()>0){


                            QueryExecutorExplicit editMessage = new QueryExecutorExplicit(GlobalShell.getParentHash(this.getClass()), resultWaiter -> CurrentApi.getInstanse().getApplicationApi().editMessage(poemaMessage,message.getText().toString(), resultWaiter));

                            String prevValue = poemaMessage.getMessage();

                            position = adapter.getPosition(poemaMessage);

                            poemaMessage.setMessage(message.getText().toString());

                            adapter.notifyItemChanged(position);


                            BackendRunner backendRunner = new BackendRunner(new BackendRunner.SuccessAnswer() {
                                @Override
                                public void onSuccessCompleted(List<Object> answers) {

                                    GlobalShell.tryCloseKeyboard(getActivity());

                                    PoemaMessage message = (PoemaMessage) answers.get(0);

                                    if(currentPoema.getLastMessage().getObjectId().equals(message.getObjectId())){
                                        LogSystem.logThis("POEMA EVENT");
                                        currentPoema.setLastMessage(message);
                                        EventBus.getDefault().post(new PoemaEvent(currentPoema));
                                    }

                                }
                            }, new BackendRunner.FailureAnswer() {
                                @Override
                                public void onFailureCompleted(String messsage) {
                                    LogSystem.logThis("FAIL...");
                                    poemaMessage.setMessage(prevValue);
                                    adapter.notifyItemChanged(position);

                                }
                            }, editMessage);

                            backendRunner.run();
                        }
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }
}
