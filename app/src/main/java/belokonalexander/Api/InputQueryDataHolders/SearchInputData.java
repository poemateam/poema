package belokonalexander.Api.InputQueryDataHolders;

import belokonalexander.Api.InputQueryDataHolders.MassiveQueryInputData;
import belokonalexander.Api.Models.Annotations.DefaultAdditionSearchData;
import belokonalexander.Api.Models.SearchedEntity;

/**
 * Created by alexander on 15.08.2016.
 */
public class SearchInputData extends MassiveQueryInputData{

    private String key;
    private String value;
    private Boolean isFullContains;
    private Boolean isEmpty;
    private Class<? extends SearchedEntity> metadataEntity;

    public SearchInputData(String key, String value, Boolean isFullContains, Class <? extends SearchedEntity> metadataEntity) {
        this.key = key;
        this.value = value;
        this.metadataEntity = metadataEntity;
        this.isFullContains = isFullContains;
        this.isEmpty = false;
    }


    public String getPlace()
    {
        String dbTable =  DefaultAdditionSearchData.getSearchTable(metadataEntity);
        return (dbTable==null) ? null : dbTable.toUpperCase() ;
    }



    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return getKey() + " - " + getValue() + " - " + getPlace();
    }

    public Boolean getFullContains() {
        return isFullContains;
    }

    public SearchInputData() {
        isEmpty = true;
    }

    public Boolean getEmpty() {
        return isEmpty;
    }
}
