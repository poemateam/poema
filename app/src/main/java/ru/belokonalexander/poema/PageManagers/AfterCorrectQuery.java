package ru.belokonalexander.poema.PageManagers;

import java.util.List;

/**
 * Created by alexander on 22.11.2016.
 */
public interface AfterCorrectQuery {
    void showPostObjects(List<Object> answer);
}
