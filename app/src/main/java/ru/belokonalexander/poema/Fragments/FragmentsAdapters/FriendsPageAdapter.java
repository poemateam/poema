package ru.belokonalexander.poema.Fragments.FragmentsAdapters;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;


import ru.belokonalexander.poema.Fragments.FriendList;
import ru.belokonalexander.poema.Fragments.SearchFriendsTab;
import ru.belokonalexander.poema.R;

/**
 * Created by alexander on 22.08.2016.
 */
public class FriendsPageAdapter extends android.support.v4.app.FragmentPagerAdapter {


    Context context;

    public FriendsPageAdapter(Context context, FragmentManager fm) {
        super(fm);
        this.context = context;
    }

    FriendList fl;
    SearchFriendsTab sft;

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position)
        {
            case 0:

                FriendList myFriendsFraagment = new FriendList();
                fragment = myFriendsFraagment;
                fl = myFriendsFraagment;

                break;
            case 1:

                SearchFriendsTab searchFriendsFragment = new SearchFriendsTab();
                fragment = searchFriendsFragment;
                sft = searchFriendsFragment;

                break;
        }

        return fragment;
    }

    public void onSelected(TabLayout.Tab tab){
        if(tab.getText().equals(context.getString(R.string.search_friends))){
            sft.onSelected();
        }

    }

    @Override
    public int getCount() {

        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return context.getResources().getString(R.string.my_friends);
            case 1:
                return context.getResources().getString(R.string.search_friends);

        }
        return null;
    }
}
