package belokonalexander.Api.ApiExceptions.Resolvers;

import android.content.Context;

import java.util.Date;

import StaticHelper.LogSystem;
import StaticHelper.PoemaErrors;
import belokonalexander.Api.ApiExceptions.PoemaApiError;
import belokonalexander.Api.ApiExceptions.PoemaErrorResolver;
import belokonalexander.Api.Models.FriendsRelate;
import belokonalexander.Api.Models.Poema;
import ru.belokonalexander.poema.PoemaApplication;
import ru.belokonalexander.poema.R;

/**
 * Created by admin on 26.08.2016.
 */
public class DublicateError extends PoemaErrorResolver{

    public DublicateError(Context context) {
        super(context);
    }


    Exception e;

    @Override
    protected Boolean resolve(PoemaApiError error) {

        e = error;

        int errorCode = PoemaErrorResolver.convertInAppErrorCode(error);

          if(errorCode== PoemaErrors.DUBLICATE_ERROR)
        {
            resolveAction();

            return true;
        }

        return false;
    }

    @Override
    protected void resolveAction() {

        if(e instanceof PoemaApiError){

            PoemaApiError error = (PoemaApiError) e;

            String msg = "";

            if(error.getItemType().equals(Poema.class)){
                msg = context.getResources().getString(R.string.dublicate_poema);
            } else if (error.getItemType().equals(FriendsRelate.class))
                msg = context.getResources().getString(R.string.dublicate_friendship);

            if(msg.length()>0)
                LogSystem.makeToast(((PoemaApplication)context.getApplicationContext()).getCurrentActivity(),msg, LogSystem.ToastType.Error,LogSystem.TOAST_POSITION_BOTTOM);

        }



    }

}
