package belokonalexander.SuppleRecycler;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

import com.liucanwen.app.headerfooterrecyclerview.RecyclerViewUtils;

import StaticHelper.LogSystem;
import belokonalexander.DevicePrefs;
import ru.belokonalexander.poema.R;

/**
 * Created by alexander on 10.10.2016.
 */
public class AutoRefreshActionRecycler<T> extends ActionRecycler<T> {
    public AutoRefreshActionRecycler(Context context) {
        super(context);
    }

    public AutoRefreshActionRecycler(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AutoRefreshActionRecycler(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }


    View footerViewRefresh;

    boolean isReverse = false;

    @Override
    public void init(Toolbar toolbar) {
        super.init(toolbar);
        footerViewRefresh = LayoutInflater.from(getContext()).inflate(R.layout.item_bottom_refresh, this, false);
        footerViewRefresh.getLayoutParams().height = (int) (DevicePrefs.getNormalListHeight(getContext()));
        recyclerState.setPageSize(pageSizeCalculate());

        if (isReverse) {    //hack for hide bottom (header) when use reverse
            headerView.getLayoutParams().height = 1;
        }
    }

    public void init(Toolbar toolbar, boolean isReverse){
        this.isReverse = isReverse;
        init(toolbar);
    }

    int refreshBorder = 0; // start refresh position

    boolean footerIsShownAsRefresh;
    boolean scrollLock = false;

    @Override
    void onScrollHeightController(RecyclerView recyclerView, int dx, int dy) {
        super.onScrollHeightController(recyclerView, dx, dy);



        if(((!isReverse && (!recyclerView.canScrollVertically(1) || allDy >= (refreshBorder)))||(isReverse&&!recyclerView.canScrollVertically(-1) || -allDy >= (refreshBorder)))&&!allDataWasObtained&&!scrollLock && !getUpdatingInProgress()) {
            scrollLock = true;
            getData(UpdateMode.ADD);
            //TODO call if data smaller than window height
        }
    }


    private void recalculateFooterVisible() {
        //футер показывается, только при возможности прокрутки (т.е если содержимое превосходит общий размер recycler view'a)
        int recyclerHeight = this.getHeight();
        int contains = (int)(adapter.getItemCount()* DevicePrefs.getNormalListHeight(getContext())) + headerView.getHeight();


        /*LogSystem.logThis("CALCULATED HEIGHT: " + (adapter.getItemCount()*DevicePrefs.getNormalListHeight(getContext())) + " ---> " + DevicePrefs.getNormalListHeight(getContext()) + " /// " + adapter.getItemCount());

        LogSystem.logThis("RECYCLER SCROLL HEIGHT: " + (computeVerticalScrollRange() - (DevicePrefs.getNormalListHeight(getContext()))/2) + " / " +
        (computeVerticalScrollOffset() + computeVerticalScrollExtent()));
*/
        //пересчитываем также границу скролла до рефреша - ВЕСЬ СПИСОК - РАЗМЕР RECYCLER - (ДОПОЛНИТЕЛЬНО ДЛЯ РАННЕГО СКРОЛЛА)
        refreshBorder = (int)(adapter.getItemCount()*DevicePrefs.getNormalListHeight(getContext()) - this.getHeight() - (2 * DevicePrefs.getNormalListHeight(getContext())));
        setShowFooter(contains > recyclerHeight && !allDataWasObtained);

    }

    private void setShowFooter(boolean state){
        if(state) {

            RecyclerViewUtils.setFooterView(this, footerViewRefresh);
            footerIsShownAsRefresh = true;

        } else {
            RecyclerViewUtils.removeFooterView(this);
            footerIsShownAsRefresh = false;
        }
    }

    private int pageSizeCalculate() {

        int oneElementHeight = (int) DevicePrefs.getNormalListHeight(getContext());
        int deviceHeight = DevicePrefs.getDisplay().heightPixels;

        //округляем до 10
        int pageSize = (deviceHeight/oneElementHeight+10);
        pageSize -= pageSize %10;
        //пусть будет 30
        return Math.max(30,pageSize);
    }

    @Override
    void afterUpdating() {
        super.afterUpdating();
        recalculateFooterVisible();
        scrollLock = false;
    }
}
