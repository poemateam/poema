package belokonalexander.RxCommonElements.Form;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import java.util.List;

import StaticHelper.LogSystem;
import ru.belokonalexander.poema.R;

/**
 * Created by admin on 31.07.2016.
 */
public class FormValidator {

    private List<? extends ValidationField> formFields;

    private Context context;

    public boolean checkFormFields()
    {

        boolean result = true;

        for(int i = 0 ; i < formFields.size(); i++)
        {
            TextInputLayout textInputLayout = (TextInputLayout) ((View)formFields.get(i)).getParent();

            if(!formFields.get(i).validate().isSuccess())
            {
                LogSystem.logThis("ERROR: " + formFields.get(i).validate().getErrors().get(0));
                result = false;
                textInputLayout.setErrorEnabled(true);
                textInputLayout.setError(formFields.get(i).validate().getErrors().get(0));

            } else {
                textInputLayout.setErrorEnabled(false);
                textInputLayout.setError(null);
            }
        }

        return result;
    }

    public <T extends ValidationField>FormValidator(Context context, List<T> fields){

        this.context = context;
        this.formFields = fields;

    }





}
