package belokonalexander.Api;

import android.app.Activity;
import android.content.Context;

import com.backendless.Subscription;

import java.io.File;
import java.util.HashMap;
import java.util.List;

import belokonalexander.Api.InputQueryDataHolders.MassiveQueryInputData;
import belokonalexander.Api.Models.Poema;
import belokonalexander.Api.Models.PoemaMessage;
import belokonalexander.Api.Models.User;
import belokonalexander.Api.InputQueryDataHolders.SearchInputData;
import ru.belokonalexander.poema.R;

/**
 * Created by admin on 30.07.2016.
 */
public interface ApiItem {

    String EMAIL_KEY = "EMAIL";
    String PASSWORD_KEY = "PASSWORD";
    String NAME_KEY = "NAME";


    public enum FeedbackCategory{
        OTHER, PROPOSAL, ERROR, GRATITUDE;

        public String getKey(Context context) {

            switch (this){
                case ERROR:
                    return context.getResources().getString(R.string.error);
                case OTHER:
                    return context.getResources().getString(R.string.other);
                case PROPOSAL:
                    return context.getResources().getString(R.string.proposal);
                case GRATITUDE:
                    return context.getResources().getString(R.string.gratitude);
            }

            return context.getResources().getString(R.string.other);
        }
    }

    void login(HashMap<String,String> data, ResultWaiter actions);
    void registration(HashMap<String,String> data, ResultWaiter actions);
    String getSavedToken();
    String getDescription();
    void logout(Context redirectContext);

    void searchEntity(SearchInputData dat, ResultWaiter waiter);

    void updateCurrentUser(ResultWaiter waiter);

    void updateCurrentUserProperty(String property, ResultWaiter waiter);

    boolean isSessionValid();

    void uploadUserImage(File file,ResultWaiter actions);

    // --- MODELS ---- //
    User getCurrentUser();

    void getUser(String userId, ResultWaiter waiter);

    void offerFriendship(User user,ResultWaiter waiter);

    void cancelFriendship(User user,ResultWaiter waiter);

    void confirmFriendship(User user, ResultWaiter waiter);

    void getFriends(String userId, MassiveQueryInputData data, int status, ResultWaiter waiter, boolean includeInviteForMe);

    int convertInAppError(int errorCodeApi);

    int convertInAppError(String message);

    void getShortFriendsList(String userId, ResultWaiter waiter);

    void createPoema(Poema poema, ResultWaiter waiter);

    void getPoems(String userId, MassiveQueryInputData data, ResultWaiter waiter);

    void deletePoems(Poema poema, ResultWaiter waiter);

    void startMessagingService();

    void stopMessageingService();

    void getPoemsMessage(Poema poema, MassiveQueryInputData data, ResultWaiter waiter);

    void sendMessage(Poema poema, String poemaMessage, ResultWaiter waiter);

    void readMessage(Poema poema, ResultWaiter waiter);

    void editMessage(PoemaMessage poemaMessage, String newText, ResultWaiter waiter);

    void getPoema(String poemaId, ResultWaiter waiter);

    void restorePassword(String userId, ResultWaiter waiter);

    void registerDevice(String userId, ResultWaiter waiter);

    //TODO когда пользователь выходит из приложения "выход" жмет или же возникает ошибка истечения токена
    void unregisterDevice(ResultWaiter waiter);

    void getAllPoema(String poemaId, ResultWaiter waiter);

    void sendFeedback(String userId, String category, String message, ResultWaiter waiter);
}
