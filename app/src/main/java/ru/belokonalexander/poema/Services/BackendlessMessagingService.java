package ru.belokonalexander.poema.Services;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;

import com.backendless.Backendless;
import com.backendless.Subscription;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.messaging.Message;
import com.backendless.messaging.SubscriptionOptions;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import StaticHelper.LogSystem;
import StaticHelper.Settings;
import belokonalexander.Api.CurrentApi;
import belokonalexander.Api.LocalQueryExecutorExplicit;
import belokonalexander.Api.Models.PoemaMessage;
import belokonalexander.Api.Models.QueryLog;
import belokonalexander.Api.Models.SugarAdditional.SugarPoemaDB;
import belokonalexander.GlobalShell;
import belokonalexander.SharedAppPrefs;
import ru.belokonalexander.poema.Events.MessageEvent;

public class BackendlessMessagingService extends Service {
    public BackendlessMessagingService() {
    }


    private Subscription activeSubscription;

    private Set<String> myPoemsIDs = new HashSet<>();

    @Override
    public void onCreate() {
        super.onCreate();

        LogSystem.logThis(" ON CREATE SERVICE");
    }

    public static final String OPERATION_DESC = "createMessages";


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        LogSystem.logThis(" ON START COMMAND SERVICE");


        SubscriptionOptions subscriptionOptions = null;
        try {
            subscriptionOptions = new SubscriptionOptions();
            subscriptionOptions.setSubtopic(CurrentApi.getInstanse().getCurrentUser().getObjectId());
        } catch (Exception e) {
            e.printStackTrace();
            stopSelf();
            super.onStartCommand(intent, flags, startId);
        }

        Backendless.Messaging.subscribe("default",new AsyncCallback<List<Message>>() {
            @Override
            public void handleResponse(List<Message> messages) {

                List<PoemaMessage> poemaMessages = new ArrayList<>();
                for(Message message : messages){
                    PoemaMessage pm = new PoemaMessage(message);
                    pm.setQueryLog(new QueryLog(pm.getObjectId(), PoemaMessage.class,OPERATION_DESC+"?"+pm.getPoema()));
                    poemaMessages.add(pm);
                }

                if(SharedAppPrefs.getInstance().getUsingCache()) {

                    new AsyncTask<Object,Object,Object>() {
                        @Override
                        protected Object doInBackground(Object[] objects) {

                            SugarPoemaDB.getInstance().save(poemaMessages);
                            for(PoemaMessage msg : poemaMessages) {
                                SugarPoemaDB.getInstance().updatePoemaLastMessage(msg.getPoema(),msg.getObjectId());
                            }
                            EventBus.getDefault().post(new MessageEvent(poemaMessages));

                            return null;
                        }
                    }.execute();
                }

            }

            @Override
            public void handleFault(BackendlessFault backendlessFault) {
                LogSystem.logThis(" MESSAGES FAULT: " + backendlessFault);
            }
        }, subscriptionOptions, Settings.PULL_INTERVAL, new AsyncCallback<Subscription>() {
            @Override
            public void handleResponse(Subscription subscription) {
                activeSubscription = subscription;


                LogSystem.logThis(" Subscription ANSWER: " + subscription.getPollingInterval() + " " + Thread.currentThread().getName());
            }

            @Override
            public void handleFault(BackendlessFault backendlessFault) {
                LogSystem.logThis(" MESSAGES FAULT: " + backendlessFault);
            }
        });




        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LogSystem.logThis(" ON DESTROY SERVICE");
        if(activeSubscription!=null)
            activeSubscription.cancelSubscription();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.

        LogSystem.logThis(" ON BIND SERVICE");
        return null;
    }
}
