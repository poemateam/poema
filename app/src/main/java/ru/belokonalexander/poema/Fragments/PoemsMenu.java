package ru.belokonalexander.poema.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.RelativeLayout;

import com.liucanwen.app.headerfooterrecyclerview.HeaderAndFooterRecyclerViewAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;


import StaticHelper.LogSystem;

import belokonalexander.Api.CurrentApi;

import belokonalexander.Api.Models.Poema;

import belokonalexander.Api.Models.PoemaMessage;
import belokonalexander.Api.Models.SugarAdditional.SugarPoemaDB;
import belokonalexander.Api.Models.User;

import belokonalexander.Api.QueryExecutor;
import belokonalexander.Api.QueryExecutorExplicit;

import belokonalexander.Api.ResultWaiter;
import belokonalexander.GlobalShell;
import belokonalexander.SuppleRecycler.ActionRecycler;
import belokonalexander.SuppleRecycler.AutoRefreshActionRecycler;
import belokonalexander.SuppleRecycler.DataProviders.Abstracts.GetPoemsProvider;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.belokonalexander.poema.Events.MessageEvent;
import ru.belokonalexander.poema.Events.MessageStatusEvent;
import ru.belokonalexander.poema.Events.PoemaEvent;
import ru.belokonalexander.poema.Fragments.ListAdapters.PoemsAdapter;
import ru.belokonalexander.poema.MainActivity;
import ru.belokonalexander.poema.PageManagers.BackendRunner;
import ru.belokonalexander.poema.PageManagers.Types.ListData;
import ru.belokonalexander.poema.R;

import static StaticHelper.Settings.SHOW_ITEMS;

/**
 * Created by alexander on 28.11.2016.
 */
public class PoemsMenu extends NavigationFragment implements ListData {

    @BindView(R.id.app_toolbar)
    Toolbar toolbar;


    @BindView(R.id.recycler_my_poems)
    AutoRefreshActionRecycler<Poema> recyclerMyPoems;

    //CommonAdapter adapter = null;


    static final String ANIMATION_IMAGE = "animation_image";

    @OnClick(R.id.create_new_poema)
    void createNewPoem(View view){
        Fragment fragment = new FriendsSelector();
        fragment.setTargetFragment(this,REQUEST_FRIEND);
        ((MainActivity)getActivity()).openInThisContainer(fragment);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        if(getRootView()!=null)
            return getRootView();

        View view = inflater.inflate(R.layout.fragment_poems_menu,container,false);
        ButterKnife.bind(this, view);

        fillToolbar(getString(R.string.poems));

        setRootView(view);


        List<Poema> poems = new ArrayList<>();

        LinearLayoutManager llm = new LinearLayoutManager(getContext());

        recyclerMyPoems.setVisibility(View.INVISIBLE);
        recyclerMyPoems.setLayoutManager(llm);

        HeaderAndFooterRecyclerViewAdapter adapterWithHeader = new HeaderAndFooterRecyclerViewAdapter(new PoemsAdapter(poems,this));

        recyclerMyPoems.setAdapter(adapterWithHeader);
        recyclerMyPoems.init(toolbar);
        recyclerMyPoems.setProvider(new GetPoemsProvider(CurrentApi.getInstanse().getCurrentUser()));

        recyclerMyPoems.setAfterCacheData(() -> getActivity().runOnUiThread(() -> {
            recyclerMyPoems.setVisibility(View.VISIBLE);
            ButterKnife.apply(recyclerMyPoems, SHOW_ITEMS);
        }));



        recyclerMyPoems.getData(ActionRecycler.UpdateMode.INITIAL);


        

        return view;
    }



    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }


    private static final int REQUEST_FRIEND = 101;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == Activity.RESULT_OK){
            switch (requestCode){
                case REQUEST_FRIEND:

                    User user = SugarPoemaDB.getInstance().getUserById(data.getStringExtra("USER_ID"));
                    String title = data.getStringExtra("TITLE");
                    int[] cords = data.getIntArrayExtra("IMAGE_LOCATION_SCREEN");
                    int[] sizes = data.getIntArrayExtra("IMAGE_SIZE");

                    Poema poema = new Poema(title, CurrentApi.getInstanse().getCurrentUser(),user);
                    createPoem(poema);

                    break;
            }
        }
    }

    private void createPoem(Poema poema) {

        QueryExecutorExplicit createNewPoem = new QueryExecutorExplicit(GlobalShell.getParentHash(this.getClass(),poema.getTitle()), resultWaiter -> CurrentApi.getInstanse().getApplicationApi().createPoema(poema, resultWaiter));

        recyclerMyPoems.addFirstItem(poema);

        BackendRunner backendRunner = new BackendRunner(answers -> {
        },
           messsage -> {
            LogSystem.logThis(" ERROR: " + messsage);
            int i =0;
               for(Poema p : recyclerMyPoems.getDataAdapter().getData()){
                   if(poema.equals(p)){
                       recyclerMyPoems.removeItem(i);
                       break;
                   }
               }

        }, createNewPoem);
        backendRunner.run();
    }

    Animation getTranslateAnimation(View view){

        int startWH = view.getLayoutParams().width;

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                super.applyTransformation(interpolatedTime, t);

                //LogSystem.logThis("INTER: " + interpolatedTime);

                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
                params.leftMargin = (int)(300 * interpolatedTime);
                params.height = (int) (startWH - GlobalShell.dpToPixels(20) * interpolatedTime);
                params.width = (int) (startWH - GlobalShell.dpToPixels(20) * interpolatedTime);
                view.setLayoutParams(params);
            }
        };

        a.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                LogSystem.logThis(" END ANIMATION: " + view);
                if(view.getParent()!=null)
                     ((ViewGroup)view.getParent()).removeView(view);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        a.setDuration(2500);
        return  a;
    }

    @Override
    public void onPause() {
        super.onPause();
        LogSystem.logThis("ON PAUSE");
    }


    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        LogSystem.logThis(" POEMA ON RESUME: ");

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPoemaStatusEvent(MessageStatusEvent messageStatusEvent){
        List<Poema> poemas = recyclerMyPoems.getDataAdapter().getData();

        PoemaMessage item = messageStatusEvent.getPoemaMessage();
        int index = 0;
        for(Poema poema : poemas){
            if(poema.getLastMessage()!=null && poema.getLastMessage().getObjectId().equals(item.getObjectId())){
                poema.setLastMessage(item);
                recyclerMyPoems.updateItem(index);
                break;
            }
            index++;
        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPoemaEvent(PoemaEvent poemaEvent){
        List<Poema> data = recyclerMyPoems.getDataAdapter().getData();
        Poema eventItem = poemaEvent.getPoema();



        for(Poema poema : data) {
            if (poema.getObjectId().equals(eventItem.getObjectId())) {
                recyclerMyPoems.replaceWith(poema, eventItem, false);
                eventItem.updateInCache();
                break;
            }
        }

        recyclerMyPoems.sort((o1, o2) -> {
            //TODO NULL
            //long first = o1.getLastMessage().getCreated().getTime();
            //long second =  o2.getLastMessage().getCreated().getTime();

            long first = o1.getUpdated().getTime();
            long second =  o2.getUpdated().getTime();

            return (first>second) ? -1 : (first<second) ? 1 : 0;
        });

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNewMessagesEvent(MessageEvent messages){
        LogSystem.logThis(" NEW MESSAGES IN POEMA: " + messages);
        List<PoemaMessage> eventItems = messages.getPoemaMessage();

        List<Poema> poems = recyclerMyPoems.getDataAdapter().getData();

        //оставляю только последние сообщения максимум для 1 поэмы
        Set<String> forUniquePoems = new HashSet<>();
        for(PoemaMessage message : eventItems){
            forUniquePoems.add(message.getPoema());
        }

        Collections.reverse(eventItems);

        List<PoemaMessage> poemaMessages = new ArrayList<>();
        for(String id : forUniquePoems){
            for(PoemaMessage pm : eventItems){
                if(id.equals(pm.getPoema())){
                    poemaMessages.add(pm);
                    break;
                }
            }
        }

        eventItems = poemaMessages;

        int itemsFound = 0;
        List<PoemaMessage> notFoundMessages = new ArrayList<>();
        List<Poema> inserted = new ArrayList<>();
        for(PoemaMessage message : eventItems){
            int index = 0;
            int startItemsFound = itemsFound;

            for(Poema poema : poems){

                if(poema.getObjectId().equals(message.getPoema())){
                    //changedIndexes.add(index);
                    poema.setLastMessage(message);
                    poema.updateInCache();
                    LogSystem.logThis(" INDEXS: " + index);
                    inserted.add(recyclerMyPoems.removeItem(index));
                    itemsFound++;
                    break;
                }
                index++;
            }

            recyclerMyPoems.insertInStart(inserted);



            if(startItemsFound==itemsFound){
                notFoundMessages.add(message);
            }

            //просто обновляем страничук
            if(notFoundMessages.size()>=2){ //если много сообщений с неизвестных поэм, то просто обновляем список
                recyclerMyPoems.getData(ActionRecycler.UpdateMode.REFRESH);
            } else if(notFoundMessages.size()==1) {

                //если одно сообщение в неизвестной поэме, то просто запрашиваем эту поэму

                long startTime = new Date().getTime();

                QueryExecutorExplicit loadPoema = new QueryExecutorExplicit(GlobalShell.getParentHash(this.getClass()),
                        resultWaiter -> CurrentApi.getInstanse().getApplicationApi().getPoema(notFoundMessages.get(0).getPoema(), resultWaiter));

                BackendRunner backendRunner = new BackendRunner(new BackendRunner.SuccessAnswer() {
                    @Override
                    public void onSuccessCompleted(List<Object> answers) {
                        if(recyclerMyPoems.getLastSuccessCall()<startTime){
                            if(answers.get(0) instanceof Poema)
                            recyclerMyPoems.addFirstItem((Poema) answers.get(0));
                        }
                    }
                }, new BackendRunner.FailureAnswer() {
                    @Override
                    public void onFailureCompleted(String messsage) {

                    }
                }, loadPoema);

                backendRunner.run();

            }




        }





    }


    @Override
    public ActionRecycler getActionRecycler() {
        return recyclerMyPoems;
    }
}
