package belokonalexander.Api.Models.Annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by alexander on 22.09.2016.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Relation {
}
