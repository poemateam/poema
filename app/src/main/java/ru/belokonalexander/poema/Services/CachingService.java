package ru.belokonalexander.poema.Services;

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;

import java.util.ArrayList;

import StaticHelper.LogSystem;
import StaticHelper.Settings;
import belokonalexander.Api.Models.SugarAdditional.SugarPoemaDB;

public class CachingService extends IntentService {




    public CachingService() {
        super("Caching Service");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        Object response = intent.getSerializableExtra("data");

        try {
            SugarPoemaDB.getInstance().save(response);
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }


}
