package belokonalexander;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.ColorInt;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.ybq.android.spinkit.style.DoubleBounce;
import com.github.ybq.android.spinkit.style.ThreeBounce;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import StaticHelper.LogSystem;
import StaticHelper.Settings;
import ru.belokonalexander.poema.CustomViews.CustomScrollView;
import ru.belokonalexander.poema.Interfaces.OnScrollFullDown;
import ru.belokonalexander.poema.Interfaces.OnScrollTItleTransition;
import ru.belokonalexander.poema.Interfaces.OnScrollViewListener;
import ru.belokonalexander.poema.R;
import rx.Observable;

import rx.Subscriber;
import rx.functions.Action1;
import rx.observables.ConnectableObservable;

/**
 * Created by admin on 26.07.2016.
 */
public class GlobalShell {

    /*
        open other activity and close parent
    */
    public static void openActivityWithCloseThis(Context context, Class activity)
    {
        Intent intent = new Intent();
        intent.setClass(context,activity);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

        context.startActivity(intent);

    }

    public static void openActivityWithCloseThis(Context context, Class activity, Intent homeIntent)
    {
        Intent intent = new Intent();
        intent.setClass(context,activity);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.putExtra("EXTEND", homeIntent.getExtras());
        context.startActivity(intent);

    }


    public static <T> void notifyAllSubscribers(Observable<T> obs, Subscriber<T>... subs)
    {
        ConnectableObservable connectableObservable = obs.publish();
        for(Subscriber sub : subs)
        {
            connectableObservable.subscribe(sub);
        }

        connectableObservable.connect();
    }


    public static <T> void notifyAllSubscribers(Observable<T> obs, Action1<T>... subs)
    {
        ConnectableObservable connectableObservable = obs.publish();
        for(Action1 sub : subs)
        {
            connectableObservable.subscribe(sub);
        }

        connectableObservable.connect();
    }




    public static int dpToPixels(float dp) {
        return (int) (dp * DevicePrefs.getScale());
    }

    public static float pixelsToDp(int px) {
        return (px/ DevicePrefs.getScale());
    }


    public static OnScrollViewListener customHeaderScroll(View view,int maxScrollY, OnScrollFullDown onScrollFullDown, OnScrollTItleTransition onScrollTitleTransition)
    {
        return (v, x, y, oldl, oldt) -> {

            int translationY = Math.max(-v.getCurrentScrollY(),-(maxScrollY));

            int currentTranslation = Math.min(translationY,0);

            float translationPercent = ((float)-currentTranslation/(float)maxScrollY);

            float mantieAlpha = (float)translationFunction(translationPercent);

            ColorDrawable cd = (ColorDrawable) view.getBackground();

            cd.setAlpha((int) (255 * mantieAlpha));

            if(onScrollTitleTransition!=null)
            {
                onScrollTitleTransition.action(translationPercent);
            }

            if(onScrollFullDown!=null)
                if(mantieAlpha==1)
                    onScrollFullDown.action(true);
                else onScrollFullDown.action(false);


        };
    }

    public static double translationFunction(float percent) // percent is t
    {
        //return new AnticipateInterpolator().getInterpolation(percent);
        return new LinearInterpolator().getInterpolation(percent);
    }

    public static boolean startImageChooser(Activity callback, int resultCode)
    {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        callback.startActivityForResult(intent, resultCode );
        return true;
    }

    public static final int PROFILE_IMAGE_CHANGED = 101;

    public static void setCursorDrawableColor(EditText editText, int color) {
        try {
            Field fCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            fCursorDrawableRes.setAccessible(true);
            int mCursorDrawableRes = fCursorDrawableRes.getInt(editText);
            Field fEditor = TextView.class.getDeclaredField("mEditor");
            fEditor.setAccessible(true);
            Object editor = fEditor.get(editText);
            Class<?> clazz = editor.getClass();
            Field fCursorDrawable = clazz.getDeclaredField("mCursorDrawable");
            fCursorDrawable.setAccessible(true);
            Drawable[] drawables = new Drawable[2];
            drawables[0] = editText.getContext().getResources().getDrawable(mCursorDrawableRes);
            drawables[1] = editText.getContext().getResources().getDrawable(mCursorDrawableRes);
            drawables[0].setColorFilter(color, PorterDuff.Mode.SRC_IN);
            drawables[1].setColorFilter(color, PorterDuff.Mode.SRC_IN);
            fCursorDrawable.set(editor, drawables);
        } catch (Throwable ignored) {
        }
    }

    public static String getRealPathFromURI(Uri contentUri, Context context) {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = context.getContentResolver().query(contentUri, projection, null, null, null);
        if (cursor == null) return null;
        int column_index =             cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String s=cursor.getString(column_index);
        cursor.close();
        return s;

    }


    public static String getParentHash(Class c)
    {
        String hash;
        try {
            hash = Thread.currentThread().getStackTrace()[3].getClassName() + "." + Thread.currentThread().getStackTrace()[3].getMethodName();
        } catch (Exception e)
        {
            hash = c.getCanonicalName();
        }
        //TODO add hash function
        return hash;
    }

    public static String getParentHash(Class c, String additionalId)
    {
        String hash;
        try {
            hash = Thread.currentThread().getStackTrace()[3].getClassName() + "." + Thread.currentThread().getStackTrace()[3].getMethodName();
        } catch (Exception e)
        {
            hash = c.getCanonicalName();
        }
        //TODO add hash function
        return hash+additionalId;
    }





    public static String getHashAndDirOfBitmap(Bitmap bmp, boolean isSmall) {

        byte[] bitmapArray;

        if(isSmall)
        {
            bitmapArray = bitmapToByteArray(bmp, Bitmap.CompressFormat.JPEG, 40);
        } else bitmapArray = bitmapToByteArray(bmp, Bitmap.CompressFormat.JPEG, 100);

        String hash = null;
        byte[] help = new byte[bitmapArray.length];

        for(int i =0; i < bitmapArray.length; i++)
            help[i] = (byte) (((int)bitmapArray[i] * 31)%255);

        hash = SHAsum(bitmapArray) + SHAsum(help).substring(0,2);

        return hash;
    }

    public static byte[] bitmapToByteArray(Bitmap bmp, Bitmap.CompressFormat format, int qualuty)
    {
        byte[] byteArray = null;

        try {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(format, qualuty, stream);
        byteArray = stream.toByteArray();
        stream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return byteArray;
    }

    public static String SHAsum(byte[] convertme)  {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-1");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return byteArray2Hex(md.digest(convertme));
    }

    private static String byteArray2Hex(final byte[] hash) {
        Formatter formatter = new Formatter();
        for (byte b : hash) {
            formatter.format("%02x", b);
        }
        return formatter.toString();
    }



    public static String getFileExtension(File file)
    {
        String path = file.getAbsolutePath();
        String extension = path.substring(path.lastIndexOf(".")+1, path.length());
        return extension;
    }


    public static String getBase64(Bitmap bitmap)
    {
        String base64 = "";
        byte[] fileContent = bitmapToByteArray(bitmap, Bitmap.CompressFormat.JPEG, 100);
        base64 = Base64.encodeToString(fileContent,
                Base64.NO_WRAP);
        return base64;
    }


    public static Bitmap getMiniBitmap(Bitmap original) {

        float hw = (float)original.getHeight()/(float)original.getWidth();

        return Bitmap.createScaledBitmap(original, (int) (300/hw),300,false);
    }


    public static int getCurrentCheckedPosition(RadioGroup group){
        int pos = 0;
        for(int i =0; i < group.getChildCount(); i++){
            RadioButton button = (RadioButton) group.getChildAt(i);
            if(button.isChecked()){
                pos = i;
                break;
            }
        }
        return pos;
    }


    public static ProgressBar inflateWithProgressBar(Context context, RelativeLayout relativeLayout, int size, int initState, int marginTop, int marginLeft){

        ProgressBar progressElement = null;

        for(int i=0; i < relativeLayout.getChildCount(); i++){
            if(relativeLayout.getChildAt(i) instanceof ProgressBar){
                progressElement = (ProgressBar) relativeLayout.getChildAt(i);
                break;
            }
        }
        if(progressElement==null) {
            progressElement = new ProgressBar(context);
            relativeLayout.addView(progressElement);
            ThreeBounce threeBounce = new ThreeBounce();
            threeBounce.setColor(ContextCompat.getColor(context,R.color.colorAccent));
            progressElement.setIndeterminateDrawable(threeBounce);
        }


        RelativeLayout.LayoutParams layoutParams =
                (RelativeLayout.LayoutParams)progressElement.getLayoutParams();
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        progressElement.getLayoutParams().height=GlobalShell.dpToPixels(size);
        progressElement.getLayoutParams().width=GlobalShell.dpToPixels(size);
        layoutParams.setMargins(marginLeft, marginTop, 0, 0);
        progressElement.setLayoutParams(layoutParams);
        progressElement.setVisibility(initState);
        progressElement.bringToFront();



        return progressElement;
    };





    public static String randomString( int len ){
        final String AB = "ABCDEFGHIJabcdefghij";
        SecureRandom rnd = new SecureRandom();
        StringBuilder sb = new StringBuilder( len );
        for( int i = 0; i < len; i++ )
            sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
        return sb.toString();
    }



    public static  String listToINSQLClause  (List<?> list){

        String result = "";

        if(list.size()>0)
        {
            String delimiter = "";
            if(list.get(0) instanceof String)
                delimiter = "'";

            for(Object item : list){
                if(result.length()==0)
                    result += delimiter + item.toString() + delimiter;
                else result +="," + delimiter + item.toString() + delimiter;
            }

            result = "(" + result + ")";

        } else result = "()";

        return result;
    }

    public static  String listToINSQLClause  (List<?> list, String fieldPrefix){

        String result = "";

        if(list.size()>0)
        {
            String delimiter = "";
            if(list.get(0) instanceof String)
                delimiter = "'";

            for(Object item : list){
                if(result.length()==0)
                    result += delimiter + fieldPrefix + item.toString() + delimiter;
                else result +="," + delimiter + fieldPrefix + item.toString() + delimiter;
            }

            result = "(" + result + ")";

        }

        return result;
    }



    public static boolean listsAreEquals(List<Object> one, List<Object> two) {

        //this is recursive deep compare function
        //we get list of objects (it can be array of arrays)
        // high complexity which depends of deep (arrays as elements of lists)



        if(one==null || two==null)
            if(one==null && two==null)return true;
                else return false;


        if(one.size()!=two.size()){
            return false;
        } else {

            if(one.size()==0)
                return true;

            // i - object of answer
            for(int i =0; i < one.size(); i++){


                if(one.get(i) instanceof List){

                    if(!listsAreEquals((List)one.get(i),(List)two.get(i))){
                        return false;
                    }

                } else {
                    if(!two.contains(one.get(i))){
                        return false;
                    }
                }
            }
        }

        return true;
    }



    public static List<Object> iteratorToList(Iterator it) {

        List<Object>  result = new ArrayList<>();
        while (it.hasNext()){
            Object o = it.next();
            result.add(o);

        }

        return result;
    }

    public static String formAntonimCommand(String input, String action){
        return input.replaceFirst(firstCamelWord(input),action);
    };

    public static String firstCamelWord(String str){
        StringBuilder currentCommand = new StringBuilder();
        for(char ch : str.toCharArray()){
            if(Character.isUpperCase(ch)){
                break;
            }
            currentCommand.append(ch);
        }

        return currentCommand.toString();
    }

    public static  <T> boolean compareTwoObjects(T one, T two){
        if(one==null || two==null){
            return two == one;
        } else {
            return one.equals(two);
        }
    }

    public static int dayCount(Date date){ //return current day (HIGHT FLOR)

        //return (int) (date.getTime()/(1000*60*60*24));
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        return calendar.get(Calendar.DAY_OF_YEAR);
    }

    public static boolean internetIsOn(Context context) {


        try {
            ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                    connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
                return  true;
            }
            return false;
        } catch (Exception e) {

            try {
                ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                if (activeNetwork != null) { // connected to the internet
                    if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                        // connected to wifi
                        return true;
                    } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                        // connected to the mobile provider's data plan
                        return true;
                    }
                } else {
                    return false;
                }
            } catch (Exception e1) {
                return false;
            }
        }
        return false;
    }

    public static void tryCloseKeyboard(Activity activity){
        try { // hide keyboard
            InputMethodManager inputManager = (InputMethodManager) activity.
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //public static boolean keyboardIsOpen()

    private static final AtomicLong TIME_STAMP = new AtomicLong();
    // can have up to 1000 ids per second.
    public static long getUniqueMillis() {
        long now = System.currentTimeMillis();
        while (true) {
            long last = TIME_STAMP.get();
            if (now <= last)
                now = last + 1;
            if (TIME_STAMP.compareAndSet(last, now))
                return now;
        }
    }

    public static Map<String, Object> jsonToMap(JSONObject json) throws JSONException {
        Map<String, Object> retMap = new HashMap<String, Object>();

        if(json != JSONObject.NULL) {
            retMap = toMap(json);
        }
        return retMap;
    }

    public static Map<String, Object> toMap(JSONObject object) throws JSONException {
        Map<String, Object> map = new HashMap<String, Object>();

        Iterator<String> keysItr = object.keys();
        while(keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);

            if(value instanceof JSONArray) {
                value = toList((JSONArray) value);
            }

            else if(value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            map.put(key, value);
        }
        return map;
    }

    public static List<Object> toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<Object>();
        for(int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if(value instanceof JSONArray) {
                value = toList((JSONArray) value);
            }

            else if(value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }



    public static Bitmap getBitmapFromURL(String url){
        return null;
    };

}
