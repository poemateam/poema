package ru.belokonalexander.poema.PageManagers;

import android.support.v7.widget.ActionMenuView;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;

import java.util.Date;
import java.util.List;

import StaticHelper.LogSystem;
import StaticHelper.Settings;
import belokonalexander.Api.ApiTask;
import belokonalexander.Api.QueryExecutorExplicit;
import ru.belokonalexander.poema.Fragments.FragmentContentWaiter;
import ru.belokonalexander.poema.Fragments.NavigationFragment;
import ru.belokonalexander.poema.R;

/**
 * Created by alexander on 14.11.2016.
 */

/*
*
* fragment can be of two types:
* 1) all data loading from backend - we're waiting for request and show @FragmentContentWaiter
* 2) we have @boolean getFromCache() method, and if got true - show cached data
*       otherwise (if false) -> run @FragmentContentWaiter
*    in this case we have two methods -> @boolean getFromCache() and @void getDataFromBackend() from [1]
*    after Cached we run async task for get backend data and block all views that initiate post requests
*
*  3) We have interface ApiPostAgent -> if some view has this interface then the view doesn't enable until getDataFromBackend() finish
 */

public abstract class ApiFragment extends NavigationFragment {

    QueryExecutorExplicit[] backendTasks;

    Date lastUpdate = null;

    View refreshView;

    public void setBackendTasks(QueryExecutorExplicit... backendTasks) {
        this.backendTasks = backendTasks;
    }

    public abstract void showView(List<Object> data, boolean fromCache);           // fill view from model


    public void buildFragment(){
        new FragmentContentWaiter(getContext(), (ViewGroup) getRootView(), getToolbarContainer(), answers -> showView(answers, false), backendTasks).startLoading();
    }

    public void initApiFragment(){

        MenuItem refreshItem = getToolbar().getMenu()
                .add(R.string.refresh);
        refreshItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        refreshItem.setOnMenuItemClickListener(menuItem -> {
            refreshPage();
            return false;
        });
        refreshItem.setIcon(R.drawable.ic_autorenew_white_36dp);
        ActionMenuView amv = (ActionMenuView) getToolbar().getChildAt(getToolbar().getChildCount()-1);
        refreshView = amv.getChildAt(0);


    }

     public void refreshPage(){

        for(ApiTask item: backendTasks)
            item.refresh();

        startRefresh();
        new BackendRunner(answers -> {showView(answers, false); finishRefresh();}, messsage -> finishRefresh(), backendTasks).run();
    }

    boolean taskRunning = false;

    public boolean isTaskRunning() {
        return taskRunning;
    }

    void startRefresh(){

        LogSystem.logThis(" START TEFRESHING /// ");

        taskRunning = true;



        refreshView.setEnabled(false);

        AnimationSet animationSet = new AnimationSet(true);

        Animation animationRotate = new RotateAnimation(0,360,Animation.RELATIVE_TO_SELF, .5f, Animation.RELATIVE_TO_SELF, .5f);
        animationRotate.setDuration(Settings.REFRESH_ROTATE_ANIMATION_DURATION);
        animationRotate.setRepeatMode(Animation.RESTART);
        animationRotate.setRepeatCount(Animation.INFINITE);
        animationSet.addAnimation(animationRotate);
        refreshView.startAnimation(animationSet);

    }

    void finishRefresh(){

        taskRunning = false;
        refreshView.clearAnimation();
        refreshView.setEnabled(true);
        lastUpdate = new Date();
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }
}
