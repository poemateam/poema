package belokonalexander.Api;

/**
 * Created by alexander on 16.11.2016.
 */
public class LocalQueryExecutorExplicit extends LocalQueryExecutor implements ApiTask {

    public LocalQueryExecutorExplicit(String parentHash,  QueryExecutor.Actions queryAction) {
        super(parentHash);
        this.queryAction = queryAction;
    }

    public LocalQueryExecutorExplicit(String parentHash,  QueryExecutor.Actions queryAction, boolean selfInitialization) {
        super(parentHash);
        this.queryAction = queryAction;

        if(selfInitialization){
            refresh();
        }
    }

    QueryExecutor.Actions queryAction;

    public Object execute() {
        return super.execute(queryAction);
    }

    @Override
    public void refresh() {
        resultWaiter = new ResultWaiter();
    }

    @Override
    public void cancel() {
        resultWaiter.setInterrupted();
    }

    public QueryExecutor.Actions getQueryAction() {
        return queryAction;
    }



}
