package ru.belokonalexander.poema.Fragments.ListAdapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.List;

import StaticHelper.LogSystem;
import belokonalexander.Api.Models.SearchEntityHolder;
import belokonalexander.Api.Models.SearchedEntity;

/**
 * Created by alexander on 09.09.2016.
 * adapter for search results witch represents like recycler view
 */
public class SearchAdapter<T extends SearchEntityHolder> extends CommonAdapter {


    //entity it's dummy SearchEntity object that represent access to static fields of SearchEntity interface implementation
    //from the static fields we get concrete realisation of view holder
    SearchEntityHolder entity;

    public SearchAdapter(List<T> searchItems,  SearchedEntity entity, Fragment fragmentHolder) {
        super(searchItems,fragmentHolder);
        this.entity = (SearchEntityHolder) entity;
    }

    public Class entityType(){
        return entity.getClass();
    }

    @Override
    RecyclerView.ViewHolder onCreateVH(ViewGroup parent, int viewType) {
        return entity.getSearchViewForItem(parent, this);
    }

    @Override
    void onBindVH(RecyclerView.ViewHolder holder, int position) {
        entity.onBindViewHolder(holder,(SearchedEntity)items.get(position),position, context); // fill created view holder by data
    }

}
