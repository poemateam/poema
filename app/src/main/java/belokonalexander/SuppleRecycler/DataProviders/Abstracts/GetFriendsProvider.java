package belokonalexander.SuppleRecycler.DataProviders.Abstracts;

import java.util.ArrayList;
import java.util.List;

import StaticHelper.LogSystem;
import belokonalexander.Api.CurrentApi;
import belokonalexander.Api.InputQueryDataHolders.MassiveQueryInputData;
import belokonalexander.Api.LocalQueryExecutorExplicit;
import belokonalexander.Api.Models.SugarAdditional.SugarPoemaDB;
import belokonalexander.Api.Models.User;
import belokonalexander.Api.QueryExecutor;
import belokonalexander.GlobalShell;

/**
 * Created by alexander on 10.10.2016.
 */
public class GetFriendsProvider extends SolidProvider<User> {

    User user;

    public GetFriendsProvider(User user) {
        this.user = user;
    }

    @Override
    public List<User> getData(MassiveQueryInputData state) {
        QueryExecutor task = new QueryExecutor(GlobalShell.getParentHash(getClass()));

        int status = user.getObjectId().equals(CurrentApi.getInstanse().getCurrentUser().getObjectId()) ? 3 : 2;

        LogSystem.logThis(" STATUS: " + status);

        Object data = task.execute(resultWaiter -> CurrentApi.getInstanse().getApplicationApi().getFriends(user.getObjectId(), state, status, resultWaiter,true));
        if(data instanceof List)
            return (List<User>) data;

        return null;
    }

    @Override
    public List<User> getCacheData(int pageSize) {

        String[] show = {"getFriends?" + user.getObjectId(),"createFriends?"+user.getObjectId(), "updateFriends?"+user.getObjectId() };

        LocalQueryExecutorExplicit getUsersLocal = new LocalQueryExecutorExplicit(GlobalShell.getParentHash(this.getClass()),
                resultWaiter -> resultWaiter.setResult(SugarPoemaDB.getInstance().getTopUsers(pageSize, show)), true);


        List result = null;
        try {
            result = (List) getUsersLocal.execute();
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }

        return result==null ? new ArrayList<>() : result;


    }
}
