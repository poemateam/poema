package StaticHelper;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.OvershootInterpolator;
import android.view.animation.TranslateAnimation;

import com.bumptech.glide.DrawableRequestBuilder;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import belokonalexander.Api.CurrentApi;
import belokonalexander.Api.Models.User;
import belokonalexander.GlobalShell;
import butterknife.ButterKnife;
import ru.belokonalexander.poema.R;

import static android.R.attr.id;

/**
 * Created by admin on 27.07.2016.
 */
public class Settings {

    public static final int DEBOUNCE_BUTTON_ANIMATION_CLICK_VALUE = 1000;
    public static final int THROTTLE_CLICK_VALUE = 1500;
    public static final int SEARCH_DEBOUNCE = 800;
    public static final int THROTTLE_CLICK_VALUE_LOCAL = 100;
    public static final int SEARCH_DEBOUNCE_LOCAL = 100;

    public static final int TOAST_ANIMATION_VALUE = 1000;
    public static final int TOAST_ANIMATION_VALUE_SHORT = 500;

    public static final int TOAST_SHOW_VALUE = 2500;
    public static final int TOAST_SHOW_VALUE_SHORT = 1000;



    public  static final int REFRESH_ROTATE_ANIMATION_DURATION = 1000;
    public  static final int REFRESH_TRANSLATE_ANIMATION_DURATION = 300;

    public static final int SHORT_USER_LIST_COUNT = 20;

    public static final int LIST_ENDLESS_PAGE_SIZE = -1;

    public static final int PULL_INTERVAL = 5000;

    public static final int MAX_IMAGE_SIZE = 8096000;

    public static final int INITIAL_ANIMATION = 500;

    public static final int INTERFACE_CHANGES_ANIMATION = 500;

    public static final String DATE_FORMAT_MASK = "hh:mm:ss dd-mm-yyyy";

    public static final String DATE_FORMAT_MASK_SHORT = "hh:mm:ss";

    public static final Long REFRESH_ON_RESUME_TIMEOUT = 3000L; //ms - 5min - сколько времени должно пройти с последнего успешного запроса, чтобы в onResume бновилась страничка

    public static<T> DrawableRequestBuilder setCommonOptions(DrawableRequestBuilder<T> options) {
        return options.diskCacheStrategy(DiskCacheStrategy.ALL).skipMemoryCache(false);
    }

    public enum DateFormat
    {
        Standart("HH:mm:ss dd-mm-yyyy"),
        Short("HH:mm"),
        ShortWithSeconds("HH:mm:ss"),
        StandartShort("HH:mm  dd/MM/yy"),
        StandartDate("dd MMM yyyy"),
        SmartChat();


        String pattern;


        DateFormat(String s) {
            pattern = s;
        }

        DateFormat() {

        }
    }

    public static String getDate(Date date, DateFormat dateFormat){

        String pattern;

        if(dateFormat==DateFormat.SmartChat){

            Date today = new Date();
            if(GlobalShell.dayCount(today)-GlobalShell.dayCount(date)==0){
                pattern = "HH:mm ";
            } else pattern = "dd MMM yy";

        } else {
            pattern = dateFormat.pattern;
        }

        SimpleDateFormat df = new SimpleDateFormat(pattern);
        return  df.format(date);
    }

    private static final String[] SUPPORTED_IMAGE_FILE_FORMATS = {"jpeg","bmp","png","jpg"};
    public static final long SLEEP_EXECUTOR_TIME = 20;

    public static Boolean isImageTypeSupported(String extension)
    {
        for(int i=0; i < SUPPORTED_IMAGE_FILE_FORMATS.length; i++)
            if(extension.equals(SUPPORTED_IMAGE_FILE_FORMATS[i]))
                return true;

        return false;
    }

    public static SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

    public static int UPDATE_INTERVAL_FRAGMENT = 5; //sec

    public static Drawable getDefaultImage(Context context, User user){

        int resource;

        if(Math.abs(CurrentApi.getInstanse().getLongHashOfItem(user.getObjectId())) % 2 == 0){
            resource = R.drawable.ava1__big;
        } else resource = R.drawable.ava2__big;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return context.getResources().getDrawable(resource, context.getTheme());
        } else {
            return context.getResources().getDrawable(resource);
        }
    }

    public static final ButterKnife.Action<View> SHOW_ITEMS = (view, index) -> {

        /*float from = -1;
        float to = 0;

        if(index%2==0){
            from = 1;
            to = 0;
        }

        Animation animation = new TranslateAnimation(Animation.RELATIVE_TO_SELF,from, Animation.RELATIVE_TO_SELF, to,Animation.RELATIVE_TO_SELF, 0,Animation.RELATIVE_TO_SELF, 0);
        animation.setDuration(Settings.INITIAL_ANIMATION);
        animation.setFillAfter(true);
        animation.setInterpolator(new OvershootInterpolator(1f));
        view.startAnimation(animation);*/
    };

    public static final ButterKnife.Action<View> SHOW_ITEMS_ANIMATE = (view, index) -> {

        float from = -1;
        float to = 0;

        if(index%2==0){
            from = 1;
            to = 0;
        }

        Animation animation = new TranslateAnimation(Animation.RELATIVE_TO_SELF,from, Animation.RELATIVE_TO_SELF, to,Animation.RELATIVE_TO_SELF, 0,Animation.RELATIVE_TO_SELF, 0);
        animation.setDuration(Settings.INITIAL_ANIMATION);
        animation.setFillAfter(true);
        animation.setInterpolator(new OvershootInterpolator(1f));
        view.startAnimation(animation);
    };

}
