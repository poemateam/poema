package StaticHelper;

/**
 * Created by alexander on 05.09.2016.
 */
public interface PopupCallback {

    void onResult(Object result);

}
