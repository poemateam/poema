package ru.belokonalexander.poema.PageManagers.Types;

import belokonalexander.SuppleRecycler.ActionRecycler;

/**
 * Created by Alexander on 16.02.2017.
 */

public interface ListData {
    ActionRecycler getActionRecycler();
}
