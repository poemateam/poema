package belokonalexander;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import StaticHelper.LogSystem;
import StaticHelper.Settings;
import belokonalexander.Api.CurrentApi;
import belokonalexander.Api.Models.User;
import ru.belokonalexander.poema.PoemaApplication;

/**
 * Created by admin on 26.07.2016.
 */
public class SharedAppPrefs {

    private  final String PREFS = "settings";
    private  final String UNIQUE = "unique_settings";
    private  final String USER = "user_settings";

    private  final String _COUNTER = "_counter";
    private  final String _SHOW_INTRO = "_show_intro";
    private  final String _DEVICE_REGISTRATION = "_push_registration"; //for device, может еще надо будет для каждого пользователя делать?

    //------ SETTINGS ------

    private  final String _USE_CACHE = "_use_caching";
    private  final String _SHOW_NOTIFICATIONS = "_notif_show";
    private  final String _NOTIFICATIONS_VIBRO = "_notif_vibro";
    private  final String _NOTIFICATIONS_SOUND = "_notif_sound";

    private final String  _CURRENT_USER = "_current_user";
    private final String _CURRENT_USER_HASH = "_current_user_hash";

    //------  ------  ------

    private  SharedPreferences SettingsCommon;
    private  SharedPreferences SettingsUnique;
    private  SharedPreferences SettingsUser;

    private static SharedAppPrefs sharedAppPrefs;

    public static SharedAppPrefs getInstance(){
        if(sharedAppPrefs==null){
            sharedAppPrefs = new SharedAppPrefs(PoemaApplication.getContext());
        }
        return sharedAppPrefs;
    }

    private SharedAppPrefs(Context context)
    {
            SettingsCommon = context.getSharedPreferences(PREFS,Context.MODE_PRIVATE);
            SettingsUnique = context.getSharedPreferences(UNIQUE,Context.MODE_PRIVATE);
            SettingsUser = context.getSharedPreferences(USER, Context.MODE_PRIVATE);
    }


    public  void incCounter()
    {
        SharedPreferences.Editor editor = SettingsCommon.edit();
        editor.putInt(_COUNTER, getCounter()+1);
        editor.apply();

    }

    public  Integer getCounter()
    {
        return SettingsCommon.getInt(_COUNTER,0);
    }

    public  Boolean getPushRegistration()
    {
        return SettingsCommon.getBoolean(_DEVICE_REGISTRATION,false);
    }

    public  void setCorrectPushRegistration(){
        SharedPreferences.Editor editor = SettingsCommon.edit();
        editor.putBoolean(_DEVICE_REGISTRATION, true);
        editor.apply();
    }

    public  void closeIntro()
    {
        SharedPreferences.Editor editor = SettingsCommon.edit();
        editor.putBoolean(_SHOW_INTRO, false);
        editor.apply();
    };

    public  boolean shouldShowIntro()
    {
        return SettingsCommon.getBoolean(_SHOW_INTRO,true);
    }

    public  void setUsingCache(Boolean usingCache) {
        SharedPreferences.Editor editor = SettingsUnique.edit();
        editor.putBoolean(_USE_CACHE, usingCache);
        editor.apply();
    }

    public  Boolean getUsingCache() {
        return SettingsUnique.getBoolean(_USE_CACHE,true);
    }

    public  void setUsingNotifications(Boolean usingCache) {
        SharedPreferences.Editor editor = SettingsUnique.edit();
        editor.putBoolean(_SHOW_NOTIFICATIONS, usingCache);
        editor.apply();
    }

    public  Boolean getUsingNotification() {
        return SettingsUnique.getBoolean(_SHOW_NOTIFICATIONS,true);
    }

    public  void setUsingNotificationsSound(Boolean usingCache) {
        SharedPreferences.Editor editor = SettingsUnique.edit();
        editor.putBoolean(_NOTIFICATIONS_SOUND, usingCache);
        editor.apply();
    }

    public  Boolean getUsingNotificationsSound() {
        return SettingsUnique.getBoolean(_NOTIFICATIONS_SOUND,true);
    }


    public  void setUsingNotificationsVibro(Boolean usingCache) {
        SharedPreferences.Editor editor = SettingsUnique.edit();
        editor.putBoolean(_NOTIFICATIONS_VIBRO, usingCache);
        editor.apply();
    }

    public  Boolean getUsingNotificationsVibro() {
        return SettingsUnique.getBoolean(_NOTIFICATIONS_VIBRO,true);
    }


    public  String getMainUserSettings() {
        return UNIQUE;
    }

    public  void clarUserSettings(){
        SettingsUnique.edit().clear().apply();
    }

    public synchronized void storeCurrentUser(User cu) {
        SharedPreferences.Editor editor = SettingsUser.edit();
        HashMap<String,Object> hashMap = new HashMap<>();
        cu.fillToMap(hashMap);
        JSONObject jsonObject = new JSONObject(hashMap);
        editor.putString(_CURRENT_USER,jsonObject.toString());
        editor.apply();

    }

    public User restoreCurrentUser(){
        String tmp = SettingsUser.getString(_CURRENT_USER, "");

        if(tmp.length()!=0) {
            try {
                JSONObject jsonObject = new JSONObject(tmp);

                Map<String, Object> map = GlobalShell.jsonToMap(jsonObject);

                String[] datefieldsNames = {"updated","created","birthday" };

                for(String name : datefieldsNames ){
                    if(map.get(name)!=null){
                        if((map.get(name))==JSONObject.NULL){
                            map.remove(name);
                        } else {
                            map.put(name,new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy").parse((String) map.remove(name)));
                        }
                    }
                }

                User user = new User();

                user.initFromMap(map);

                return user;

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return null;
    }


    public void setUserHash(String userHash) {
        SharedPreferences.Editor editor = SettingsUser.edit();
        editor.putString(_CURRENT_USER_HASH, userHash);
        editor.apply();
    }

    public String getUserHash() {
        return SettingsUser.getString(_CURRENT_USER_HASH, "");
    }

    public void clearUserData(){
        SettingsUser.edit().clear().apply();
    }
}
