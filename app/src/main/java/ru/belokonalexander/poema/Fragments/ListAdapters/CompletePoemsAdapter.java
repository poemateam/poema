package ru.belokonalexander.poema.Fragments.ListAdapters;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;


import com.ms.square.android.expandabletextview.ExpandableTextView;

import java.util.List;

import Helper.ClickableExpandableTextView;
import Helper.RecycledElement;
import StaticHelper.LogSystem;
import StaticHelper.Settings;
import belokonalexander.Api.Models.CompletePoema;
import ru.belokonalexander.poema.R;

/**
 * Created by alexander on 26.09.2016.
 */
public class CompletePoemsAdapter extends CommonAdapter<RecycledElement>  {

    ItemChangedCallback itemChangedCallback;

    public CompletePoemsAdapter(List items, Fragment fragmentHolder, ItemChangedCallback callback) {
        super(items, fragmentHolder);
        this.itemChangedCallback = callback;
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    RecyclerView.ViewHolder onCreateVH(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_complete_poema,parent,false);

        return new CompletePoemaViewHolder(v, new Clicks() {
            @Override
            public void mainClick(CompletePoema poem) {
                LogSystem.logThis("123");
            }

            @Override
            public void onLongClick(CompletePoema poema) {

            }
        }, new OnContextClicks() {
            @Override
            public void delete(CompletePoema poema, int index) {
                poema.selfDelete();
                deleteItem(index);
            }
        });
    }

    public interface ItemChangedCallback {
        void onCahngeCount(int how);
        void onChangeSize();
    }

    @Override
    void onBindVH(RecyclerView.ViewHolder holder, int position) {
        CompletePoemaViewHolder holderMain = (CompletePoemaViewHolder) holder;

        CompletePoema poema = (CompletePoema) items.get(position).getItem();


        holderMain.textView.setText(poema.getText());
        holderMain.title.setText(poema.getTitle());
        holderMain.authors.setText(poema.getAuthors());

        if(items.get(position).getShowState()== RecycledElement.ShowState.OPEN){
            SparseBooleanArray sb = new SparseBooleanArray(1);
            sb.put(0,false);
            holderMain.textView.setText(poema.getText(), sb ,0);
        } else {
            SparseBooleanArray sb = new SparseBooleanArray(1);
            sb.put(0,true);

            holderMain.textView.setText(poema.getText(), sb ,0);
        }

        if(poema.getFavorite()==null || !poema.getFavorite()){
            holderMain.favoriteItem.setImageResource(R.drawable.ic_favorite_border_white_18dp);
        } else {
            holderMain.favoriteItem.setImageResource(R.drawable.ic_favorite_white_18dp);
        }

        holderMain.favoriteItem.clearAnimation();

    }


    //callback for all methods which update adapter's items
    private class CompletePoemaViewHolder extends RecyclerView.ViewHolder {

         CardView cv;
         ClickableExpandableTextView textView;
            TextView title;
            TextView authors;
            TextView date;
            ImageView menuItem;
            ImageView favoriteItem;

        CompletePoemaViewHolder(View itemView, Clicks clicks, OnContextClicks contextClicks) {
            super(itemView);

            cv = (CardView)itemView.findViewById(R.id.item_view);
            textView = (ClickableExpandableTextView) itemView.findViewById(R.id.expand_text_view);
            title = (TextView) itemView.findViewById(R.id.title_text);
            authors = (TextView) itemView.findViewById(R.id.authors_text);
            menuItem = (ImageView) itemView.findViewById(R.id.menu_item);
            favoriteItem = (ImageView) itemView.findViewById(R.id.favorite_item);



            menuItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showPopup(v, (CompletePoema) getItem(getLayoutPosition()).getItem(), contextClicks, getLayoutPosition()-1);
                }
            });

            favoriteItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    clickChangeStateAnimation(((CompletePoema) getItem(getLayoutPosition()).getItem()).changeFavorite(), (ImageView) v);

                }
            });

            textView.setOnExpandStateChangeListener(new ExpandableTextView.OnExpandStateChangeListener() {
                @Override
                public void onExpandStateChanged(TextView textView, boolean isExpanded) {
                    getItem(getLayoutPosition()).setShowState(isExpanded);
                    if(itemChangedCallback!=null && !isExpanded){
                        itemChangedCallback.onChangeSize();
                    }
                }
            });


        }


    }



    private interface OnContextClicks{
        void delete(CompletePoema poema, int index);
    }

    private interface Clicks{
        void mainClick(CompletePoema poem);
        void onLongClick(CompletePoema poema);
    }



    private void clickChangeStateAnimation(Boolean state, ImageView image){


        Animation hide = new AlphaAnimation(1f,0f);
        hide.setDuration(Settings.INTERFACE_CHANGES_ANIMATION);

        hide.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                image.setVisibility(View.INVISIBLE);
                if(state)
                    image.setImageResource(R.drawable.ic_favorite_white_18dp);
                else image.setImageResource(R.drawable.ic_favorite_border_white_18dp);

                Animation show = new AlphaAnimation(0f, 1f);
                show.setDuration(Settings.INTERFACE_CHANGES_ANIMATION);
                show.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        image.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                image.startAnimation(show);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });



        image.startAnimation(hide);

    }

    private void showPopup(View view, CompletePoema poema, OnContextClicks contextClicks, int index) {
        // pass the imageview id

        PopupMenu popup = new PopupMenu(context, view);
        MenuInflater inflate = popup.getMenuInflater();
        inflate.inflate(R.menu.library_popup, popup.getMenu());
        LogSystem.logThis("Position: " + poema.getId());

        popup.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.copy:
                    ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText(null, poema.getFullText());
                    clipboard.setPrimaryClip(clip);
                    LogSystem.makeToast(fragmentHolder.getActivity(),context.getResources().getString(R.string.copy_in_buffer_success), LogSystem.ToastType.Info, LogSystem.TOAST_POSITION_CENTER);
                    break;
                case R.id.delete:
                    contextClicks.delete(poema, index);
                    if(itemChangedCallback!=null)
                        itemChangedCallback.onCahngeCount(-1);
                    break;
                default:
                    return false;
            }
            return false;
        });
        popup.show();
    }


}

