package belokonalexander.SuppleRecycler;

import android.content.Context;

import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.MenuItem;
import android.view.View;

import StaticHelper.LogSystem;
import belokonalexander.Api.InputQueryDataHolders.SearchInputData;
import belokonalexander.Api.Models.SearchedEntity;

import belokonalexander.SuppleRecycler.DataProviders.Abstracts.ISearchProvider;
import belokonalexander.SuppleRecycler.DataProviders.Abstracts.SearchProvider;

import ru.belokonalexander.poema.Fragments.ListAdapters.SearchAdapter;

import ru.belokonalexander.poema.R;


/**
 * Created by alexander on 10.10.2016.
 */
public class SearchViewRecycler extends AutoRefreshActionRecycler {
    public SearchViewRecycler(Context context) {
        super(context);
    }

    public SearchViewRecycler(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SearchViewRecycler(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }


    EntitySearchView searchView;


    public void init(Toolbar toolbar, boolean localQueries) {

        Class < ? extends SearchedEntity> searchedEntity = ((SearchAdapter) adapter).entityType();

        this.init(toolbar, searchedEntity, EntitySearchView.QueryTypeSettings.REMOTE_QUERY);

    }

    public void init(Toolbar toolbar, Class < ? extends SearchedEntity> searchedEntity, EntitySearchView.QueryTypeSettings queryTypeSettings) {
        super.init(toolbar);

        MenuItem menuItem = toolbar.getMenu().add(getResources().getString(R.string.search));
        menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);


        menuItem.setActionView(new EntitySearchView(getContext()));
        menuItem.setActionView(R.layout.search_panel);
        menuItem.setIcon(R.drawable.ic_search_white_36dp);

        searchView = ((EntitySearchView)menuItem.getActionView());
        searchView.initSearch(searchedEntity, getContext(), queryTypeSettings);
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setSearchTask(new EntitySearchView.SearchTask() {
            @Override
            public void startSearch() {
                if(provider instanceof ISearchProvider) {
                    ((ISearchProvider)provider).updateSearchInputData(new SearchInputData(searchView.searchParams.get(searchView.keyPosition).getName(), searchView.getQuery().toString().trim(), searchView.searchParams.get(searchView.keyPosition).getFullContain(), searchedEntity));
                }
                getData(UpdateMode.REWRITE);
            }

            @Override
            public void startEmpty() {
                if(provider instanceof ISearchProvider){
                    if(((ISearchProvider)provider).onEmptyInputData()){
                        getData(UpdateMode.REWRITE);
                    }
                }
            }
        });
        searchView.getQueryArea().setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus)
                    hideToolbarScrollListener.reset();
            }
        });


    }


}
