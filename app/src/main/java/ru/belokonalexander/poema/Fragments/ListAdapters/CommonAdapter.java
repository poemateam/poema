package ru.belokonalexander.poema.Fragments.ListAdapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by alexander on 26.09.2016.
 */
abstract public class CommonAdapter<T> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    List<T> items;
    Context context;
    Fragment fragmentHolder;

    abstract  RecyclerView.ViewHolder onCreateVH(ViewGroup parent, int viewType);
    abstract  void onBindVH(RecyclerView.ViewHolder holder, int position);



    public CommonAdapter(List<T> items,  Fragment fragmentHolder) {
        this.context = fragmentHolder.getContext();
        this.items = items;
        this.fragmentHolder = fragmentHolder;
    }



    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return onCreateVH(parent, viewType);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        onBindVH(holder, position);
    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    public int getItemDataSize(){
        return items.size();
    }

    public void clear(){
        items.clear();
        notifyDataSetChanged();
    }

    public void deleteItem(int index){
        items.remove(index);
        notifyItemRemoved(index);
    }

    public List<T> getData(){
        return items;
    }

    public void  add(T item){
        items.add(item);
    }

    public  void addFirst(T item){
        items.add(0,item);
    }

    public void update(int index, T item){
        T up = items.get(index);
        up = item;
    }

    public void update(T item){
        int index = items.indexOf(item);
        T updated = items.get(index);
        updated = item;
    }

    //call from holder interface by getLayoutPosition()
    public T getItem(int pos){
        return items.get(pos-1);
    }

    public int getPosition(T item){
        for(int i =0; i < items.size(); i++){
            if(items.get(i).equals(item)){ //fix on equals
                return i;
            }
        }
        return -1;
    }

    public Fragment getFragmentHolder(){
        return  fragmentHolder;
    }

}
