package ru.belokonalexander.poema.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eftimoff.viewpagertransformers.AccordionTransformer;
import com.eftimoff.viewpagertransformers.BackgroundToForegroundTransformer;
import com.eftimoff.viewpagertransformers.CubeInTransformer;
import com.eftimoff.viewpagertransformers.FlipHorizontalTransformer;
import com.eftimoff.viewpagertransformers.ParallaxPageTransformer;
import com.eftimoff.viewpagertransformers.StackTransformer;
import com.eftimoff.viewpagertransformers.TabletTransformer;
import com.eftimoff.viewpagertransformers.ZoomInTransformer;
import com.eftimoff.viewpagertransformers.ZoomOutSlideTransformer;
import com.eftimoff.viewpagertransformers.ZoomOutTranformer;

import StaticHelper.LogSystem;
import belokonalexander.Api.Models.SugarAdditional.SugarPoemaDB;
import belokonalexander.Api.Models.User;
import butterknife.BindView;
import butterknife.ButterKnife;

import ru.belokonalexander.poema.Fragments.FragmentsAdapters.FriendsPageAdapter;
import ru.belokonalexander.poema.Fragments.PageTransformers.DepthPageTransformer;
import ru.belokonalexander.poema.Fragments.PageTransformers.ZoomOutPageTransformer;
import ru.belokonalexander.poema.R;

/**
 * Created by alexander on 13.08.2016.
 */
public class Friends extends Fragment {

    @BindView(R.id.container)
    ViewPager mViewPager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_friends,container,false);
        ButterKnife.bind(this, view);

        FragmentPagerAdapter mSectionsPagerAdapter = new FriendsPageAdapter(getActivity(),getChildFragmentManager());
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        mViewPager.setPageTransformer(true, new StackTransformer());

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                ((FriendsPageAdapter)mSectionsPagerAdapter).onSelected(tab);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });



        return view;
    }


}
