package ru.belokonalexander.poema;


import android.os.PersistableBundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import BorrowedSolutions.ViewPagerCustomDuration;

import StaticHelper.LogSystem;
import belokonalexander.Api.CurrentApi;
import belokonalexander.Api.QueryExecutorExplicit;
import belokonalexander.GlobalShell;
import belokonalexander.SharedAppPrefs;
import butterknife.BindView;
import butterknife.ButterKnife;


import ru.belokonalexander.poema.Fragments.PageTransformers.DepthPageTransformer;
import ru.belokonalexander.poema.Fragments.SignIn;
import ru.belokonalexander.poema.Fragments.SignUp;
import ru.belokonalexander.poema.PageManagers.BackendRunner;

public class Login extends AppCompatActivity {

    private SectionsPagerAdapter mSectionsPagerAdapter;

    Tracker tracker;


    @BindView(R.id.container)
    ViewPagerCustomDuration mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);





        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setPageTransformer(true, new DepthPageTransformer());
        mViewPager.setScrollDurationFactor(2);

        PoemaApplication application = (PoemaApplication) this.getApplication();
        tracker = application.getTracker(PoemaApplication.TrackerName.GLOBAL_TRACKER);


    }

    @Override
    protected void onResume() {
        super.onResume();
        tracker.setScreenName("Screen~" + getClass().getName());
        tracker.send(new HitBuilders.ScreenViewBuilder().build());
        System.gc();
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            switch (position)
            {
                case 0:

                    SignIn signInFragment = new SignIn();
                    signInFragment.setSettings(() -> {GlobalShell.tryCloseKeyboard(Login.this);mViewPager.setCurrentItem(1); });

                    fragment = signInFragment;


                    break;
                case 1:


                    SignUp signUpFragment = new SignUp();
                    signUpFragment.setSettings(() -> {GlobalShell.tryCloseKeyboard(Login.this);mViewPager.setCurrentItem(0); });
                    fragment = signUpFragment;

                    break;
            }

            return fragment;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getResources().getString(R.string.sign_in);
                case 1:
                    return getResources().getString(R.string.sign_up);

            }
            return null;
        }
    }

    public void tryRegisterDevice() {

            QueryExecutorExplicit registerDevice = new QueryExecutorExplicit(GlobalShell.getParentHash(this.getClass()), resultWaiter -> CurrentApi.getInstanse().getApplicationApi().registerDevice(CurrentApi.getInstanse().getCurrentUser().getObjectId(), resultWaiter));

            BackendRunner backendRunner = new BackendRunner(answers -> {
                LogSystem.logThis("Девайс зарегестрирован");
                SharedAppPrefs.getInstance().setCorrectPushRegistration();
            }, messsage -> {
            }, registerDevice);

            backendRunner.run();

    }


}
