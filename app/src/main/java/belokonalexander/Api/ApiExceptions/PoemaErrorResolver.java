package belokonalexander.Api.ApiExceptions;

import android.content.Context;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import StaticHelper.LogSystem;
import StaticHelper.Settings;
import belokonalexander.Api.CurrentApi;
import ru.belokonalexander.poema.PoemaApplication;

/**
 * Created by admin on 26.08.2016.
 */
public abstract  class PoemaErrorResolver {

    protected PoemaErrorResolver next;


    protected Context context;

    public PoemaErrorResolver(Context context)
    {
        this.context = context;
    }

    public PoemaErrorResolver setNext(PoemaErrorResolver n)
    {
        next = n;
        return n;

    }

    public boolean tryResolve(PoemaApiError error)
    {



        Tracker t = ((PoemaApplication)context.getApplicationContext()).getTracker(PoemaApplication.TrackerName.APP_TRACKER);
        Class errorType = error.getItemType();
        String type = "unknown";
        if(errorType!=null)
            type = errorType.getName();

        t.send(new HitBuilders.ExceptionBuilder()
                .setDescription(error.getMessage() + ":" + String.valueOf(error.getCode()) + "(t: " + type + ", s: " + error.desc + ")")
                .setFatal(false)
                .build());


        if(error.getResolvedStatus())
            return false;


        boolean result = false;
        result = resolve(error);
        if(!result){
            if(next!=null)
               result = next.tryResolve(error);
        }

        return result;
    }

    protected abstract Boolean resolve(PoemaApiError error);
    protected abstract void resolveAction();




    public static int convertInAppErrorCode(PoemaApiError e)
    {

        if(e != null)
        {
            if(CurrentApi.getInstanse().getApplicationApi().convertInAppError(((PoemaApiError) e).getCode())>=0){
                return CurrentApi.getInstanse().getApplicationApi().convertInAppError(((PoemaApiError) e).getCode());
            } else
                return CurrentApi.getInstanse().getApplicationApi().convertInAppError(e.getMessage());
        }

        return -1;
    }

}

