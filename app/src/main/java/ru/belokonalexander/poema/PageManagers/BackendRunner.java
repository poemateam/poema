package ru.belokonalexander.poema.PageManagers;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import StaticHelper.LogSystem;
import belokonalexander.Api.ApiTask;
import belokonalexander.Api.QueryExecutorExplicit;
import belokonalexander.RxCommonElements.AnswerApiRx;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by alexander on 16.11.2016.
 */
public class BackendRunner {
    //FOR ASYNC CALLING ApiTask[] implements
    // IF WE GET ERROR -> CALL onFailure and interrupt ALL task

    private Observable<Object> provider;
    private Subscriber<Object> receiver;

    private ApiTask[] tasks;
    private List<Object> answers = new ArrayList<>();


    public BackendRunner(SuccessAnswer successAnswer, FailureAnswer failureAnswer, ApiTask... tasks) {
        this.tasks = tasks;
        this.successAnswer = successAnswer;
        this.failureAnswer = failureAnswer;
    }



    public void run(){



        if(tasks == null)
            throw new NullPointerException(); //need to set tasks

        refreshQueryState();
        answers.clear();



        provider = Observable.from(tasks)
                .observeOn(Schedulers.newThread())
                .flatMap(queryTask -> Observable.create(s -> {
                    s.onNext(queryTask.execute());
                    s.onCompleted();
                }))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnCompleted(() -> {

                    successAnswer.onSuccessCompleted(answers);
                });

        receiver = AnswerApiRx.getOnlyOnNext(new AnswerApiRx.CustomizeActionAdditional() {
            @Override
            public void onSuccess(Object res) {
                answers.add(res);
            }

            @Override
            public void onFailure(String message) {

                failureAnswer.onFailureCompleted(message);
                throw new NullPointerException();
            }

            @Override
            public void onCanceled() {
                //if returns null
                failureAnswer.onFailureCompleted("canceled");
                throw new NullPointerException();
            }
        });

        provider.subscribe(receiver);

    };

    private void refreshQueryState(){
            //recreate queries (wee need to clear state of each query)
            for (int i=0; i< tasks.length ; i++){
                tasks[i].refresh();
            }


    }

    private SuccessAnswer successAnswer;

    public interface SuccessAnswer{
        void onSuccessCompleted(List<Object> answers);          // void onSuccessFinish(); //runs after all queries successfully finished and provide results
    }

    private FailureAnswer failureAnswer;

    public interface FailureAnswer{
        void onFailureCompleted(String messsage);          // void onSuccessFinish(); //runs after all queries successfully finished and provide results
    }

}
