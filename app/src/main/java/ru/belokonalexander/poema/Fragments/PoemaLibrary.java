package ru.belokonalexander.poema.Fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.liucanwen.app.headerfooterrecyclerview.HeaderAndFooterRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

import Helper.RecycledElement;
import belokonalexander.Api.Models.CompletePoema;
import belokonalexander.Api.Models.SugarAdditional.SugarPoemaDB;
import belokonalexander.SuppleRecycler.ActionRecycler;
import belokonalexander.SuppleRecycler.DataProviders.Abstracts.GetCompletePoemsProvider;
import belokonalexander.SuppleRecycler.EntitySearchView;
import belokonalexander.SuppleRecycler.SearchViewRecycler;
import butterknife.BindView;
import butterknife.ButterKnife;
import ru.belokonalexander.poema.Fragments.ListAdapters.CompletePoemsAdapter;
import ru.belokonalexander.poema.R;

import static StaticHelper.Settings.SHOW_ITEMS;
import static StaticHelper.Settings.SHOW_ITEMS_ANIMATE;

/**
 * Created by alexander on 13.08.2016.
 */
public class PoemaLibrary extends NavigationFragment{

    @BindView(R.id.app_toolbar)
    Toolbar toolbar;

    @BindView(R.id.my_poems)
    SearchViewRecycler recyclerMyPoems;

    int countOfElements = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if(rootView!=null)
            return rootView;

        View view = inflater.inflate(R.layout.fragment_poema_library,container,false);
        ButterKnife.bind(this, view);
        fillToolbar(getString(R.string.poema_library));



        List<RecycledElement> poems = new ArrayList<>();

        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        recyclerMyPoems.setLayoutManager(llm);

        HeaderAndFooterRecyclerViewAdapter adapterWithHeader = new HeaderAndFooterRecyclerViewAdapter(new CompletePoemsAdapter(poems, this, new CompletePoemsAdapter.ItemChangedCallback() {
            @Override
            public void onCahngeCount(int how) {
                countOfElements+=how;
                String addition = "";
                if(countOfElements>0){
                    addition = " (" + countOfElements + ")";
                }
                getToolbar().setTitle(getString(R.string.poema_library) + addition);
                recyclerMyPoems.resetTollbarState();
            }

            @Override
            public void onChangeSize() {
                recyclerMyPoems.resetTollbarState();
            }
        }));



        recyclerMyPoems.setAdapter(adapterWithHeader);
        recyclerMyPoems.setToolbarIndicator(1);
        recyclerMyPoems.init(toolbar,CompletePoema.class, EntitySearchView.QueryTypeSettings.LOCAL_QUERY);
        recyclerMyPoems.setProvider(new GetCompletePoemsProvider());
        recyclerMyPoems.getData(ActionRecycler.UpdateMode.INITIAL);


        rootView = view;

        ButterKnife.apply(recyclerMyPoems, SHOW_ITEMS_ANIMATE);


        fillToolbarCount();

        return view;
    }

    private void fillToolbarCount() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                countOfElements = (int) SugarPoemaDB.getInstance().getCompletePoemsSize();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                String addition = "";
                if(countOfElements>0){
                    addition = " (" + countOfElements + ")";
                }
                getToolbar().setTitle(getString(R.string.poema_library) + addition);
            }
        }.execute();


    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }


}
