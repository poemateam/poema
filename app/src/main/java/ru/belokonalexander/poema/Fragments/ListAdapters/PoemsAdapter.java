package ru.belokonalexander.poema.Fragments.ListAdapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.greenrobot.eventbus.EventBus;

import java.util.List;
import java.util.zip.Inflater;

import StaticHelper.LogSystem;
import StaticHelper.Settings;
import belokonalexander.Api.CurrentApi;
import belokonalexander.Api.Models.Poema;
import belokonalexander.Api.Models.User;
import belokonalexander.Api.QueryExecutorExplicit;
import belokonalexander.GlobalShell;
import de.hdodenhof.circleimageview.CircleImageView;
import ru.belokonalexander.poema.CustomViews.GlideCircleTransformation;
import ru.belokonalexander.poema.Fragments.NavigationFragment;
import ru.belokonalexander.poema.Fragments.PoemsMessages;
import ru.belokonalexander.poema.Fragments.Profile;
import ru.belokonalexander.poema.PageManagers.BackendRunner;
import ru.belokonalexander.poema.R;

/**
 * Created by alexander on 26.09.2016.
 */
public class PoemsAdapter extends CommonAdapter<Poema>  {


    public PoemsAdapter(List items, Fragment fragmentHolder) {
        super(items, fragmentHolder);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    RecyclerView.ViewHolder onCreateVH(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_poem,parent,false);

        return new PoemsViewHolder(v, new Clicks() {
            @Override
            public void mainClick(Poema poem) {
                if(poem.getObjectId()!=null) {

                    PoemsMessages openPoema = new PoemsMessages();
                    Bundle bundle = new Bundle();

                    EventBus.getDefault().postSticky(poem);

                    openPoema.setArguments(bundle);
                    ((NavigationFragment)fragmentHolder).openNewFragment(openPoema);
                }
            }

            @Override
            public void onLingClick(Poema poema) {
                QueryExecutorExplicit deletePoem = new QueryExecutorExplicit(GlobalShell.getParentHash(this.getClass(),poema.getObjectId()), resultWaiter -> CurrentApi.getInstanse().getApplicationApi().deletePoems(poema, resultWaiter));


                BackendRunner backendRunner = new BackendRunner(answers -> {
                    deleteItem(getPosition(poema));
                },
                        messsage -> {
                            LogSystem.logThis(" ERROR: " + messsage);

                            //TODO
                        }, deletePoem);

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle(R.string.delete_poema);
                builder.setMessage(R.string.are_you_shure);
                builder.setPositiveButton(R.string.yes, (dialog, which) -> {
                    backendRunner.run();
                    dialog.dismiss();
                });

                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }

    @Override
    void onBindVH(RecyclerView.ViewHolder holder, int position) {
        PoemsViewHolder holderMain = (PoemsViewHolder) holder;

        Poema poema = items.get(position);

        holderMain.poemTitle.setText(poema.getTitle());

        holderMain.usersContainer.removeAllViews();


        int fontSize = 0;
        String msg = "";
        String date = "";
        int gravity = Gravity.CENTER_VERTICAL;
        int iconVisibility = View.INVISIBLE;


        if(poema.getLastMessage()!=null){
            fontSize = (int) (context.getResources().getDimension(R.dimen.text_size_tiny_normal) / context.getResources().getDisplayMetrics().density);
            msg = poema.getLastMessage().getMessage();
            date = Settings.getDate(poema.getLastMessage().getCreated(), Settings.DateFormat.SmartChat);


            if(poema.getLastMessage().getActual() && !poema.getLastMessage().isMyMessage()){ //TODO
                iconVisibility = View.VISIBLE;
            }
        } else {
            fontSize = (int) (context.getResources().getDimension(R.dimen.text_size_small) / context.getResources().getDisplayMetrics().density);
            msg = context.getResources().getString(R.string.poema_has_no_messages);
            gravity = Gravity.RIGHT|Gravity.BOTTOM;

        }

        holderMain.poemLastMessage.setText(msg);
        holderMain.poemLastMessageDate.setText(date);
        holderMain.poemLastMessageDate.setGravity(gravity);
        holderMain.poemLastMessage.setTextSize(fontSize);
        holderMain.poemLastMessage.setGravity(gravity);
        holderMain.eventIcon.setVisibility(iconVisibility);


        LayoutInflater inflater = fragmentHolder.getActivity().getLayoutInflater();
        CircleImageView portret = (CircleImageView) inflater.inflate(R.layout.circle_image, null);
        portret.setBorderOverlay(true);
        portret.setBorderWidth(GlobalShell.dpToPixels(1));

        User orderUser = (poema.getLastMessage()!=null) ? poema.getMember(poema.getLastMessage().getOwner()) : poema.getAnotherUser();


            portret.setBorderColor(ContextCompat.getColor(context,R.color.image_border_color));


            portret.setOnClickListener(v -> {
                Profile openFriendProfile = new Profile();
                Bundle bundle = new Bundle();
                bundle.putString("UserID", orderUser.getObjectId());
                bundle.putString("Name", orderUser.getName());
                openFriendProfile.setArguments(bundle);
                ((NavigationFragment)getFragmentHolder()).openNewFragment(openFriendProfile);
            });
            holderMain.usersContainer.addView(portret);

            Settings.setCommonOptions(Glide.with(context).load(orderUser.getSmallImage()).override(GlobalShell.dpToPixels(56), GlobalShell.dpToPixels(56)).error(Settings.getDefaultImage(context,orderUser))).into(portret);


    }


    //callback for all methods which update adapter's items
    private class PoemsViewHolder extends RecyclerView.ViewHolder {

         CardView cv;
         TextView poemTitle;
         LinearLayout usersContainer;
         TextView poemLastMessage;
         TextView poemLastMessageDate;
         View eventIcon;

         PoemsViewHolder(View itemView, Clicks clicks) {
            super(itemView);

            cv = (CardView)itemView.findViewById(R.id.item_view);
            poemTitle = (TextView) itemView.findViewById(R.id.poem_title);
            usersContainer = (LinearLayout) itemView.findViewById(R.id.users_container);
            poemLastMessage = (TextView) itemView.findViewById(R.id.poem_last_message);
            poemLastMessageDate = (TextView) itemView.findViewById(R.id.poem_last_message_date);
            eventIcon = (ImageView) itemView.findViewById(R.id.poema_event_icon);


            cv.setOnClickListener(view -> clicks.mainClick(getItem(getLayoutPosition())));
            cv.setOnLongClickListener(v -> {
                clicks.onLingClick(getItem(getLayoutPosition()));
                return false;
            });

        }
    }

    private interface Clicks{
        void mainClick(Poema poem);
        void onLingClick(Poema poema);
    }
}

