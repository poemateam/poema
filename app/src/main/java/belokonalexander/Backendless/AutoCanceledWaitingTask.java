package belokonalexander.Backendless;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import com.backendless.async.callback.AsyncCallback;
import com.backendless.cache.CacheService;
import com.backendless.exceptions.BackendlessFault;
import com.orm.SugarRecord;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import StaticHelper.LogSystem;
import StaticHelper.Settings;
import belokonalexander.Api.ApiExceptions.PoemaApiError;
import belokonalexander.Api.CurrentApi;
import belokonalexander.Api.InputQueryDataHolders.MassiveQueryInputData;
import belokonalexander.Api.Models.ApiEntity;
import belokonalexander.Api.Models.QueryLog;
import belokonalexander.Api.Models.SugarAdditional.SugarPersistence;
import belokonalexander.Api.Models.SugarAdditional.SugarPoemaDB;
import belokonalexander.Api.ResultWaiter;
import belokonalexander.SharedAppPrefs;
import ru.belokonalexander.poema.Services.CachingService;

/**
 * Created by alexander on 11.10.2016.
 */
abstract public class AutoCanceledWaitingTask<T> implements AsyncCallback<T> {

    ResultWaiter waiter;
    boolean dbCaching = true;
    boolean mainProvider = false;
    boolean ignoreErrorsResolve = false;
    private Context context;

    public AutoCanceledWaitingTask(ResultWaiter waiter, Context context) {
        this.waiter = waiter;
        this.context = context;
    }



    public AutoCanceledWaitingTask(ResultWaiter waiter, Context context, boolean ignoreErrorsResolve) {
        this(waiter,context);
        this.ignoreErrorsResolve = ignoreErrorsResolve;
    }



    //callback from success api query
    //ATTENTION handleResponse calling in MAIN THREAD
    @Override
    public void handleResponse(T o) {
        if(!waiter.isInterrupted()) {

            Object response = onCorrectResponse(o);

            waiter.setResult(response);

            if(waiter.isDeleteCommand() || waiter.isEditCommand() || waiter.isEmptyCommand())
                return;

            addDescriptionToData(response,waiter);

            try {
                deleteRelatedLogs(waiter);
            } catch (Exception e) {
                return;
            }

            if(SharedAppPrefs.getInstance().getUsingCache() && dbCaching) {
                Intent intent = new Intent(context, CachingService.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("data", (Serializable) response);
                intent.putExtras(bundle);
                context.startService(intent);
            }

        }
    }

    private void deleteRelatedLogs(ResultWaiter waiter) {

        //перезаписываем те же самые
        if(waiter.getResult() instanceof List && ((List)waiter.getResult()).isEmpty()) {
            MassiveQueryInputData data = waiter.getMassiveQueryInputData();
            if(data!=null)
                SugarPoemaDB.getInstance().deleteFromLogs(waiter.getRequestDescription(), data.getOffset());
            else SugarPoemaDB.getInstance().deleteFromLogs(waiter.getRequestDescription(), null);
        }

        //удаляем противоположные
        if(waiter.getDeletedCommand()==null || waiter.getDeletedCommand().isEmpty())
            return;

        StringBuilder whereClause = new StringBuilder();
        int i =0;
        for(String command : waiter.getDeletedCommand()){

            if(i==0) whereClause.append("query = ?");
            else {
                whereClause.append(" OR query = ?");
            }

            i++;
        }
        String[] array = new  String[waiter.getDeletedCommand().size()];

        SugarRecord.deleteAll(QueryLog.class,whereClause.toString(), waiter.getDeletedCommand().toArray(array));

    }

    // for fill log data
    private void addDescriptionToData(Object response, ResultWaiter waiter) {

        if(response instanceof List && ((List) response).size()>0 && ((List) response).get(0) instanceof ApiEntity){

            List<ApiEntity> list = (List) response;
            Object firstItem = ((List) response).get(0);

            Class type = firstItem.getClass();
            LogSystem.logThis("Class name: " + type);

            int i = 0;
            MassiveQueryInputData data = waiter.getMassiveQueryInputData();

            for (ApiEntity item : list){
                if(data!=null) {
                    item.setQueryLog(new QueryLog(item.getObjectId(), type, waiter.getRequestDescription(), data.getOffset(),i));
                    i++;
                } else {
                    item.setQueryLog(new QueryLog(item.getObjectId(), type, waiter.getRequestDescription()));
                }
            }
        } else if(response instanceof ApiEntity){
            ApiEntity item = (ApiEntity) response;
            item.setQueryLog(new QueryLog(item.getObjectId(), response.getClass(), waiter.getRequestDescription()));
        }

    }


    //callback from failed api query
    @Override
    public void handleFault(BackendlessFault backendlessFault) {

        LogSystem.logThis("Неудачный ответ: " + backendlessFault + " code: " + backendlessFault.getCode());

        if(!waiter.isInterrupted()) {
            waiter.setResult(new PoemaApiError(backendlessFault));
            if (!ignoreErrorsResolve)
                CurrentApi.getInstanse().getErrorResolver().tryResolve(new PoemaApiError(backendlessFault,waiter.getClassItem(), waiter.getService()));
        }
    }

    abstract Object onCorrectResponse(T o);



}
