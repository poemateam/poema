package belokonalexander.RxCommonElements.Form;

/**
 * Created by admin on 31.07.2016.
 */
public interface FormAction {
     Object actionStart();    // main action
     void succeedAction(Object result);    //action after success
     void failedAction(Exception e); //action after failing
}
