package ru.belokonalexander.poema.CustomViews.Buttons;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.gc.materialdesign.utils.Utils;

import belokonalexander.GlobalShell;


/**
 * Created by Alexander on 17.02.2017.
 */

public class ButtonFlatPoema extends ButtonPoema {
    TextView textButton;

    public ButtonFlatPoema(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    protected void setDefaultProperties() {
        this.minHeight = 36;
        this.minWidth = 88;
        this.rippleSize = 3;
        this.setMinimumHeight(Utils.dpToPx((float)this.minHeight, this.getResources()));
        this.setMinimumWidth(Utils.dpToPx((float)this.minWidth, this.getResources()));
        this.setBackgroundResource(com.gc.materialdesign.R.drawable.background_transparent);
    }

    protected void setAttributes(AttributeSet attrs) {
        String text = null;
        int textResource = attrs.getAttributeResourceValue("http://schemas.android.com/apk/res/android", "text", -1);
        if(textResource != -1) {
            text = this.getResources().getString(textResource);
        } else {
            text = attrs.getAttributeValue("http://schemas.android.com/apk/res/android", "text");
        }

        if(text != null) {
            this.textButton = new TextView(this.getContext());
            this.textButton.setText(text.toUpperCase());
            this.textButton.setTextColor(this.backgroundColor);
            this.textButton.setTypeface((Typeface)null, 1);

            this.textButton.setPadding(GlobalShell.dpToPixels(14),0,GlobalShell.dpToPixels(14),0);


            LayoutParams bacgroundColor = new LayoutParams(-2, -2);
            bacgroundColor.addRule(13, -1);
            this.textButton.setLayoutParams(bacgroundColor);
            this.addView(this.textButton);
        }

        int bacgroundColor1 = attrs.getAttributeResourceValue("http://schemas.android.com/apk/res/android", "background", -1);
        if(bacgroundColor1 != -1) {
            this.setBackgroundColor(this.getResources().getColor(bacgroundColor1));
        } else {
            this.background = attrs.getAttributeIntValue("http://schemas.android.com/apk/res/android", "background", -1);
            if(this.background != -1) {
                this.setBackgroundColor(this.background);
            }
        }

    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if(this.x != -1.0F) {
            Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setColor(this.makePressColor());
            canvas.drawCircle(this.x, this.y, this.radius, paint);
            if(this.radius > (float)(this.getHeight() / this.rippleSize)) {
                this.radius += this.rippleSpeed;
            }

            if(this.radius >= (float)this.getWidth()) {
                this.x = -1.0F;
                this.y = -1.0F;
                this.radius = (float)(this.getHeight() / this.rippleSize);
                if(this.onClickListener != null && this.clickAfterRipple) {
                    this.onClickListener.onClick(this);
                }
            }

            this.invalidate();
        }

    }

    protected int makePressColor() {
        return Color.parseColor("#88DDDDDD");
    }

    public void setText(String text) {
        this.textButton.setText(text.toUpperCase());
    }

    public void setBackgroundColor(int color) {
        this.backgroundColor = color;
        if(this.isEnabled()) {
            this.beforeBackground = this.backgroundColor;
        }

        this.textButton.setTextColor(color);
    }

    public TextView getTextView() {
        return this.textButton;
    }

    public String getText() {
        return this.textButton.getText().toString();
    }
}
