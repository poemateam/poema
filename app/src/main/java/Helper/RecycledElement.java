package Helper;

import belokonalexander.Api.Models.CompletePoema;

/**
 * Created by Alexander on 02.03.2017.
 */

public class RecycledElement {

    public enum ShowState {
        OPEN, CLOSED;
    }

    public enum SelectState{
        SELECTED, NOT_SELECTED;
    }

    Object item;

    public RecycledElement(Object item) {
        this.item = item;
    }

    ShowState showState = ShowState.CLOSED;
    SelectState selectState = SelectState.NOT_SELECTED;

    public ShowState getShowState() {
        return showState;
    }

    public SelectState getSelectState() {
        return selectState;
    }

    public void setShowState(boolean isOpened) {
       if(isOpened){
           this.showState = ShowState.OPEN;
       } else
           this.showState = ShowState.CLOSED;
    }

    public void setSelectState(SelectState selectState) {
        this.selectState = selectState;
    }

    public Object getItem() {
        return item;
    }
}
