package ru.belokonalexander.poema.Events;

import java.util.List;

import belokonalexander.Api.Models.PoemaMessage;

/**
 * Created by Alexander on 12.02.2017.
 */

public class MessageStatusEvent {

    PoemaMessage poemaMessage;

    public MessageStatusEvent(PoemaMessage  poemaMessage) {
        this.poemaMessage = poemaMessage;
    }

    public  PoemaMessage  getPoemaMessage() {
        return poemaMessage;
    }

    public void setPoemaMessage(PoemaMessage  poemaMessage) {
        this.poemaMessage = poemaMessage;
    }
}
