package ru.belokonalexander.poema;

import android.app.ActivityManager;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.backendless.Backendless;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;
import java.util.List;

import Helper.MaterialTutorial.TutorialItem;
import Helper.MaterialTutorial.tutorial.MaterialTutorialActivity;
import StaticHelper.LogSystem;
import belokonalexander.Api.CurrentApi;
import belokonalexander.Api.Models.SugarAdditional.SugarPoemaDB;
import belokonalexander.Api.Models.User;
import belokonalexander.Api.QueryExecutorExplicit;
import belokonalexander.GlobalShell;
import belokonalexander.SharedAppPrefs;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SplashScreen extends AppCompatActivity {

    public static final int REQUEST_CODE_INTRO = 101;

    @BindView(R.id.initSteps)
    TextView initSteps;

    @OnClick(R.id.initSteps)
    public void click()
    {
        LogSystem.logThis("UUU");
    }

    Tracker tracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash_screen);
        ButterKnife.bind(this);

        CurrentApi.getInstanse().getApplicationApi().stopMessageingService();

        PoemaApplication application = (PoemaApplication) this.getApplication();
        tracker = application.getTracker(PoemaApplication.TrackerName.GLOBAL_TRACKER);

        Thread aut = new Thread(() -> {


            String userToken = CurrentApi.getInstanse().getApplicationApi().getSavedToken();
            if (userToken != null && !userToken.equals("")) {

                LogSystem.logThis("Проверяю пользователя");

                if(CurrentApi.getInstanse().getApplicationApi().isSessionValid() || !GlobalShell.internetIsOn(getBaseContext()))
                {
                    LogSystem.logThis("Сессия валидна");

                    CurrentApi.getInstanse().setUserHash(userToken);
                    User currentUser = CurrentApi.getInstanse().getApplicationApi().getCurrentUser();

                    LogSystem.logThis(" CURRENT USER: " + currentUser);

                    if(currentUser!=null) {
                        CurrentApi.getInstanse().setCurrentUser(currentUser);
                        GlobalShell.openActivityWithCloseThis(getBaseContext(), MainActivity.class, getIntent());
                    } else {
                        LogSystem.logThis("Пользователь не найден");
                        startApplication();
                    }
                }  else {
                    LogSystem.logThis("Сессия не валидна");
                    startApplication();
                }
            } else {
                LogSystem.logThis("Токен не сохранен");
                startApplication();
            }
        });

        aut.start();


      /*  // Возвращаем лимит памяти в мегабайтах
        int memoryClass = ((ActivityManager) getSystemService(Context.ACTIVITY_SERVICE))
                .getMemoryClass();



        LogSystem.logThis( " max: " + memoryClass );*/

    }



    //User is not logged -> Start Intro or Login activity
    private void startApplication() {

        SugarPoemaDB.getInstance().clearDatabase();
        SharedAppPrefs.getInstance().clearUserData();

        if (SharedAppPrefs.getInstance()    .shouldShowIntro()) // open tutorial
        {
            openIntroActivity(getTutorialItems(getBaseContext()), MaterialTutorialActivity.MATERIAL_TUTORIAL_ARG_TUTORIAL_ITEMS);
        } else {
            GlobalShell.openActivityWithCloseThis(getBaseContext(), Login.class);
        }

    }


    private ArrayList<TutorialItem> getTutorialItems(Context context) {
        TutorialItem tutorialItem1 = new TutorialItem(context.getString(R.string.slide_title_1), context.getString(R.string.slide_subtitle_1),
                R.color.slide_1, R.drawable.intro_1);
        TutorialItem tutorialItem2 = new TutorialItem(context.getString(R.string.slide_title_2), context.getString(R.string.slide_subtitle_2),
                R.color.slide_2,  R.drawable.intro_2);
        TutorialItem tutorialItem3 = new TutorialItem(context.getString(R.string.slide_title_3), context.getString(R.string.slide_subtitle_3),
                R.color.slide_3,  R.drawable.intro_3);
        ArrayList<TutorialItem> tutorialItems = new ArrayList<>();
        tutorialItems.add(tutorialItem1);
        tutorialItems.add(tutorialItem2);
        tutorialItems.add(tutorialItem3);

        return tutorialItems;
    }


    public void openIntroActivity(ArrayList< ? extends Parcelable> params, String name)
    {
        Intent mainAct = new Intent(this, Intro.class);
        mainAct.putParcelableArrayListExtra(name,params);
        startActivityForResult(mainAct, REQUEST_CODE_INTRO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==REQUEST_CODE_INTRO)
        {
            if(resultCode==RESULT_OK)
            {
                GlobalShell.openActivityWithCloseThis(getBaseContext(),Login.class);
            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        tracker.setScreenName("Screen~" + getClass().getName());
        tracker.send(new HitBuilders.ScreenViewBuilder().build());
    }
}