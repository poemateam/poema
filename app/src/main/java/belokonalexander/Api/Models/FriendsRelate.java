package belokonalexander.Api.Models;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import java.util.Date;
import java.util.Map;

import belokonalexander.Api.CurrentApi;
import belokonalexander.Api.Models.Annotations.Field;
import belokonalexander.Api.Models.Annotations.Table;
import belokonalexander.Api.Models.SugarAdditional.MyFriendsRelate;
import ru.belokonalexander.poema.R;

/**
 * Created by alexander on 22.09.2016.
 */
@Table(name = "FriendsRelate")
public class FriendsRelate extends ApiEntity{

    @Field
    String objectId;

    @Field
    Integer Status;

    @Field
    String usersCompositeKey;

    @Field
    User UserOne;

    @Field
    User UserTwo;

    @Field
    Date created;

    @Field
    Date updated;


    public void updateCompositeKey(String usetOneId, String usetTwoId){
        try {
            usersCompositeKey = String.valueOf((CurrentApi.getInstanse().getLongHashOfItem(usetOneId)+ CurrentApi.getInstanse().getLongHashOfItem(usetTwoId))%Long.MAX_VALUE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateCompositeKey(){
        try {
            usersCompositeKey = String.valueOf((CurrentApi.getInstanse().getLongHashOfItem(UserOne.getObjectId())+ CurrentApi.getInstanse().getLongHashOfItem(UserTwo.getObjectId()))%Long.MAX_VALUE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public FriendsRelate(String objectId, Integer status, User userOne, User userTwo) {
        this.objectId = objectId;
        Status = status;
        UserOne = userOne;
        UserTwo = userTwo;
        updateCompositeKey();
    }

    public enum RelateStates{
        NONE, I_SEND_FRIENDSHIP, I_GOT_FRIENDSHIP, FRIENDS
    }

    public FriendsRelate(){}

    @Override
    public String toString() {
        return getUserOne().getName() + " / " + getUserTwo().getName() + " st: " + getStatus();
    }

    public String getObjectId() {
        return objectId;
    }

    @Override
    public void afterInit() {

    }

    public FriendsRelate setObjectId(String objectId) {
        this.objectId = objectId;
        return this;
    }

    public FriendsRelate(Map<String, Object> input){
        initFromMap(input);
    }

    public Integer getStatus() {
        return Status;
    }

    public FriendsRelate setStatus(Integer status) {
        Status = status;
        return this;
    }

    public User getUserOne() {
        return UserOne;
    }


    public User getUserTwo() {
        return UserTwo;
    }

    public FriendsRelate setUserOne(User userOne) {
        UserOne = userOne;
        return this;
    }

    public FriendsRelate setUserTwo(User userTwo) {
        UserTwo = userTwo;
        return this;
    }


    public static void fillStatusInImage(Context context, RelateStates friendsRelate, ImageView link){

        Drawable result = null;

        switch (friendsRelate) {
            case FRIENDS:
                result = context.getResources().getDrawable(R.drawable.ic_person_white_18dp);
                link.setImageDrawable(result);
                link.setColorFilter(context.getResources().getColor(R.color.friend_color), PorterDuff.Mode.MULTIPLY);
                break;
            case NONE:
                result = context.getResources().getDrawable(R.drawable.ic_person_white_18dp);
                link.setImageDrawable(result);
                link.setColorFilter(context.getResources().getColor(R.color.not_friend_color), PorterDuff.Mode.MULTIPLY);
                break;
            case I_GOT_FRIENDSHIP:
                result = context.getResources().getDrawable(R.drawable.ic_person_add_white_18dp);
                link.setImageDrawable(result);
                link.setColorFilter(context.getResources().getColor(R.color.color_add_friend), PorterDuff.Mode.MULTIPLY);
                break;
            case I_SEND_FRIENDSHIP:
                result = context.getResources().getDrawable(R.drawable.ic_send_white_18dp);
                link.setImageDrawable(result);
                link.setColorFilter(context.getResources().getColor(R.color.not_friend_color), PorterDuff.Mode.MULTIPLY);
                break;
        }



    }

    public User getFriendFromRelate(String invokerId){

        if(getUserOne()!= null && !getUserOne().getObjectId().equals(invokerId)){
            return getUserOne();
        } else if(getUserTwo()!= null && !getUserTwo().getObjectId().equals(invokerId)){
            return getUserTwo();
        } else return null;

    };

    Boolean myInitiation = null;

    public boolean isMyInitiation(){

        User another = getFriendFromRelate(CurrentApi.getInstanse().getCurrentUser().getObjectId());

        if(another==null)
            return myInitiation;

        return another.getObjectId().equals(getUserTwo().getObjectId());
    };


    public FriendsRelate(MyFriendsRelate myFriendsRelate){
        objectId = myFriendsRelate.getObjectId();
        Status = myFriendsRelate.getStatus();
        myInitiation = myFriendsRelate.isMyInitiation();
        updateCompositeKey();
    };


}
