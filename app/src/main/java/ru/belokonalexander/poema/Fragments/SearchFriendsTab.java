package ru.belokonalexander.poema.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.liucanwen.app.headerfooterrecyclerview.HeaderAndFooterRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

import belokonalexander.Api.InputQueryDataHolders.SearchInputData;
import belokonalexander.Api.Models.User;
import belokonalexander.SuppleRecycler.ActionRecycler;
import belokonalexander.SuppleRecycler.DataProviders.Abstracts.SearchProvider;
import belokonalexander.SuppleRecycler.SearchViewRecycler;
import butterknife.BindView;
import butterknife.ButterKnife;
import ru.belokonalexander.poema.Fragments.ListAdapters.SearchAdapter;
import ru.belokonalexander.poema.R;

/**
 * Created by alexander on 13.08.2016.
 */
public class SearchFriendsTab extends NavigationFragment
{


    @BindView(R.id.recycler_search_freinds)
    SearchViewRecycler recyclerSearchFriends;

    @BindView(R.id.app_toolbar)
    Toolbar toolbar;


    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;


    SearchAdapter adapter;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if(rootView!=null)
            return rootView;

        View view = inflater.inflate(R.layout.fragment_search_friends,container,false);
        ButterKnife.bind(this, view);
        fillToolbar(getString(R.string.search_friends));

        List<User> userList = new ArrayList<>();

        adapter = new SearchAdapter<>(userList,new User(),this);

        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        recyclerSearchFriends.setLayoutManager(llm);
        HeaderAndFooterRecyclerViewAdapter adapterWithHeader = new HeaderAndFooterRecyclerViewAdapter(adapter);
        recyclerSearchFriends.setAdapter(adapterWithHeader);
        recyclerSearchFriends.setToolbarIndicator(1);
        recyclerSearchFriends.init(toolbar, false);
        recyclerSearchFriends.setProvider(new SearchProvider(new SearchInputData("name","",false, User.class)));



        toolbar.setTitle(getString(R.string.search_friends));

        rootView = view;

        return view;
    }

    boolean wasSelected = false;
    public void onSelected(){
        if(!wasSelected){
            recyclerSearchFriends.getData(ActionRecycler.UpdateMode.INITIAL);
            wasSelected = true;
        }
    };

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }



}
