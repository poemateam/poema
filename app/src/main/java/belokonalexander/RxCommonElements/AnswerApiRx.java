package belokonalexander.RxCommonElements;

import com.backendless.exceptions.BackendlessException;

import StaticHelper.LogSystem;
import belokonalexander.Api.ApiExceptions.PoemaApiError;
import belokonalexander.Api.CurrentApi;
import belokonalexander.Api.Models.User;
import belokonalexander.Api.QueryAnalyzer;
import belokonalexander.Api.QueryExecutor;
import rx.Subscriber;

/**
 * Created by alexander on 24.08.2016.
 */
public class AnswerApiRx {

    public static void customizeRxSubsribeAction(Object answer, CustomizeAction customizeAction){

        if(answer==null) //работа потока была отменена
        {
            customizeAction.onCanceled();
        }
        else if(answer instanceof Exception)
        {
            if(answer instanceof PoemaApiError) {
                customizeAction.onFailure(((PoemaApiError) answer).getMessage());
            }
            else
                customizeAction.onFailure(((Exception)answer).getMessage());
        }
        else customizeAction.onSuccess();
    }

    public static void customizeRxSubsribeActionAdditional(Object answer, CustomizeActionAdditional customizeAction){

        if(answer==null) //работа потока была отменена
        {
            customizeAction.onCanceled();
        }
        else if(answer instanceof Exception)
        {
            if(answer instanceof PoemaApiError) {
                customizeAction.onFailure(((PoemaApiError) answer).getMessage());
            }
             else
                customizeAction.onFailure(((Exception)answer).getMessage());
        }
        else customizeAction.onSuccess(answer);
    }

    public interface CustomizeAction
    {
        void onSuccess();
        void onFailure(String message);
        void onCanceled();
    }

    public interface CustomizeActionAdditional
    {
        void onSuccess(Object res);
        void onFailure(String message);
        void onCanceled();
    }


    public static Subscriber<Object> getOnlyOnNext(CustomizeActionAdditional customizeAction){

        return new Subscriber<Object>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(Object res) {

                customizeRxSubsribeActionAdditional(res, customizeAction);
            }
        };

    }


}
