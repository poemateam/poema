package ru.belokonalexander.poema.Interfaces;

/**
 * Created by alexander on 17.08.2016.
 */
public interface OnScrollFullDown {

   void action(boolean isOnBottom);

}
