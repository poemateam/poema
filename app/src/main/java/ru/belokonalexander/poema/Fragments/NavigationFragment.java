package ru.belokonalexander.poema.Fragments;



import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.greenrobot.eventbus.EventBus;

import java.util.Date;

import StaticHelper.AppNavigation;
import StaticHelper.LogSystem;
import StaticHelper.Settings;
import belokonalexander.GlobalShell;
import belokonalexander.SuppleRecycler.ActionRecycler;
import ru.belokonalexander.poema.MainActivity;
import ru.belokonalexander.poema.PageManagers.Types.CustomData;
import ru.belokonalexander.poema.PageManagers.Types.ListData;
import ru.belokonalexander.poema.PoemaApplication;
import ru.belokonalexander.poema.R;

import static android.R.attr.fragment;
import static android.R.attr.tag;

/**
 * Created by alexander on 10.10.2016.
 */
 abstract public class NavigationFragment extends Fragment {


    //for recreate view in back state situation
    //because fragment not recreates, but onCreate is calling (wee need skip onCreate)
    View rootView = null;

    Tracker tracker;


    private View.OnClickListener onNavigationClick = null;

    public View.OnClickListener getOnNavigationClick() {
        return onNavigationClick;
    }

    public void fillToolbar(String title){


        if(getArguments()!=null && getArguments().getBoolean(AppNavigation.BACKPRESS_ENABLE)){
            getToolbar().setNavigationIcon(R.drawable.ic_keyboard_arrow_left_white_36dp);

            onNavigationClick = view1 -> {


                FragmentManager manager = getActivity().getSupportFragmentManager();



                getActivity().onBackPressed();

                Fragment fragment = null;

                MainActivity activity = (MainActivity) getActivity();


                for(Fragment fr : activity.getSupportFragmentManager().getFragments()){
                    if(fr==null){
                        continue;
                    }
                    if(fr.getTag()!=null && fr.getTag().equals(String.valueOf(activity.getTopFragment()))){
                        fragment = fr;
                    }
                }


                if(fragment!=null)
                    fragment.onResume();

            };

            getToolbar().setNavigationOnClickListener(onNavigationClick);
        }  else {
            getToolbar().setNavigationIcon(R.drawable.ic_menu_white_36dp);
            onNavigationClick = view1 -> ((MainActivity) getActivity()).openNavigation();

            getToolbar().setNavigationOnClickListener(onNavigationClick);
        }

        getToolbar().setContentInsetStartWithNavigation(0);
        getToolbar().setTitle(title);

    }


    public View getRootView() {
        return rootView;
    }

    public void setRootView(View rootView) {
        this.rootView = rootView;
    }

    public View[] getToolbarComponents(){

        ViewGroup toolbarContainer = (ViewGroup) getToolbarContainer();

        View[] childs = new View[toolbarContainer.getChildCount()];

        for(int i =0; i < toolbarContainer.getChildCount(); i++)
        {
                childs[i] = toolbarContainer.getChildAt(i);
        }

        return childs;
    }

    public View getToolbarContainer(){

        View view;

        view = ((View)getToolbar().getParent());
        if(view.getId()>0)
            return view;

        return getToolbar();
    }

    abstract public Toolbar getToolbar();

    public void openNewFragment(Fragment fragment){
        this.onPause();
        ((MainActivity)getActivity()).openInThisContainer(fragment);
    }

    @Override
    public void onResume() {
        super.onResume();


        tracker.setScreenName("Screen~" + getClass().getName());
        tracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public void onPause() {
        super.onPause();
        LogSystem.logThis(" ON PAUSE NAVIGATION");
        GlobalShell.tryCloseKeyboard(getActivity());

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PoemaApplication application = (PoemaApplication) getActivity().getApplication();
        tracker = application.getTracker(PoemaApplication.TrackerName.GLOBAL_TRACKER);
    }
}
