package ru.belokonalexander.poema.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.backendless.BackendlessUser;
import com.dd.processbutton.iml.ActionProcessButton;

import java.util.HashMap;
import java.util.List;

import StaticHelper.LogSystem;
import belokonalexander.Api.ApiExceptions.PoemaApiError;
import belokonalexander.Api.ApiItem;
import belokonalexander.Api.CurrentApi;
import belokonalexander.Api.Models.User;
import belokonalexander.Api.QueryExecutor;
import belokonalexander.Api.QueryExecutorExplicit;
import belokonalexander.RxCommonElements.Form.FormAction;
import belokonalexander.GlobalShell;
import belokonalexander.RxCommonElements.Form.FormTextElement;
import belokonalexander.RxCommonElements.Form.RxForm;
import belokonalexander.SharedAppPrefs;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.belokonalexander.poema.Interfaces.RedirectNextFragment;
import ru.belokonalexander.poema.Login;
import ru.belokonalexander.poema.MainActivity;
import ru.belokonalexander.poema.PageManagers.BackendRunner;
import ru.belokonalexander.poema.R;

/**
 * Created by alexander on 28.07.2016.
 */
public class SignUp extends Fragment {

    @BindView(R.id.sign_up_button)
    ActionProcessButton signUpButton;

    @BindViews({R.id.email_edit, R.id.password_edit, R.id.username_edit})
    List<FormTextElement> formFields;

    @OnClick(R.id.sign_in_button)
    public void clickForRedirect()
    {
        if(redirectObject!=null)
             redirectObject.redirect();
    }

    RedirectNextFragment redirectObject;


    public void setSettings(RedirectNextFragment r)
    {
        this.redirectObject = r;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_sign_up,container,false);
        ButterKnife.bind(this,view);



        RxForm.create(getContext(), signUpButton, formFields, new FormAction() {
            @Override
            public Object actionStart() {
                GlobalShell.tryCloseKeyboard(getActivity());
                HashMap<String, String> data = new HashMap<>();
                data.put(ApiItem.EMAIL_KEY, formFields.get(0).getText().toString().trim());
                data.put(ApiItem.PASSWORD_KEY, formFields.get(1).getText().toString().trim());
                data.put(ApiItem.NAME_KEY, formFields.get(2).getText().toString().trim());


                QueryExecutor waiterShell = new QueryExecutor(GlobalShell.getParentHash(this.getClass()));

                Object result = waiterShell.execute(resultWaiter -> CurrentApi.getInstanse().getApplicationApi().registration(data, resultWaiter));

                if (!(result instanceof Exception)) {
                    waiterShell = new QueryExecutor(GlobalShell.getParentHash(this.getClass()));
                    result = waiterShell.execute(resultWaiter -> CurrentApi.getInstanse().getApplicationApi().login(data, resultWaiter));
                }

                return result;
            }

            @Override
            public void succeedAction(Object user) {
                CurrentApi.getInstanse().setCurrentUser((User) user);
                ((Login)getActivity()).tryRegisterDevice();
                GlobalShell.openActivityWithCloseThis(getContext(), MainActivity.class);
            }

            @Override
            public void failedAction(Exception e) {
                if (e instanceof PoemaApiError) {

                    PoemaApiError error = (PoemaApiError) e;
                    if(error.getCode()==3033)
                        error.setResolvedStatus(true);

                    if(!CurrentApi.getInstanse().getErrorResolver().tryResolve(error)){
                            LogSystem.makeToast(getActivity(),getContext().getResources().getString(R.string.user_exists), LogSystem.ToastType.Error,LogSystem.TOAST_POSITION_BOTTOM);
                    }
                }
            }
        });


        return view;
    }




}
