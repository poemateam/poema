package belokonalexander.SuppleRecycler;

import android.content.Context;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.ActionMenuView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.ybq.android.spinkit.style.Circle;
import com.liucanwen.app.headerfooterrecyclerview.HeaderAndFooterRecyclerViewAdapter;
import com.liucanwen.app.headerfooterrecyclerview.RecyclerViewUtils;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import StaticHelper.LogSystem;
import StaticHelper.Settings;
import belokonalexander.Api.InputQueryDataHolders.MassiveQueryInputData;
import belokonalexander.GlobalShell;
import belokonalexander.RxCommonElements.AnswerApiRx;
import belokonalexander.SuppleRecycler.DataProviders.Abstracts.SolidProvider;
import ru.belokonalexander.poema.Fragments.ListAdapters.CommonAdapter;
import ru.belokonalexander.poema.PoemaApplication;
import ru.belokonalexander.poema.R;
import ru.belokonalexander.poema.Scrollers.HidingScrollListener;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by alexander on 06.10.2016.
 */
public class ActionRecycler<T> extends RecyclerView {
    public ActionRecycler(Context context) {
        super(context);
    }

    public ActionRecycler(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ActionRecycler(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    //current Y position of scroll
    int allDy=0;

    CommonAdapter<T> adapter;
    SolidProvider<T> provider;

    Long lastSuccessCall;

    public Long getLastSuccessCall() {
        return lastSuccessCall;
    }

    Toolbar toolbar;
    TextView noDataMessage;
    ProgressBar progressElement;

    Boolean updatingInProgress = false;

    MassiveQueryInputData recyclerState;

    SwipeRefreshLayout swipeRefreshLayout = null;

    View headerView;

    View refreshView;

    boolean allDataWasObtained;

    int TOOLBAR_REFRESH_MODE = 0;

    @Override
    public void setAdapter(Adapter adapter) {
        super.setAdapter(adapter);
        this.adapter = (CommonAdapter) ((HeaderAndFooterRecyclerViewAdapter) adapter).getInnerAdapter();
    }

    public void setAfterSuccessInit(AfterInit afterInit){
        afterSuccessInit = afterInit;
    }

    public void setAfterCacheData(AfterCacheData after){
        afterCacheData = after;
    }

    AfterInit afterSuccessInit;
    AfterCacheData afterCacheData;



    View toolbarContainer;

    public void setToolbarIndicator(int i){
        TOOLBAR_REFRESH_MODE = i; // 0 - toolbar, 1 - progress, 2 - none
    }

    public void init(Toolbar toolbar){
        this.toolbar = toolbar;

        View potentialContainer = (View) toolbar.getParent();
        if(potentialContainer.getId()>0) toolbarContainer = potentialContainer;
            else toolbarContainer = toolbar;

        toolbarContainer.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);

        headerView = LayoutInflater.from(getContext()).inflate(R.layout.item_top, this, false);

        headerView.getLayoutParams().height = toolbarContainer.getMeasuredHeight();

        RecyclerViewUtils.setHeaderView(this, headerView);
        this.addOnScrollListener(hideToolbarScrollListener);
        this.recyclerState = new MassiveQueryInputData();

        this.addOnScrollListener(new RecyclerView.OnScrollListener() {
                                     @Override
                                     public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                                         super.onScrolled(recyclerView, dx, dy);
                                         onScrollHeightController(recyclerView, dx, dy);
                                     }
                                 }
        );


        if(this.getParent() instanceof SwipeRefreshLayout){
            swipeRefreshLayout = (SwipeRefreshLayout) this.getParent();

            swipeRefreshLayout.setOnRefreshListener(() -> getData(UpdateMode.REFRESH));

            swipeRefreshLayout.setEnabled(false);

            swipeRefreshLayout.setProgressViewOffset(false, 0, headerView.getLayoutParams().height + headerView.getLayoutParams().height / 2);
            int accentColor = ContextCompat.getColor(getContext(), R.color.colorAccent);
            swipeRefreshLayout.setColorSchemeColors(accentColor, accentColor, accentColor);

        }

        //region create progress and message

        noDataMessage = new TextView(getContext());

        RelativeLayout relativeLayout;
        RelativeLayout.LayoutParams relparms;
        RelativeLayout.LayoutParams layoutParams;

        if (TOOLBAR_REFRESH_MODE==1) {



            progressElement = new ProgressBar(getContext());
            relativeLayout = new RelativeLayout(getContext());
            relparms = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            relativeLayout.setLayoutParams(relparms);
            relativeLayout.addView(progressElement);
            layoutParams = (RelativeLayout.LayoutParams) progressElement.getLayoutParams();
            layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);

            progressElement.getLayoutParams().height= GlobalShell.dpToPixels(40);
            progressElement.getLayoutParams().width=GlobalShell.dpToPixels(40);
            progressElement.setLayoutParams(layoutParams);
            progressElement.setVisibility(INVISIBLE);
            progressElement.bringToFront();
            ((ViewGroup)getRootView()).addView(relativeLayout);

            Circle chasingDots = new Circle();
            chasingDots.setColor(ContextCompat.getColor(getContext(), R.color.colorAccent));
            progressElement.setIndeterminateDrawable(chasingDots);
        } else if(TOOLBAR_REFRESH_MODE == 0){

            MenuItem refreshItem = toolbar.getMenu()
                    .add(0,999,999,R.string.refresh);
            refreshItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            refreshItem.setOnMenuItemClickListener(menuItem -> {
                getData(UpdateMode.REFRESH);
                return false;
            });
            refreshItem.setIcon(R.drawable.ic_autorenew_white_36dp);

            ActionMenuView amv = (ActionMenuView) toolbar.getChildAt(toolbar.getChildCount()-1);
            refreshView = amv.getChildAt(amv.getChildCount()-1);

        }


        relativeLayout = new RelativeLayout(getContext());
        relparms = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        relativeLayout.setLayoutParams(relparms);
        relativeLayout.addView(noDataMessage);
        layoutParams =
                (RelativeLayout.LayoutParams)noDataMessage.getLayoutParams();
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);

        noDataMessage.setTextSize(18); // FIXME: 03.10.2016
        noDataMessage.setTextColor(getResources().getColor(R.color.normal_text_color));
        noDataMessage.setLayoutParams(layoutParams);
        noDataMessage.setText(getResources().getString(R.string.no_data_found));
        noDataMessage.setVisibility(INVISIBLE);

        noDataMessage.bringToFront();
        ((ViewGroup)getRootView()).addView(relativeLayout);
        //endregion

    }

    public ActionRecycler setProvider(SolidProvider<T> provider) {
        this.provider = provider;
        return this;
    }




    public enum UpdateMode{
        ADD, REWRITE, REFRESH, INITIAL;
    }

    public enum DataType{
        CACHE, QUERY, EMPTY
    }

    DataType lastDataSourceType = DataType.EMPTY;


    public void insertInStart(List<T> inserted) {
        List<T> data = getDataAdapter().getData();
        data.addAll(0,inserted);
        adapter.notifyItemRangeInserted(0,inserted.size());
    }

    public void addLastItem(T item){
        adapter.add(item);
        adapter.notifyItemInserted(adapter.getItemCount()-1);
    }

    public void addFirstItem(T item){
        adapter.addFirst(item);
        adapter.notifyItemInserted(0);

        if(noDataMessage.getVisibility()==VISIBLE){
            noDataMessage.setVisibility(INVISIBLE);
        }

    }

    public void updateItem(int index, T item){
        adapter.update(index,item);
    }

    public void updateItem(int index){
        adapter.notifyItemChanged(index);
    }

    public void updateItem(T item){
        adapter.update(item);
    }

    public void replaceWith(T replaced, T item, boolean notify){
        List<T> data = adapter.getData();
        int index = data.indexOf(replaced);
        T rep = data.set(index,item);

        if(notify)
            adapter.notifyItemChanged(index);

    }

    public void sort(Comparator<T> comparator){
        Collections.sort(getDataAdapter().getData(), comparator);
        adapter.notifyDataSetChanged();

    }

    public T removeItem(int index){
        List<T> data = adapter.getData();
        T removedItem = data.remove(index);
        adapter.notifyItemRemoved(index);
        return removedItem;
    }

    public void removeItem(T item){
        List data = adapter.getData();
        int index = data.indexOf(item);
        data.remove(item);
        adapter.notifyItemRemoved(index);
    }

    public void moveItem(int posTo, int posFrom){

        T item = getDataAdapter().getData().get(posFrom);
        getDataAdapter().getData().remove(item);

        getDataAdapter().getData().add(0, item);
        adapter.notifyItemMoved(posTo, posFrom);
        //adapter.notifyDataSetChanged();
    }


    public CommonAdapter<T> getDataAdapter(){
        return adapter;
    }

    public void getData(UpdateMode updateMode){

        if(updateMode!=UpdateMode.REFRESH && (updateMode==UpdateMode.REWRITE || recyclerState.getOffset()==0))
            beforeUpdating();
        else if(updateMode==UpdateMode.REFRESH && TOOLBAR_REFRESH_MODE==0){
            beforeUpdating();
        }

        Observable.just("")
                .observeOn(Schedulers.newThread())
                .flatMap(u -> Observable.create(s -> {

                    MassiveQueryInputData caller = null;
                    if (updateMode == UpdateMode.REWRITE || updateMode == UpdateMode.REFRESH || updateMode == UpdateMode.INITIAL) {
                       caller = new MassiveQueryInputData(recyclerState.getPageSize());
                    } else caller = recyclerState;

                    recyclerState.setOffset(adapter.getItemCount());



                    if(updateMode == UpdateMode.INITIAL){
                        initByCacheData(provider.getCacheData(recyclerState.getPageSize()));
                    }


                    s.onNext(provider.getData(caller));
                    s.onCompleted();
                }))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(res -> {
                    AnswerApiRx.customizeRxSubsribeAction(res, new AnswerApiRx.CustomizeAction() {
                        @Override
                        public void onSuccess() {

                            lastSuccessCall = new Date().getTime();
                            //LogSystem.logThis("ON SUCCESS");

                            boolean changed = true;

                            List<T> data = (List<T>) res;
                            if (updateMode == UpdateMode.REWRITE || updateMode == UpdateMode.REFRESH || updateMode == UpdateMode.INITIAL) {
                                changed = rewriteData(data,updateMode);
                            }

                            if(changed) {
                                for (T item : data)
                                    adapter.add(item);

                                adapter.notifyDataSetChanged();
                            }


                            if(data.size()<recyclerState.getPageSize()) allDataWasObtained = true;
                            else allDataWasObtained = false;

                            recyclerState.addOffset(data.size());
                            LogSystem.logThis(" WAWA? " + Thread.currentThread().getName());
                            //



                            if (adapter.getItemCount() == 0) noDataMessage.setVisibility(VISIBLE);
                            else noDataMessage.setVisibility(INVISIBLE);

                            afterUpdating();
                        }

                        @Override
                        public void onFailure(String message) {

                            //LogSystem.logThis("ON FAILURE 1111: " + message);
                            afterUpdating();
                        }

                        @Override
                        public void onCanceled() {
                            //LogSystem.logThis("ON FAILURE 1111: ");
                            afterUpdating();
                        }
                    });


                });

    }

    private void initByCacheData(List<T> cacheData) {


        for (T item : cacheData) {
            adapter.add(item);
        }

        Handler mainHandler = new Handler(getContext().getMainLooper());

        Runnable myRunnable = () -> adapter.notifyDataSetChanged();
        mainHandler.post(myRunnable);

        lastDataSourceType = DataType.CACHE;

        if(afterCacheData!=null)
            afterCacheData.onLoad();

    }


    private void hideToolbar() {

        toolbarContainer.animate().translationY(-toolbar.getHeight()).setInterpolator(new AccelerateInterpolator(2));

    }

    private void showToolbar() {

        toolbarContainer.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2));

    }

    public void resetTollbarState(){
        hideToolbarScrollListener.reset();
    }

    protected boolean preToolbarHideShowCondition(){
        return true;
    }


    protected HidingScrollListener hideToolbarScrollListener = new HidingScrollListener() {
        @Override
        public boolean onHide() {

            if (allDy > headerView.getLayoutParams().height * 3/4 && preToolbarHideShowCondition()) {
                hideToolbar();
                return true;
            }

            return false;
        }

        @Override
        public void onShow() {
            showToolbar();
        }
    };

    private void beforeUpdating(){

        if(TOOLBAR_REFRESH_MODE==1)
            progressElement.setVisibility(VISIBLE);
        else if(TOOLBAR_REFRESH_MODE==0) {
            refreshView.setEnabled(false);
            animateToolbarIcon();
        }
        setUpdatingInProgress(true);
    }

     void afterUpdating(){
        if(TOOLBAR_REFRESH_MODE==1){
            progressElement.setVisibility(GONE);
        } else if(TOOLBAR_REFRESH_MODE==0) {
            refreshView.clearAnimation();
            refreshView.setEnabled(true);
        }


         if(swipeRefreshLayout!=null) {
             if(swipeRefreshLayout.isRefreshing())
             swipeRefreshLayout.setRefreshing(false);

             if(!swipeRefreshLayout.isEnabled())
                 swipeRefreshLayout.setEnabled(true);

         }

        setUpdatingInProgress(false);

    }


    void onScrollHeightController(RecyclerView recyclerView, int dx, int dy){
        allDy += dy;
    }

    boolean rewriteData(List data, UpdateMode updateMode){

        boolean rewrite = true;

        if(updateMode == UpdateMode.INITIAL && lastDataSourceType ==DataType.CACHE) {

            List cacheData = adapter.getData();

            if(GlobalShell.listsAreEquals(cacheData,data)){
                rewrite = false;
                LogSystem.logThis("DATA HAS THE SAME VALUES");
            } else LogSystem.logThis("DATA HAS THE DIFFERENT VALUES");

            if(afterSuccessInit!=null)
                afterSuccessInit.onSuccess();
        }

        if(rewrite) {
            adapter.clear();
            recyclerState.reset();
            scrollToPosition(0);
            allDy = 0;
            hideToolbarScrollListener.reset();
        }


        return  rewrite;
    }

    public Boolean getUpdatingInProgress() {
        return updatingInProgress;
    }

    private void setUpdatingInProgress(Boolean updatingInProgress) {
        this.updatingInProgress = updatingInProgress;
    }


    private void animateToolbarIcon(){


        AnimationSet animationSet = new AnimationSet(true);
        Animation animationRotate = new RotateAnimation(0,360,Animation.RELATIVE_TO_SELF, .5f, Animation.RELATIVE_TO_SELF, .5f);
        animationRotate.setDuration(Settings.REFRESH_ROTATE_ANIMATION_DURATION);
        animationRotate.setRepeatMode(Animation.RESTART);
        animationRotate.setRepeatCount(Animation.INFINITE);
        Animation animationAlpha = new AlphaAnimation(1f,.7f);
        animationAlpha.setDuration(300);
        animationAlpha.setRepeatCount(Animation.INFINITE);
        animationAlpha.setRepeatMode(Animation.REVERSE);
        animationSet.addAnimation(animationRotate);
        //animationSet.addAnimation(animationAlpha);

        refreshView.startAnimation(animationSet);
    }

    public interface AfterInit{
        void onSuccess();
    }

    public interface AfterCacheData {
        void onLoad();
    }

}
