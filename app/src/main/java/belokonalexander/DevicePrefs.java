package belokonalexander;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.WindowManager;

/**
 * Created by admin on 26.07.2016.
 */
public class DevicePrefs {


    static DisplayMetrics display;

    public static DisplayMetrics getDisplay() {
        return display;
    }

    public static void InitDevicePrefs(DisplayMetrics d)
    {
        if(display==null)
        {
            display = d;;
        }
    }

    public static float getScale() {
        return display.density;
    }

    public static int getActinBarSize(Context context)
    {
        final TypedArray styledAttributes = context.getTheme().obtainStyledAttributes(
                new int[] { android.R.attr.actionBarSize });
        return (int) styledAttributes.getDimension(0, 0);
    }

    public static float getNormalListHeight(Context context)
    {
        TypedValue value = new TypedValue();
        DisplayMetrics metrics = new DisplayMetrics();

        context.getTheme().resolveAttribute(
                android.R.attr.listPreferredItemHeight, value, true);
        ((WindowManager) (context.getSystemService(Context.WINDOW_SERVICE)))
                .getDefaultDisplay().getMetrics(metrics);

        return TypedValue.complexToDimension(value.data, metrics);
    }

}
