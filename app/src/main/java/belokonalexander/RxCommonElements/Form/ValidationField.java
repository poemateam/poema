package belokonalexander.RxCommonElements.Form;

import java.util.List;

/**
 * Created by alexander on 02.08.2016.
 */
public interface ValidationField {

    ValidationAnswer validate();

}
