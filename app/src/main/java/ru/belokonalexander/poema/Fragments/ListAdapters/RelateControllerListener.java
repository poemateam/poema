package ru.belokonalexander.poema.Fragments.ListAdapters;

import belokonalexander.Api.Models.FriendsRelate;
import belokonalexander.Api.Models.User;

/**
 * Created by alexander on 20.10.2016.
 */
public interface RelateControllerListener {
    void onUpdateUserState(FriendsRelate.RelateStates from, User user);
}
