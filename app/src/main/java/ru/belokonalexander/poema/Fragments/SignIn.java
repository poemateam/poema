package ru.belokonalexander.poema.Fragments;

import android.content.Intent;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.BackendlessCallback;
import com.dd.processbutton.iml.ActionProcessButton;
import com.gc.materialdesign.views.Button;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import StaticHelper.LogSystem;
import belokonalexander.Api.ApiExceptions.PoemaApiError;
import belokonalexander.Api.ApiItem;
import belokonalexander.Api.CurrentApi;
import belokonalexander.Api.Models.User;

import belokonalexander.Api.QueryExecutor;
import belokonalexander.Api.QueryExecutorExplicit;
import belokonalexander.GlobalShell;
import belokonalexander.RxCommonElements.Form.FormAction;
import belokonalexander.RxCommonElements.Form.FormTextElement;
import belokonalexander.RxCommonElements.Form.RxForm;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.belokonalexander.poema.Interfaces.RedirectNextFragment;
import ru.belokonalexander.poema.Login;
import ru.belokonalexander.poema.MainActivity;
import ru.belokonalexander.poema.PageManagers.BackendRunner;
import ru.belokonalexander.poema.R;


/**
 * Created by alexander on 28.07.2016.
 */
public class SignIn extends Fragment {



    @BindView(R.id.sign_in_button)
    ActionProcessButton signInButton;

    @BindViews({R.id.email_edit,R.id.password_edit})
    List<FormTextElement> formTextFields;


    @OnClick(R.id.sign_up_button)
    public void clickForRedirect()
    {
        if(redirectObject!=null)
            redirectObject.redirect();
    }

    @BindView(R.id.forgot_password_spin)
    ProgressBar progressBarForgotPassword;

    @OnClick(R.id.forgot_password)
    public void forgotPasswordCallback(View view){

        String email = formTextFields.get(0).getText().toString().trim();

        if(email.length()==0){
            LogSystem.makeToast(getActivity(),getContext().getString(R.string.need_email), LogSystem.ToastType.Info, LogSystem.TOAST_POSITION_BOTTOM);
            return;
        }


        QueryExecutorExplicit restorePassword = new QueryExecutorExplicit(GlobalShell.getParentHash(this.getClass()), resultWaiter -> CurrentApi.getInstanse().getApplicationApi().restorePassword( email, resultWaiter));

        view.setEnabled(false);
        progressBarForgotPassword.setVisibility(View.VISIBLE);

        BackendRunner backendRunner = new BackendRunner(new BackendRunner.SuccessAnswer() {
            @Override
            public void onSuccessCompleted(List<Object> answers) {
                LogSystem.makeToast(getActivity(),getContext().getString(R.string.email_was_sent) + " " + email, LogSystem.ToastType.Success, LogSystem.TOAST_POSITION_BOTTOM);
                view.setEnabled(true);
                progressBarForgotPassword.setVisibility(View.GONE);
            }
        }, messsage -> {
            if(!messsage.equals("canceled"))
                LogSystem.makeToast(getActivity(),getContext().getString(R.string.cant_find_user), LogSystem.ToastType.Error, LogSystem.TOAST_POSITION_BOTTOM);

            progressBarForgotPassword.setVisibility(View.GONE);
            view.setEnabled(true);
        },
        restorePassword);

        backendRunner.run();

    }

    RedirectNextFragment redirectObject;

    public void setSettings(RedirectNextFragment r)
    {
        this.redirectObject = r;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_sign_in,container,false);
        ButterKnife.bind(this, view);


                    RxForm.create(getContext(), signInButton, formTextFields, new FormAction() {
                        @Override
                        public Object actionStart() {
                            GlobalShell.tryCloseKeyboard(getActivity());
                            HashMap<String, String> data = new HashMap<>();
                            data.put(ApiItem.EMAIL_KEY, formTextFields.get(0).getText().toString().trim());
                            data.put(ApiItem.PASSWORD_KEY, formTextFields.get(1).getText().toString().trim());
                            QueryExecutor waiterShell = new QueryExecutor(GlobalShell.getParentHash(this.getClass()));
                            return waiterShell.execute(resultWaiter -> CurrentApi.getInstanse().getApplicationApi().login(data, resultWaiter));
                        }

                        @Override
                        public void succeedAction(Object user) {
                            User appUser = (User)user;
                            CurrentApi.getInstanse().setCurrentUser(appUser);
                            ((Login)getActivity()).tryRegisterDevice();
                            GlobalShell.openActivityWithCloseThis(getContext(), MainActivity.class);
                        }

                        @Override
                        public void failedAction(Exception e) {
                            if (e instanceof PoemaApiError) {
                                PoemaApiError error = (PoemaApiError) e;

                                if(error.getCode()==3003) {
                                    error.setResolvedStatus(true);
                                }


                                if(!CurrentApi.getInstanse().getErrorResolver().tryResolve(error)){

                                    LogSystem.makeToast(getActivity(), getContext().getResources().getString(R.string.error_invalid_user), LogSystem.ToastType.Error, LogSystem.TOAST_POSITION_BOTTOM);
                                }
                            }

                        }
                    });


                     return view;
                 }


}
