package Helper;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.ms.square.android.expandabletextview.ExpandableTextView;

/**
 * Created by Alexander on 02.03.2017.
 */

public class ExpandableTextDecorator extends ExpandableTextView {
    public ExpandableTextDecorator(Context context) {
        super(context);
    }

    public ExpandableTextDecorator(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ExpandableTextDecorator(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    ExpandableTextView textView;

    @Override
    public void onClick(View view) {
        super.onClick(view);
    }
}
