package belokonalexander.Api.Models.Annotations;

import android.content.Context;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import belokonalexander.Api.Models.SearchedEntity;
import ru.belokonalexander.poema.R;

/**
 * Created by admin on 15.08.2016.
 */
public class DefaultAdditionSearchData {

    public static List<String> getSearchFields(Class <? extends SearchedEntity> home) {
        List<String> result = new ArrayList<>();
        for (java.lang.reflect.Field field : home.getDeclaredFields()) {
            if (field.isAnnotationPresent(SearchField.class)) {
                result.add(field.getName());
            }
        }
        return result;
    }

    public static List<SearchItem> getSearchFieldsAndType(Class <? extends SearchedEntity> home, Context context) {
        List<SearchItem> result = new ArrayList<>();
        for (java.lang.reflect.Field field : home.getDeclaredFields()) {
            if (field.isAnnotationPresent(SearchField.class)) {
                final SearchField annotation = field.getAnnotation(SearchField.class);
                result.add(new SearchItem(field.getName(),annotation.lazySearch(), annotation.fullContains(), getLanguagedAlias(annotation.alias(), context),annotation.order()));
            }
        }

        Collections.sort(result,new Comparator<SearchItem>() {
            @Override
            public int compare(SearchItem o1, SearchItem o2) {
                return  (o1.getOrder() > o2.getOrder()) ?  1 :
                        (o1.getOrder() < o2.getOrder()) ? -1 : 0;
            }
        });

        return result;
    }

    private static String getLanguagedAlias(String alias, Context context) {
        String res = "Unknown";
        switch (alias) {
            case "Pen name":
                res = context.getString(R.string.pen_name_s);
                break;
            case "Email":
                res = context.getString(R.string.email_s);
                break;
            case "Name":
                res = context.getString(R.string.name_s);
                break;
            case "Title":
                res = context.getString(R.string.title);
                break;
        }
        return res;
    }

    public static String getSearchTable(Class <? extends SearchedEntity> home)
    {
        Annotation[] annotations = home.getAnnotations();
        for(Annotation annotation : annotations)
        {
            if(annotation instanceof Table)
                return ((Table)annotation).name();
        }

        return null;
    }



}
