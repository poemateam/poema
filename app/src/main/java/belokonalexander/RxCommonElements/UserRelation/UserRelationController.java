package belokonalexander.RxCommonElements.UserRelation;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.daimajia.swipe.SwipeLayout;

import org.greenrobot.eventbus.EventBus;

import StaticHelper.LogSystem;
import belokonalexander.Api.CurrentApi;
import belokonalexander.Api.Models.FriendsRelate;
import belokonalexander.Api.Models.User;
import belokonalexander.Api.QueryExecutor;
import belokonalexander.GlobalShell;
import belokonalexander.RxCommonElements.AnswerApiRx;
import ru.belokonalexander.poema.Events.FriendEvent;
import ru.belokonalexander.poema.Fragments.ListAdapters.RelateControllerListener;
import ru.belokonalexander.poema.Fragments.ListAdapters.ControllerHolder;
import ru.belokonalexander.poema.PoemaApplication;
import ru.belokonalexander.poema.R;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by alexander on 21.09.2016.
 */
public class UserRelationController extends LinearLayout{

    private User user; //another user which we wanna contact

    private Context context;

    private RelateControllerListener onChangeListener = null;

    public UserRelationController(Context context) {
        super(context);
        this.context = context;
    }

    public UserRelationController(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    public UserRelationController(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
    }

    private ControllerHolder controllerHolder = null;
    private View link = null;

    public void setUser(User user, RelateControllerListener onChangeListener, ControllerHolder controllerHolder){
        this.controllerHolder = controllerHolder;
        this.link = controllerHolder.getControllerLink();
        setUser(user,onChangeListener);
    }

    public void setUser(User user, RelateControllerListener onChangeListener){
        this.user = user;
        this.onChangeListener = onChangeListener;
        updateStatus(true);
    }

    public void setUser(User user){
        this.user = user;
        updateStatus(true);
    }

    //all actions will work via this implement
    UserSingleRelation userSingleRelation = new UserSingleRelation();

    //change status and change layout
    private void updateStatus(boolean init){



        this.removeAllViews();

        View inflatedContent = null;

            switch (user.getFriendStatusDesc()) {
                case NONE:
                    inflatedContent = setNoneFriendInflater();
                    break;
                case I_SEND_FRIENDSHIP:
                    inflatedContent = setISendFriendshipInflater();
                    break;
                case I_GOT_FRIENDSHIP:
                    inflatedContent = setIGotFriendshipInflater();
                    break;
                case FRIENDS:
                    inflatedContent = setUserIsMyFriendInflater();

                    break;
            }

        addView(inflatedContent,0);

        if(controllerHolder!=null){
            if(init && controllerHolder.getSwipeLayout().getOpenStatus()== SwipeLayout.Status.Open)
                controllerHolder.getSwipeLayout().close(false,true);

            if(link!=null && link instanceof ImageView) {
                FriendsRelate.fillStatusInImage(getContext(),user.getFriendStatusDesc(), (ImageView) link);
            }
        }
    }

    FriendsRelate.RelateStates prevRelate = null;

    public User getUser() {
        return user;
    }

    private View setUserIsMyFriendInflater() {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View addFriendsController = inflater.inflate(R.layout.user_relate_we_are_friends,this,false);



        if(controllerHolder==null){
            addFriendsController.findViewById(R.id.cancel_friendship).setBackground(null);
        }

        addFriendsController.findViewById(R.id.cancel_friendship).setOnClickListener(v-> {

            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(R.string.delete_friendship);
            builder.setMessage(R.string.are_you_shure);
            builder.setNegativeButton(R.string.no, (dialog, which) -> {

            });
            builder.setPositiveButton(R.string.yes, (dialog, which) -> {
                userSingleRelation.execute((u, resultWaiter) -> CurrentApi.getInstanse().getApplicationApi().cancelFriendship(u, resultWaiter), v);
                dialog.dismiss();
            });

            AlertDialog alert = builder.create();
            alert.show();

            //
        });

        return addFriendsController;
    }

    private View setNoneFriendInflater() {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View addFriendsController = inflater.inflate(R.layout.user_relate_none_friend, null);

        if(controllerHolder==null){
            addFriendsController.findViewById(R.id.send_friend_invite).setBackground(null);
        }

        addFriendsController.findViewById(R.id.send_friend_invite).setOnClickListener(f -> userSingleRelation.execute((u, resultWaiter) -> CurrentApi.getInstanse().getApplicationApi().offerFriendship(u, resultWaiter),f));

        return addFriendsController;

    }

    private View setISendFriendshipInflater() {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View addFriendsController = inflater.inflate(R.layout.user_relate_i_sended_friend, null);

        if(controllerHolder==null){
            addFriendsController.findViewById(R.id.cancel).setBackground(null);
        }

        addFriendsController.findViewById(R.id.cancel).setOnClickListener(f -> {

            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(R.string.cancel_friendship);
            builder.setMessage(R.string.are_you_shure);
            builder.setNegativeButton(R.string.no, (dialog, which) -> {

            });
            builder.setPositiveButton(R.string.yes, (dialog, which) -> {
                userSingleRelation.execute((u, resultWaiter) -> CurrentApi.getInstanse().getApplicationApi().cancelFriendship(u, resultWaiter),f);
                dialog.dismiss();
            });

            AlertDialog alert = builder.create();
            alert.show();

        });

        return addFriendsController;

    }

    private View setIGotFriendshipInflater() {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View addFriendsController = inflater.inflate(R.layout.user_relate_i_got_friend, null);
        if(controllerHolder==null){
            addFriendsController.findViewById(R.id.user_relate_i_got_friend).setBackground(null);
            addFriendsController.findViewById(R.id.user_relate_i_got_friend_cancel).setBackground(null);
        }
        addFriendsController.findViewById(R.id.user_relate_i_got_friend).setOnClickListener(f -> userSingleRelation.execute((u, resultWaiter) -> CurrentApi.getInstanse().getApplicationApi().confirmFriendship(u, resultWaiter),f));

        addFriendsController.findViewById(R.id.user_relate_i_got_friend_cancel).setOnClickListener(f -> {

            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(R.string.reject_friendship);
            builder.setMessage(R.string.are_you_shure);
            builder.setNegativeButton(R.string.no, (dialog, which) -> {

            });
            builder.setPositiveButton(R.string.yes, (dialog, which) -> {
                userSingleRelation.execute((u, resultWaiter) -> CurrentApi.getInstanse().getApplicationApi().cancelFriendship(u, resultWaiter),f);
                dialog.dismiss();
            });

            AlertDialog alert = builder.create();
            alert.show();

        });


        return addFriendsController;
    }

    //TODO поставить ограничение на отправление заявок (это должно быть на бэкенде)

    class UserSingleRelation {

        boolean queryInProgress = false;


        public void execute(RelationApiMethod action,View holder) {

            if(queryInProgress) return;
            queryInProgress = true;

            Animation updatingAnimation = new AlphaAnimation(1f,.5f);
            updatingAnimation.setDuration(600);
            updatingAnimation.setRepeatCount(Animation.INFINITE);



            // is execute on card view
            if(controllerHolder!=null) {
                //waiting for result
                controllerHolder.getSwipeLayout().close();
                View link = controllerHolder.getControllerLink();
                if(link!=null){
                    link.startAnimation(updatingAnimation);
                }
            } else {
                //todo callback from flat mode
                holder.startAnimation(updatingAnimation);
                holder.setEnabled(false);
            }

            FriendsRelate.RelateStates currentRelate = user.getFriendStatusDesc();

            QueryExecutor queryExecutor = new QueryExecutor(GlobalShell.getParentHash(getClass()),true);
            Observable.just(user)
                    .observeOn(Schedulers.newThread())
                    .flatMap(u -> Observable.create(s -> {

                        s.onNext(queryExecutor.execute(resultWaiter -> action.execute(u, resultWaiter)));
                        s.onCompleted();
                    }))
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(res -> {
                        AnswerApiRx.customizeRxSubsribeAction(res, new AnswerApiRx.CustomizeAction() {
                            @Override
                            public void onSuccess() {
                                LogSystem.logThis("PREV RELATE: " + prevRelate + " / CURRENT RELATE: " + currentRelate);
                                prevRelate = currentRelate;

                                updateStatus(false);
                                if(onChangeListener !=null)
                                    onChangeListener.onUpdateUserState(prevRelate,user);

                                showToast(user.getFriendStatusDesc(), prevRelate);


                                EventBus.getDefault().post(new FriendEvent(user));
                            }
                            @Override
                            public void onFailure(String message) {

                            }
                            @Override
                            public void onCanceled() {

                            }
                        });
                        if(controllerHolder!=null) {
                            //waiting for result
                            View link = controllerHolder.getControllerLink();
                            if(link!=null){
                                link.setAlpha(1f);
                                link.clearAnimation();
                            }
                        } else {
                            holder.setAlpha(1f);
                            holder.clearAnimation();
                            holder.setEnabled(true);
                        }
                        queryInProgress = false;
                    });
        }


    }

    private void showToast(FriendsRelate.RelateStates currentRelate, FriendsRelate.RelateStates prevRelate) {
        switch (currentRelate){
            case FRIENDS:
                LogSystem.makeToast(((PoemaApplication)context.getApplicationContext()).getCurrentActivity(),context.getResources().getString(R.string.friends_success), LogSystem.ToastType.Success,LogSystem.TOAST_POSITION_BOTTOM);
                break;
            case NONE:
                if(prevRelate== FriendsRelate.RelateStates.I_SEND_FRIENDSHIP)
                    LogSystem.makeToast(((PoemaApplication)context.getApplicationContext()).getCurrentActivity(),context.getResources().getString(R.string.friends_none_canceled_success), LogSystem.ToastType.Success,LogSystem.TOAST_POSITION_BOTTOM);
                if(prevRelate== FriendsRelate.RelateStates.I_GOT_FRIENDSHIP)
                    LogSystem.makeToast(((PoemaApplication)context.getApplicationContext()).getCurrentActivity(),context.getResources().getString(R.string.friends_none_cancel_success), LogSystem.ToastType.Success,LogSystem.TOAST_POSITION_BOTTOM);
                if(prevRelate== FriendsRelate.RelateStates.FRIENDS)
                    LogSystem.makeToast(((PoemaApplication)context.getApplicationContext()).getCurrentActivity(),context.getResources().getString(R.string.friends_none_success), LogSystem.ToastType.Success,LogSystem.TOAST_POSITION_BOTTOM);

                break;
            case I_SEND_FRIENDSHIP:
                LogSystem.makeToast(((PoemaApplication)context.getApplicationContext()).getCurrentActivity(),context.getResources().getString(R.string.friends_i_send_success), LogSystem.ToastType.Success,LogSystem.TOAST_POSITION_BOTTOM);
                break;
            case I_GOT_FRIENDSHIP:
                LogSystem.makeToast(((PoemaApplication)context.getApplicationContext()).getCurrentActivity(),context.getResources().getString(R.string.friends_i_got_success), LogSystem.ToastType.Success,LogSystem.TOAST_POSITION_BOTTOM);
                break;
        }
    }


}
