package StaticHelper;

import android.app.Activity;
import android.media.effect.Effect;
import android.os.Build;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.gc.materialdesign.utils.Utils;
import com.gitonway.lee.niftynotification.lib.Configuration;
import com.gitonway.lee.niftynotification.lib.Effects;
import com.gitonway.lee.niftynotification.lib.NiftyNotificationView;

import java.util.Date;

import belokonalexander.DevicePrefs;
import belokonalexander.GlobalShell;
import ru.belokonalexander.poema.R;


/**
 * Created by alexander on 26.07.2016.
 */
public class LogSystem {

    public static final String tag = "TAG";

    public static void logThis(Object o) {
        Log.e(tag, "-> " + o);
    }

    public static Long lastToast = new Date().getTime();
    public static final int TOAST_QUEUE_TIME = 1000;

    public enum ToastType
    {
        Error ("#db4437"),
        Info ("#ededed"),
        Success ("#5FCE6F");

        ToastType(String s) {
            color = s;
        }

        private final String color;

    }

    public static final int TOAST_POSITION_TOP = 0;
    public  static final int TOAST_POSITION_BOTTOM = 1;
    public  static final int TOAST_POSITION_CENTER = 2;

    public static void logThis(Object o, boolean condition){
        if(condition)
            logThis(o);
    }

    public static void makeToast(Activity activity, String text, ToastType type, int position )
    {

        View root = activity.findViewById(android.R.id.content);
        RelativeLayout relativeLayout = (RelativeLayout) activity.findViewById(R.id.toast_area_id);

        if(relativeLayout==null){

            relativeLayout = new RelativeLayout(activity);
            RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.MATCH_PARENT);



            relativeLayout.setLayoutParams(rlp);
            relativeLayout.setId(R.id.toast_area_id);
            ((ViewGroup)root).addView(relativeLayout);
        }

        Effects effect = Effects.flip;

        switch (position)
        {
            case TOAST_POSITION_BOTTOM:
                relativeLayout.setGravity(Gravity.BOTTOM);
                effect = Effects.jelly;
                break;

            case TOAST_POSITION_TOP:
                relativeLayout.setGravity(Gravity.TOP);
                break;

            case TOAST_POSITION_CENTER:
                relativeLayout.setGravity(Gravity.CENTER);
                effect = Effects.jelly;
                break;
        }
        if(new Date().getTime() > lastToast+TOAST_QUEUE_TIME) {
            lastToast = new Date().getTime();
            NiftyNotificationView.build(activity, text, effect, relativeLayout.getId(), getConfig(type)).show();
        }
    }

    private static Configuration getConfig(ToastType type)
    {

        Configuration.Builder builder = new Configuration.Builder()
                .setAnimDuration(Settings.TOAST_ANIMATION_VALUE)
                .setDispalyDuration(Settings.TOAST_SHOW_VALUE)
                .setBackgroundColor(type.color)
                .setTextColor("#FFFFFFFF")
                .setIconBackgroundColor("#FF0000")
                .setTextPadding(5)                      //dp
                .setViewHeight(48)                      //dp
                .setTextLines(2)                        //You had better use setViewHeight and setTextLines together
                .setTextGravity(Gravity.CENTER) ;        //only text def  Gravity.CENTER,contain icon Gravity.CENTER_VERTICAL



        if(type==ToastType.Info)
            builder.setTextColor("#bb193f6e");

        return builder.build();
    }

}
