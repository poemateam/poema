package belokonalexander.Api.Models;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import ru.belokonalexander.poema.Fragments.ListAdapters.SearchAdapter;

/**
 * Created by Alexander on 22.02.2017.
 */

public interface SearchEntityHolder extends  SearchedEntity{
    RecyclerView.ViewHolder getSearchViewForItem(ViewGroup parent, SearchAdapter parentAdapter);
    void onBindViewHolder(RecyclerView.ViewHolder holder, SearchedEntity entity, int pos, Context context);

}
